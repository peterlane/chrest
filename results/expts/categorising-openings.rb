# First experiment:
# Train model on proportion of French/Sicilian positions, 
# see if linked verbal pattern can be used to classify
# opening

require 'confusion-matrix'
require 'jchrest-chess'

# Parameters for experiment
NUM_TRAINING_CYCLES = 10 # the number of times CHREST is trained on the training positions
NUM_FIXATIONS = 20       # the number of fixations CHREST makes on each position

# -- datasets
DATA_PATH = File.join(File.dirname(__FILE__), '..', 'data')

French = Positions.from_file(File.join(DATA_PATH, 'french.csv'))
Sicilian = Positions.from_file(File.join(DATA_PATH, 'sicilian.csv'))
Dataset = French.merge(Sicilian)

# Sets the label for each position in given set.
# All existing features are removed.
def set_label(positions, feature)
  positions.each do |posn|
    posn.features = [feature]
  end
end

def run_experiment()
  puts
  puts " ==== STARTING EXPERIMENT ==== "
  puts
  # -- label the two datasets
  set_label(French, 'french')
  set_label(Sicilian, 'sicilian')

  # -- split the combined dataset randomly into train/test sets
  trainset, testset = Dataset.split 0.7

  # -- create a model
  puts "Training CHREST model: "
  model = Chrest.create_model_with_features(trainset, NUM_TRAINING_CYCLES, NUM_FIXATIONS)
  puts " -- model has #{model.ltm_visual_size} visual nodes"
  puts

  # -- test the model on each test position, collecting data into a confusion matrix
  cm_result = ConfusionMatrix.new
  testset.each do |posn|
    posn.features.each do |feature|
      cm_result.add_for(feature, model.classify_scene(posn.position))
    end
  end
  # -- display the results
  cm_result.labels.each do |key|
    puts "#{"%10s" % key} #{"%8d" % cm_result.true_positive(key)} #{"%8d" % cm_result.false_negative(key)} #{"%8.2f" % cm_result.recall(key)}"
  end
  puts "Geometric mean of accuracy: #{cm_result.geometric_mean}"
  puts
  puts cm_result
end

# -- for script, start the experiment
run_experiment()

