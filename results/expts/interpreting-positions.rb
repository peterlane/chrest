# Second experiment:
# Train model on proportion of French/Sicilian positions, 
# see if linked verbal patterns can be used to 
# provide interpretations of positions
#
# Initial experiment treats each interpretation as a target classification.

require 'jchrest-chess'

# Parameters for experiment
MIN_FEATURE_APPEARANCES = 20 # the minimum number of times a feature must be present in data
                             # to use it in experiment
NUM_TRAINING_CYCLES = 2      # the number of times CHREST is trained on the training positions
NUM_FIXATIONS = 100          # the number of fixations CHREST makes on each position

# -- datasets
DATA_PATH = File.join(File.dirname(__FILE__), '..', 'data')

French = Positions.from_file(File.join(DATA_PATH, 'french.csv'))
Sicilian = Positions.from_file(File.join(DATA_PATH, 'sicilian.csv'))
Dataset = French.merge(Sicilian)

# report some values for features and their presence
def describe_features(positions, trainset, testset)
  features = []
  positions.each_position do |position|
    features << position.features
  end
  puts "Features used are:"
  features.flatten.uniq.sort.each do |feature|
    puts "-- #{feature} (#{positions.count_occurrences(feature)} occurrences) (#{trainset.count_occurrences(feature)} - #{testset.count_occurrences(feature)})"
  end
  puts
end

# This function sets the features for each position to a subset of the total, 
# based on the number of occurrences of each feature. Only features which 
# appear in total more than MIN_FEATURE_APPEARANCES times are retained. 
# This is to ensure there are suitable numbers of position to learn from 
# and generalise to.
def select_features_per_position()
  total_features = 0
  French.each_position_and_features do |posn, features|
    posn.features = features.find_all {|f| Dataset.count_occurrences(f) > MIN_FEATURE_APPEARANCES}
    total_features += posn.features.size
  end
  Sicilian.each_position_and_features do |posn, features|
    posn.features = features.find_all {|f| Dataset.count_occurrences(f) > MIN_FEATURE_APPEARANCES}
    total_features += posn.features.size
  end

  puts "Average features / position: #{total_features.to_f / (French.size + Sicilian.size)}"
end

Result = Struct.new(:correct, :false_pos, :false_neg)

# This function builds a CHREST model using the positions in the 'trainset'
# It then compares the model's interpretation of each position in the 'testset' 
# with the target interpretations, and reports some information on its performance.
# All positions with more than one corect match are displayed in detail, and 
# a list of all features with performance statistics are shown.
def get_interpretations(trainset, testset)
  results = {}
  positions_tested = 0
  matches = {}

  # -- model
  puts "Training CHREST model: "
  model = Chrest.create_model_with_features(trainset, NUM_TRAINING_CYCLES, NUM_FIXATIONS)
  puts " -- model has #{model.ltm_visual_size} visual nodes"

  # -- test the model, displaying performance information
  testset.each_position do |posn|

    interpretations = model.interpret_scene(posn, NUM_FIXATIONS)
    positions_tested += 1
    num_matches = (posn.features & interpretations).size
    matches[num_matches] = matches.fetch(num_matches, 0) + 1

    # -- if model has matched 2 or more interpretations, display the position
    if num_matches > 1
      puts "For posn:"
      puts
      puts posn
      puts
      puts "Target interpretations are:"
      puts posn.features.sort.join("\n")
      puts
      puts "Model interpretations are:"
      puts interpretations.sort.join("\n")
      puts
      puts "Number matching is: #{num_matches}"
      puts "----------------\n\n"
    end

    # -- collect statistics on individual features: true_positives, false_positives and false_negatives
    interpretations.each do |interpretation|
      result = results.fetch(interpretation, Result.new(0, 0, 0))
      if posn.features.include?(interpretation)
        result.correct += 1
      else
        result.false_pos += 1
      end
      results[interpretation] = result
    end
    posn.features.each do |feature|
      result = results.fetch(feature, Result.new(0, 0, 0))
      unless interpretations.include?(feature)
        result.false_neg += 1
      end
      results[feature] = result
    end
  end

  # -- when experiment finished, display statistics on individual features
  puts
  puts "Statistics on individual features:"
  results.each_pair do |key, value|
    puts "For #{"%34s" % key}: #{"%2d" % value.correct} correct, #{"%2d" % value.false_pos} false alarms, #{"%2d" % value.false_neg} misses"
  end

  return positions_tested, matches
end

def run_experiment()
  puts
  puts " ==== STARTING EXPERIMENT ==== "
  puts
  select_features_per_position()
  trainset, testset = Dataset.split 0.7
  describe_features(Dataset, trainset, testset)
  positions_tested, matches = get_interpretations(trainset, testset)

  # -- display a summary of how often matching interpretations were made
  puts
  puts "Summary of number of matching interpretations:"
  puts "Tested #{positions_tested} positions"
  matches.each_pair do |key, value|
    puts "#{key} matches -> #{value} times"
  end
end

# -- for script, start the experiment
run_experiment()

