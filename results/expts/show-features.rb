# This file loads the two data files and displays each feature and how often it appears

require "jchrest-chess"

DATA_PATH = File.join(File.dirname(__FILE__), '..', 'data')

French = Positions.from_file(File.join(DATA_PATH, 'french.csv'))
Sicilian = Positions.from_file(File.join(DATA_PATH, 'sicilian.csv'))

full_set = French.merge(Sicilian)
full_set.all_features.each do |feature|
  puts "#{feature} #{full_set.count_occurrences(feature)}"
end

