# CHREST Chess Experiments

Some experiments using CHREST for chess game interpretation and classification:
see <http://chrest.info/software.html#chessboard>

1. Install jruby: <http://jruby.org>
2. Install confusion-matrix: <https://rubygems.org/gems/confusion-matrix/>
3. Install jchrest: <https://rubygems.org/gems/jchrest/>
4. Install jchrest-chess: <https://rubygems.org/gems/jchrest-chess/>

Run experiments:

    jruby expts/categorising-openings.rb
    jruby expts/interpreting-positions.rb

The 'data' folder contains the chess data with interpretations in original
(.txt) and csv formats.
