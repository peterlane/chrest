package jchrest.gui;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.InterruptedIOException;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.AbstractAction;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JSpinner;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.ProgressMonitorInputStream;
import javax.swing.SpinnerNumberModel;
import javax.swing.SpringLayout;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.EmptyBorder;

import jchrest.architecture.Chrest;
import jchrest.lib.FileUtilities;
import jchrest.lib.ListPattern;
import jchrest.lib.PairedPattern;
import jchrest.lib.ParsingErrorException;
import jchrest.lib.Pattern;
import jchrest.lib.Scenes;

import org.knowm.xchart.CategoryChart;
import org.knowm.xchart.CategoryChartBuilder;
import org.knowm.xchart.XChartPanel;

/**
 * The main frame for the Chrest shell.
 */
public class Shell extends JFrame {
  private Chrest model;

  public Shell () {
    super ("CHREST (version 4)");

    model = new Chrest ();

    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    createMenuBar ();

    setContentPane( new JLabel ("Open some data to use shell") );

    setSize(600, 400);
    setLocationRelativeTo (null);
    setTheme ("Nimbus");
  }

  private void createMenuBar () {
    JMenuBar mb = new JMenuBar ();
    mb.add (createShellMenu ());
    mb.add (createDataMenu ());
    mb.add (createModelMenu ());
    setJMenuBar (mb);
  }

  /**
   * Simple action to display a message about this application.
   */
  class AboutAction extends AbstractAction implements ActionListener {
    private Shell parent;

    AboutAction (Shell parent) {
      super ("About", new ImageIcon (Shell.class.getResource("icons/About16.gif")));
      this.parent = parent;
    }

    public void actionPerformed (ActionEvent e) {
      JOptionPane.showMessageDialog (
          parent, 
          "<HTML><P>This program is a graphical interface to the <BR>" +
          "CHREST cognitive architecture.  You can load <BR>" +
          "data, train models, and visualise results in a <BR>" +
          "range of typical modelling problems for CHREST.</P>" +
          "<P><P>Copyright (c) 2010-20, Peter Lane.</P></P>" +
          "<P>Released under MIT License.</P>" + 
          "</HTML>",
          "About CHREST Shell v. 4.1.0", 
          JOptionPane.INFORMATION_MESSAGE);
    }

    private static final long serialVersionUID = 1L;
  }

  /**
   * Simple action to allow user to change look and feel.
   */
  class LookFeelAction extends AbstractAction implements ActionListener {
    private Shell parent;

    LookFeelAction (Shell parent) {
      super ("Theme");
      this.parent = parent;
    }

    public void actionPerformed (ActionEvent e) {
      Object [] possibleValues = new Object[UIManager.getInstalledLookAndFeels().length];
      int i = 0;
      for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels ()) {
        possibleValues[i] = info.getName ();
        i += 1;
      }
      String theme = (String)JOptionPane.showInputDialog (parent,
          "Select look and feel:",
          "Change theme",
          JOptionPane.INFORMATION_MESSAGE, 
          null,
          possibleValues,
          UIManager.getLookAndFeel().getName ());
      if (theme != null) {
        setTheme (theme);
      }
    }

    private static final long serialVersionUID = 1L;
  }

  /**
   * Action to load in a new data set from file.
   */
  class LoadDataAction extends AbstractAction implements ActionListener {
    private Shell parent;

    LoadDataAction (Shell parent) {
      super ("Open", new ImageIcon (Shell.class.getResource("icons/Open16.gif"))); 

      this.parent = parent;
    }

    public void actionPerformed (ActionEvent e) {
      (new LoadDataThread (parent)).execute ();
    }

    private static final long serialVersionUID = 1L;
  }

  enum Status {CANCELLED_SELECTION, CANCELLED_RUNNING, ERROR, OK};

  /**
   * Worker thread to handle loading the data.
   */
  private class LoadDataThread extends SwingWorker<Void, Void> {
    private Shell parent;
    private String task;
    private List<ListPattern> items;
    private List<PairedPattern> pairs;
    private Scenes scenes;
    private Status status = Status.OK;

    LoadDataThread (Shell parent) {
      this.parent = parent;
      task = "";
      items = null;
      pairs = null;
      scenes = null;
    }

    @Override
      public Void doInBackground () {
        File file = FileUtilities.getLoadFilename (parent);
        if (file == null) {
          status = Status.CANCELLED_SELECTION;
        } else {
          try {
            status = Status.OK; // assume all will be fine
            task = "";
            // add a monitor to the input stream, to show a message if input is taking a while
            InputStream inputStream = new ProgressMonitorInputStream(
                parent, 
                "Reading the input file", 
                new FileInputStream (file));
            BufferedReader input = new BufferedReader (new InputStreamReader (inputStream));

            String line = input.readLine ();
            if (line != null) {
              task = line.trim ();
            }

            if (task.equals ("recognise-and-learn")) {
              items = readItems (input, false);
            } else if (task.equals ("serial-anticipation")) {
              items = readItems (input, true);
            } else if (task.equals ("paired-associate")) {
              pairs = readPairedItems (input, false);
            } else if (task.equals ("categorisation")) {
              pairs = readPairedItems (input, true);
            } else if (task.equals ("visual-search")) {
              scenes = Scenes.read (input); // throws IOException if any problem
            } else if (task.equals ("visual-search-with-move")){
              scenes = Scenes.readWithMove (input); // throws IOException if any problem
            }
          } catch (InterruptedIOException ioe) {
            status = Status.CANCELLED_RUNNING; // flag cancelled error
          } catch (IOException ioe) {
            status = Status.ERROR; // flag an IO error
          }
        }
        return null;
      }

    @Override
      protected void done () {
        switch (status) {
          case CANCELLED_SELECTION:
            break;
          case ERROR:
            JOptionPane.showMessageDialog (parent, 
                "There was an error in processing your file", 
                "File error",
                JOptionPane.ERROR_MESSAGE);
            break;
          case CANCELLED_RUNNING:
            JOptionPane.showMessageDialog (parent, 
                "You cancelled the operation : no change", 
                "File Load Cancelled",
                JOptionPane.WARNING_MESSAGE);
            break;
          case OK:
            if (task.equals ("recognise-and-learn") && items != null) {
              parent.setContentPane (new RecogniseAndLearnDemo (model, items));
            } else if (task.equals ("serial-anticipation") && items != null) {
              parent.setContentPane (new PairedAssociateExperiment (model, PairedAssociateExperiment.makePairs(items)));
            } else if (task.equals ("paired-associate") && pairs != null) {
              parent.setContentPane (new PairedAssociateExperiment (model, pairs));
            } else if (task.equals ("categorisation") && pairs != null) {
              parent.setContentPane (new CategorisationExperiment (model, pairs));
            } else if (task.equals ("visual-search") && scenes != null) {
              parent.setContentPane (new VisualSearchPane (model, scenes));
            } else {
              JOptionPane.showMessageDialog (parent,
                  "Invalid task on first line of file",
                  "File error",
                  JOptionPane.ERROR_MESSAGE);
            }
            parent.validate ();
            break;
        }
      }

    private List<ListPattern> readItems (BufferedReader input, boolean verbal) throws IOException {
      List<ListPattern> items = new ArrayList<ListPattern> ();
      String line = input.readLine ();

      while (line != null) {
        ListPattern pattern;
        if (verbal) {
          pattern = Pattern.makeVerbalList (line.trim().split("[, ]"));
        } else {
          pattern = Pattern.makeVisualList (line.trim().split("[, ]"));
        }
        pattern.setFinished ();
        items.add (pattern);
        line = input.readLine ();
      } 

      return items;
    }

    // categorisation = false => make both verbal
    // categorisation = true  => make first visual, second verbal
    private List<PairedPattern> readPairedItems (BufferedReader input, boolean categorisation) throws IOException {
      List<PairedPattern> items = new ArrayList<PairedPattern> ();
      String line = input.readLine ();
      while (line != null) {
        String[] pair = line.split (":");
        if (pair.length != 2) throw new IOException (); // malformed pair
        ListPattern pat1;
        if (categorisation) {
          pat1 = Pattern.makeVisualList (pair[0].trim().split("[, ]"));
        } else {
          pat1 = Pattern.makeVerbalList (pair[0].trim().split("[, ]"));
        }
        pat1.setFinished ();
        ListPattern pat2 = Pattern.makeVerbalList (pair[1].trim().split("[, ]"));
        pat2.setFinished ();
        items.add (new PairedPattern (pat1, pat2));

        line = input.readLine ();
      }

      return items;
    }
  }

  /**
   * Action to clear data held in the model.
   */
  class ClearModelAction extends AbstractAction implements ActionListener {
    private Shell parent;

    ClearModelAction (Shell parent) {
      super ("Clear", new ImageIcon (Shell.class.getResource ("icons/Delete16.gif")));

      this.parent = parent;
    }

    public void actionPerformed (ActionEvent e) {
      if (JOptionPane.OK_OPTION == JOptionPane.showConfirmDialog (
            parent,
            "Are you sure you want to clear the model?",
            "Clear model?",
            JOptionPane.YES_NO_OPTION,
            JOptionPane.QUESTION_MESSAGE)) {
        model.clear ();
      }
    }

    private static final long serialVersionUID = 1L;
  }

  /**
   * Action to show a dialog to change properties of model.
   */
  class ModelPropertiesAction extends AbstractAction implements ActionListener {
    private Shell parent;

    ModelPropertiesAction (Shell parent) {
      super ("Properties", new ImageIcon (Shell.class.getResource("icons/Properties16.gif"))); 

      this.parent = parent;
    }

    public void actionPerformed (ActionEvent e) {
      if (JOptionPane.OK_OPTION == JOptionPane.showOptionDialog (parent, 
            properties(), 
            "CHREST: Model properties", 
            JOptionPane.OK_CANCEL_OPTION,
            JOptionPane.PLAIN_MESSAGE,
            null, null, 0)) {
        model.setAddLinkTime (((SpinnerNumberModel)addLinkTime.getModel()).getNumber().intValue ());
        model.setDiscriminationTime (((SpinnerNumberModel)discriminationTime.getModel()).getNumber().intValue ());
        model.setFamiliarisationTime (((SpinnerNumberModel)familiarisationTime.getModel()).getNumber().intValue ());
        model.setRho (((SpinnerNumberModel)rhoEntry.getModel()).getNumber().floatValue ());
        model.setVisualStmSize (((SpinnerNumberModel)visualStmSize.getModel()).getNumber().intValue ());
        model.setVerbalStmSize (((SpinnerNumberModel)verbalStmSize.getModel()).getNumber().intValue ());
        model.getPerceiver().setFieldOfView (((SpinnerNumberModel)fieldOfView.getModel()).getNumber().intValue ());
        model.setCreateSemanticLinks (createSemanticLinks.isSelected ());
        model.setCreateTemplates(createTemplates.isSelected ());
        model.setSimilarityThreshold(((SpinnerNumberModel)similarityThreshold.getModel()).getNumber().intValue ());
      }
    }

    private JSpinner addLinkTime;
    private JSpinner discriminationTime;
    private JSpinner familiarisationTime;
    private JSpinner rhoEntry;
    private JSpinner visualStmSize;
    private JSpinner verbalStmSize;
    private JSpinner fieldOfView;
    private JSpinner similarityThreshold;
    private JCheckBox createSemanticLinks;
    private JCheckBox createTemplates;

    private JPanel properties () {
      // -- create entry widgets
      addLinkTime = new JSpinner (new SpinnerNumberModel (model.getAddLinkTime (), 1, 100000, 1));
      discriminationTime = new JSpinner (new SpinnerNumberModel (model.getDiscriminationTime (), 1, 100000, 1));
      familiarisationTime = new JSpinner (new SpinnerNumberModel (model.getFamiliarisationTime (), 1, 100000, 1));
      rhoEntry = new JSpinner (new SpinnerNumberModel (model.getRho (), 0.0, 1.0, 0.1));
      visualStmSize = new JSpinner (new SpinnerNumberModel (model.getVisualStmSize (), 1, 10, 1));
      verbalStmSize = new JSpinner (new SpinnerNumberModel (model.getVerbalStmSize (), 1, 10, 1));
      fieldOfView = new JSpinner (new SpinnerNumberModel (model.getPerceiver().getFieldOfView (), 1, 100, 1));
      similarityThreshold = new JSpinner (new SpinnerNumberModel (model.getSimilarityThreshold (), 1, 100, 1));
      createSemanticLinks = new JCheckBox ("Use semantic links", model.getCreateSemanticLinks ());
      createTemplates = new JCheckBox ("Use templates", model.getCreateTemplates ());

      JPanel panel = new JPanel ();
      panel.setLayout (new SpringLayout ());
      Utilities.addLabel (panel, "Add link time (ms)", addLinkTime);
      Utilities.addLabel (panel, "Discrimination time (ms)", discriminationTime);
      Utilities.addLabel (panel, "Familiarisation time (ms)", familiarisationTime);
      Utilities.addLabel (panel, "Rho", rhoEntry);
      Utilities.addLabel (panel, "Visual STM size", visualStmSize);
      Utilities.addLabel (panel, "Verbal STM size", verbalStmSize);
      Utilities.addLabel (panel, "Field of view", fieldOfView);
      Utilities.addLabel (panel, "Similarity threshold", similarityThreshold);
      Utilities.addLabel (panel, "", createSemanticLinks);
      Utilities.addLabel (panel, "", createTemplates);

      Utilities.makeCompactGrid (panel, 10, 2, 3, 3, 10, 5);
      panel.setMaximumSize (panel.getPreferredSize ());

      return panel;
    }

    private static final long serialVersionUID = 1L;
  }

  /**
   * Action to save ltm from current model in Graphviz dot format.
   */
  class SaveLtmAsDotAction extends AbstractAction implements ActionListener {
    private Shell parent;

    SaveLtmAsDotAction (Shell parent) {
      super ("Save LTM (.dot)"); 

      this.parent = parent;
    }

    public void actionPerformed (ActionEvent e) {
      File file = FileUtilities.getSaveFilename (parent, "Save LTM (.dot)");
      if (file == null) return;
      try {
        FileWriter writer = new FileWriter (file);
        model.writeLtmAsDot (writer);
        writer.close ();
      } catch (IOException ioe) {
        JOptionPane.showMessageDialog (parent,
            "File " + file.getName () + 
            " could not be saved due to an error.",
            "Error: File save error",
            JOptionPane.ERROR_MESSAGE);
      }
    }

    private static final long serialVersionUID = 1L;
  }

  /**
   * Action to save test-link data from current model in VNA format.
   */
  class SaveModelAsVnaAction extends AbstractAction implements ActionListener {
    private Shell parent;

    SaveModelAsVnaAction (Shell parent) {
      super ("Save visual network (.VNA)"); 

      this.parent = parent;
    }

    public void actionPerformed (ActionEvent e) {
      File file = FileUtilities.getSaveFilename (parent, "Save visual network");
      if (file == null) return;
      try {
        FileWriter writer = new FileWriter (file);
        model.writeModelAsVna (writer);
        writer.close ();
      } catch (IOException ioe) {
        JOptionPane.showMessageDialog (parent,
            "File " + file.getName () + 
            " could not be saved due to an error.",
            "Error: File save error",
            JOptionPane.ERROR_MESSAGE);
      }
    }

    private static final long serialVersionUID = 1L;
  }

  /**
   * Action to save semantic links in current model as VNA.
   */
  class SaveModelSemanticLinksAsVnaAction extends AbstractAction implements ActionListener {
    private Shell parent;

    SaveModelSemanticLinksAsVnaAction (Shell parent) {
      super ("Save visual semantic links (.VNA)"); 

      this.parent = parent;
    }

    public void actionPerformed (ActionEvent e) {
      File file = FileUtilities.getSaveFilename (parent, "Save visual semantic links");
      if (file == null) return;
      try {
        FileWriter writer = new FileWriter (file);
        model.writeModelSemanticLinksAsVna (writer);
        writer.close ();
      } catch (IOException ioe) {
        JOptionPane.showMessageDialog (parent,
            "File " + file.getName () + 
            " could not be saved due to an error.",
            "Error: File save error",
            JOptionPane.ERROR_MESSAGE);
      }
    }

    private static final long serialVersionUID = 1L;
  }

  /**
   * Action to display information about the current model.
   */
  class ModelInformationAction extends AbstractAction implements ActionListener {
    private Shell parent;

    ModelInformationAction (Shell parent) {
      super ("Information", new ImageIcon (Shell.class.getResource("icons/Information16.gif")));

      this.parent = parent;
    }

    public void actionPerformed (ActionEvent e) {
      JPanel base = new JPanel ();
      base.setLayout (new GridLayout(1,1));

      JTabbedPane jtb = new JTabbedPane ();
      jtb.addTab ("Info", getInfoPane ());
      jtb.addTab ("Contents", getHistogramPane (model.getContentCounts(), "contents", "Histogram of Contents Sizes", "Contents size"));
      jtb.addTab ("Images", getHistogramPane (model.getImageCounts(), "images", "Histogram of Image Sizes", "Image size"));
      jtb.addTab ("Semantic links", getHistogramPane (model.getSemanticLinkCounts(), "semantic", "Histogram of Number of Semantic Links", "Number of semantic links"));
      base.add (jtb);

      JOptionPane pane = new JOptionPane (base, JOptionPane.INFORMATION_MESSAGE);
      JDialog dialog = pane.createDialog (parent, "CHREST: Model information");
      dialog.setResizable (true);
      dialog.setVisible (true);
    }

    private static final long serialVersionUID = 1L;
  }

  private JPanel getInfoPane () {
    DecimalFormat twoPlaces = new DecimalFormat("0.00");
    String[] headings = { "LTM", "Number of Nodes", "Average Depth", "Average Image Size" };
    String[][] data = { 
      {"Visual", ""+model.ltmVisualSize (), twoPlaces.format (model.getVisualLtmAverageDepth ()), 
        twoPlaces.format (model.getVisualLtmAverageImageSize ()) },
      {"Verbal", ""+model.ltmVerbalSize (), twoPlaces.format (model.getVerbalLtmAverageDepth ()),
        twoPlaces.format (model.getVerbalLtmAverageImageSize ()) },
      {"Action", ""+model.ltmActionSize (), twoPlaces.format (model.getActionLtmAverageDepth ()),
        twoPlaces.format (model.getActionLtmAverageImageSize ()) }
    };

    JPanel panel = new JPanel ();
    panel.setLayout (new BorderLayout ());

    var label = new JLabel ("The LTM contains " + model.getTotalLtmNodes () + 
        " nodes in total, of which " + model.countTemplates () + " are templates");
    label.setFont (new Font (Font.SANS_SERIF, Font.PLAIN, 15));
    label.setBorder (new EmptyBorder (10, 10, 10, 10));
    panel.add (label, BorderLayout.NORTH);
    panel.add (new JScrollPane (new JTable (data, headings)));

    return panel;
  }

  private JPanel getHistogramPane (Map<Integer, Integer> contentSizes, String label, String title, String xAxis) {
    int largest = 0;
    for (Integer key : contentSizes.keySet ()) {
      if (key > largest) largest = key;
    }
    var categories = new int[largest+1];
    var counts = new int[largest+1];

    for (int i = 0; i <= largest; ++i) {
      categories[i] = i;
      counts[i] = 0;
      if (contentSizes.containsKey (i)) {
        counts[i] = contentSizes.get (i);
      }
    }

    var chart = new CategoryChartBuilder().title(title).xAxisTitle(xAxis).yAxisTitle("Frequency").build ();
    chart.getStyler().setChartTitleFont(new Font(Font.SANS_SERIF, Font.BOLD, 20));
    chart.getStyler().setAxisTitleFont(new Font(Font.SANS_SERIF, Font.BOLD, 16));
    chart.getStyler().setAxisTickLabelsFont(new Font(Font.SANS_SERIF, Font.PLAIN, 14));
    chart.addSeries (label, categories, counts);

    JPanel panel = new JPanel ();
    panel.setLayout (new BorderLayout ());
    panel.add (new XChartPanel<CategoryChart> (chart)); 
    JButton saveButton = new JButton ("Save Data");
    saveButton.setToolTipText ("Save the histogram data to a CSV file");
    saveButton.addActionListener (new SaveHistogramActionListener (this, contentSizes));
    panel.add (saveButton, BorderLayout.SOUTH);
    return panel;
  }

  class SaveHistogramActionListener implements ActionListener {
    Shell parent;
    Map<Integer, Integer> data;

    public SaveHistogramActionListener (Shell parent, Map<Integer, Integer> data) {
      this.parent = parent;
      this.data = data;
    }
    
    public void actionPerformed (ActionEvent e) {
      File file = FileUtilities.getSaveFilename (parent, "Save histogram data");
      if (file == null) return;
      try {
        FileWriter writer = new FileWriter (file);
        for (Integer key : data.keySet ()) {
          writer.write ("" + key + ", " + data.get (key) + "\n");
        }
        writer.close ();
      } catch (IOException ioe) {
        JOptionPane.showMessageDialog (parent,
            "File " + file.getName () + 
            " could not be saved due to an error.",
            "Error: File save error",
            JOptionPane.ERROR_MESSAGE);
      }
    }
  }

  /** 
   * Action to display a separate frame with information about the current model.
   * Frame is composed of separate views onto the model, all using the observer 
   * design pattern to keep updated as the model changes.
   */
  class ViewModelAction extends AbstractAction implements ActionListener {
    private Shell parent;

    ViewModelAction (Shell parent) {
      super ("View", new ImageIcon (Shell.class.getResource("icons/Find16.gif")));

      this.parent = parent;
    }

    public void actionPerformed (ActionEvent e) {
      new ChrestView (parent, model);
    }

    private static final long serialVersionUID = 1L;
  }

  private JMenu createShellMenu () {
    JMenuItem exit = new JMenuItem ("Exit", new ImageIcon (Shell.class.getResource ("icons/Stop16.gif")));
    exit.addActionListener (new ActionListener () {
      public void actionPerformed (ActionEvent e) {
        System.exit (0);
      }
    });

    JMenu menu = new JMenu ("Shell");
    menu.setMnemonic (KeyEvent.VK_S);
    menu.add (new AboutAction (this));
    menu.getItem(0).setMnemonic (KeyEvent.VK_A);
    menu.add (new LookFeelAction (this));
    menu.getItem(1).setMnemonic (KeyEvent.VK_T);
    menu.add (new JSeparator ());
    menu.add (exit);
    menu.getItem(3).setMnemonic (KeyEvent.VK_X);

    menu.getItem(0).setAccelerator (KeyStroke.getKeyStroke('A', java.awt.event.InputEvent.CTRL_DOWN_MASK, false));
    menu.getItem(3).setAccelerator (KeyStroke.getKeyStroke('X', java.awt.event.InputEvent.CTRL_DOWN_MASK, false));
    return menu;
  }

  private JMenu createDataMenu () {
    JMenu menu = new JMenu ("Data");
    menu.setMnemonic (KeyEvent.VK_D);
    menu.add (new LoadDataAction (this));

    menu.getItem(0).setAccelerator (KeyStroke.getKeyStroke('O', java.awt.event.InputEvent.CTRL_DOWN_MASK, false));
    return menu;
  }

  private JMenu createModelMenu () {
    JMenu menu = new JMenu ("Model");
    menu.setMnemonic (KeyEvent.VK_M);
    menu.add (new ClearModelAction (this));
    menu.getItem(0).setMnemonic (KeyEvent.VK_C);

    JMenu submenu = new JMenu ("Save");
    submenu.setMnemonic (KeyEvent.VK_S);
    submenu.add (new SaveLtmAsDotAction (this));
    submenu.add (new JSeparator ());
    submenu.add (new SaveModelAsVnaAction (this));
    submenu.getItem(2).setMnemonic (KeyEvent.VK_N);
    submenu.add (new SaveModelSemanticLinksAsVnaAction (this));
    submenu.getItem(3).setMnemonic (KeyEvent.VK_L);
    menu.add (submenu);

    menu.add (new ModelPropertiesAction (this));
    menu.getItem(2).setMnemonic (KeyEvent.VK_P);
    menu.add (new JSeparator ());
    menu.add (new ModelInformationAction (this));
    menu.getItem(4).setMnemonic (KeyEvent.VK_I);
    menu.add (new ViewModelAction (this));
    menu.getItem(5).setMnemonic (KeyEvent.VK_V);

    return menu;
  }

  /**
   * Set theme of user interface to the one named.
   */
  private void setTheme (String theme) {
    try { 
      for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
        if (theme.equals(info.getName())) {
          UIManager.setLookAndFeel(info.getClassName());
          break;
        }
      }
    } catch (UnsupportedLookAndFeelException e) {
    } catch (ClassNotFoundException e) {
    } catch (InstantiationException e) {
    } catch (IllegalAccessException e) {
    }
    // make sure all components are updated
    SwingUtilities.updateComponentTreeUI(this);
    // SwingUtilities.updateComponentTreeUI(fileChooser); TODO: update FileUtilities filechooser
  }

  /**
   * main method to get everything started.
   * @param args from command line are ignored
   */
  public static void main (String[] args) {
    javax.swing.SwingUtilities.invokeLater(new Runnable() {
      public void run() { 
        Shell shell = new Shell (); 
        shell.setVisible (true);
      }
    });
  }

  private static final long serialVersionUID = 1L;  
}

