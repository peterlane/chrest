package jchrest.gui;

import java.awt.*;
import java.awt.font.*;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;

/**
 * Class gives a set of size values, to use when displaying the long-term memory.
 * The size value gives information on the font, margin etc to use in the display.
 */
public class Size {
	private final String name;
	private final Font font;
	private final int margin;
	private final int axis_gap_1; // gap between nodes in one direction
	private final int axis_gap_2; // gap between nodes in other direction
	private final int small_size; // size of box if _margin == 0
	
	private static List<Size> values = null;

	Size (String name, Font font, int margin, int axis_gap_1, int axis_gap_2, int small_size) {
		this.name = name;
		this.font = font;
		this.margin = margin;
		this.axis_gap_1 = axis_gap_1;
		this.axis_gap_2 = axis_gap_2;
		this.small_size = small_size;
	}

  /**
   * Descriptive string to display.
   * @return string describing this size
   */
	public String toString () { return name; }

  /**
   * Font to use at this size.
   * @return font
   */
	public Font getFont () { return font; }

  /**
   * Margin to use around displayed object.
   * @return margin size
   */
	public int getMargin () { return margin; }

  /**
   * Depending on the given orientation, return size of horizontal separator.
   *
   * @param orientation of display
   * @return size of horizontal separator
   */	public int getHorizontalSeparator (Orientation orientation) {
		return ((orientation == Orientation.HORIZONTAL) ? axis_gap_1 : axis_gap_2);
	}

  /**
   * Depending on the given orientation, return size of vertical separator.
   *
   * @param orientation of display
   * @return size of vertical separator
   */
	public int getVerticalSeparator (Orientation orientation) {
		return ((orientation == Orientation.HORIZONTAL) ? axis_gap_1 : axis_gap_2);
	}
	/** 
   * Return true if this size should be drawn as a box rather than with text 
   * and graphics. Indicator is whether a margin has been provided or not. 
   *
   * @return true if size is classed as small
   */
	public boolean isSmall () { return margin == 0; }
	/** 
   * Size of box for small size. 
   * @return size of small box
   */
	public int getSmallSize () { return small_size; }

  /**
   * Returns a predefined list of size values.
   *
   * @return list of size values
   */
	public static List<Size> getValues () { 
		if (values == null) {
			values = new ArrayList<Size> ();
			values.add (new Size ("Large Text", new Font ("Times", Font.PLAIN, 20), 7, 30, 12, 0));
			values.add (new Size ("Medium Text", new Font ("Times", Font.PLAIN, 13), 4, 15, 7, 0));
			values.add (new Size ("Small Text", new Font ("Times", Font.PLAIN, 9), 2, 8, 5, 0));
			values.add (new Size ("Small-10", null, 0, 7, 7, 10));
			values.add (new Size ("Small-5", null, 0, 5, 5, 5));
			values.add (new Size ("Small-2", null, 0, 2, 2, 2));
		}
		
		return values; 
	}

	/** 
   * Return the TextLayout for given string at this size.
   *
   * @param str to display
   * @param g the graphics context to drawn on
   * @return the text layout  
   */
  public TextLayout getTextLayout (String str, Graphics2D g) {
    FontRenderContext frc = g.getFontRenderContext ();
    return new TextLayout (str, getFont (), frc);
  }
	
	/** 
   * Return the bounding box for given string drawn at this size.
   *
   * @param str to display
   * @param g the graphics context to drawn on
   * @return the bounding box  
   */
  public Rectangle2D getTextBounds (String str, Graphics2D g) {
    Rectangle2D result;
    try {
      result = getTextLayout(str, g).getBounds ();
    } catch (NullPointerException npe) { // g often null before Swing set up fully
      result = new Rectangle2D.Double (0, 0, 0, 0);
    }
    return result;
  }

  /**
   * Get display height of given string.
   *
   * @param str to display
   * @param g the graphics context to draw on
   * @return display height of given string
   */
	public int getHeight (String str, Graphics2D g) {
		return (int)(getTextBounds(str, g).getHeight ());
	}

  /**
   * Get display width of given string.
   *
   * @param str to display
   * @param g the graphics context to draw on
   * @return display width of given string
   */
	public int getWidth (String str, Graphics2D g) {
		return (int)(getTextBounds(str, g).getWidth ());
	}
	
	/** Display given text at coordinates. 
	 * x, y represent the top left corner.  Add the desired margin. 
   *
   * @param g the graphics context to draw on
   * @param x coordinate of top left corner
   * @param y coordinate of top left corner
   * @param str to draw 
	 */
	public void drawText(Graphics2D g, float x, float y, String str) {
		float m = (float)(getMargin ());
		TextLayout layout = getTextLayout (str, g);
		layout.draw (g, x + m, y + getHeight (str, g) + m);
	}

}

