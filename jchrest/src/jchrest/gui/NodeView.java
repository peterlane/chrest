package jchrest.gui;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

import jchrest.architecture.Chrest;
import jchrest.architecture.Link;
import jchrest.architecture.Node;

public class NodeView extends JFrame implements PropertyChangeListener {
  private final Chrest model;
  private final Node node;
  private final JLabel contentsLabel;
  private final JLabel imageLabel;
  private final JLabel associatedNode;
  private final JLabel namedBy;
  private final DefaultListModel<Node> childLinksView, similarityLinksView;
  private final JList<Node> childLinks, similarityLinks;

  public NodeView (Chrest model, Node node) {
    this.model = model;
    this.node = node;
    this.node.addPropertyChangeListener (this);
    setTitle ("Node: " + this.node.getReference ());
    addWindowListener (new WindowAdapter () {
      public void windowClosing (WindowEvent e) {
        ((NodeView)e.getWindow()).close ();
      }
    });

    // create and add the widgets
    contentsLabel = new JLabel (this.node.getContents().toString ());
    imageLabel = new JLabel (this.node.getImage().toString ());

    JPanel fields = new JPanel ();
    fields.setLayout (new GridLayout (4, 2));
    fields.add (new JLabel ("Contents: ", SwingConstants.RIGHT));
    fields.add (contentsLabel);
    fields.add (new JLabel ("Image: ", SwingConstants.RIGHT));
    fields.add (imageLabel);
    
    associatedNode = new JLabel ("");
    associatedNode.setBorder (new CompoundBorder (new EmptyBorder (3, 3, 3, 3), new EtchedBorder ()));
    if (this.node.getAssociatedNode () != null) {
      associatedNode.setIcon (new NodeIcon (this.node.getAssociatedNode (), associatedNode));
    }
    fields.add (new JLabel ("Assocated node: ", SwingConstants.RIGHT));
    fields.add (associatedNode);

    namedBy = new JLabel ("");
    namedBy.setBorder (new CompoundBorder (new EmptyBorder (3, 3, 3, 3), new EtchedBorder ()));
    if (this.node.getNamedBy () != null) {
      namedBy.setIcon (new NodeIcon (this.node.getNamedBy (), namedBy));
    }
    fields.add (new JLabel ("Named by: ", SwingConstants.RIGHT));
    fields.add (namedBy);

    childLinksView = new DefaultListModel<Node> ();
    childLinks = new JList<Node> (childLinksView);
    childLinks.setCellRenderer (new ListNodeRenderer (this.model));
    childLinks.setLayoutOrientation (JList.HORIZONTAL_WRAP);
    childLinks.setVisibleRowCount (1);
    childLinks.addMouseListener(new MouseAdapter () {
      public void mouseClicked(MouseEvent evt) {
        if (evt.getClickCount() == 2) { 
          int index = childLinks.locationToIndex(evt.getPoint());
          new NodeView (model, childLinksView.getElementAt (index));
        }
      }
    });

    similarityLinksView = new DefaultListModel<Node> ();
    similarityLinks = new JList<Node> (childLinksView);
    similarityLinks.setCellRenderer (new ListNodeRenderer (this.model));
    similarityLinks.setLayoutOrientation (JList.HORIZONTAL_WRAP);
    childLinks.setVisibleRowCount (1);
    similarityLinks.addMouseListener(new MouseAdapter () {
      public void mouseClicked(MouseEvent evt) {
        if (evt.getClickCount() == 2) { 
          int index = similarityLinks.locationToIndex(evt.getPoint());
          new NodeView (model, similarityLinksView.getElementAt (index));
        }
      }
    });

    setLayout (new BorderLayout ());
    add (fields, BorderLayout.NORTH);
    add (new JScrollPane (childLinks));
    add (new JScrollPane (similarityLinks), BorderLayout.SOUTH);

    pack ();
    setVisible (true);

    updateDisplays ();
  }

  void close () {
    node.removePropertyChangeListener (this);
    if (node.getAssociatedNode () != null) {
      node.getAssociatedNode ().removePropertyChangeListener (this);
    }
    if (node.getNamedBy () != null) {
      node.getNamedBy().removePropertyChangeListener (this);
    }
    for (Link link : node.getChildren ()) {
      link.getChildNode().removePropertyChangeListener (this);
    }
    for (Node semanticNode : node.getSemanticLinks ()) {
      semanticNode.removePropertyChangeListener (this);
    }
    setVisible (false);
    dispose ();
  }

  private void updateDisplays () {
    imageLabel.setText (node.getImage().toString ());
    if (node.getAssociatedNode () != null) {
      associatedNode.setIcon (new NodeIcon (node.getAssociatedNode (), associatedNode));
      node.getAssociatedNode().addPropertyChangeListener (this);
    }
    if (node.getNamedBy () != null) {
      namedBy.setIcon (new NodeIcon (node.getNamedBy (), namedBy));
      node.getNamedBy().addPropertyChangeListener (this);
    }

    childLinksView.clear ();
    for (Link link: node.getChildren ()) {
      childLinksView.addElement (link.getChildNode ());
      link.getChildNode().addPropertyChangeListener (this);
    }
    childLinks.setModel (childLinksView);

    similarityLinksView.clear ();
    for (Node semanticNode : node.getSemanticLinks ()) {
      similarityLinksView.addElement (semanticNode);
      semanticNode.addPropertyChangeListener (this);
    }
    similarityLinks.setModel (similarityLinksView);
  }

  public void propertyChange (PropertyChangeEvent e) {
    if (e.getPropertyName().equals("close")) {
      close ();
    } else {
      // update displays
      updateDisplays ();
    }
  }

  private static final long serialVersionUID = 1L;  
}

