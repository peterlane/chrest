package jchrest.gui;

import jchrest.architecture.Chrest;
import jchrest.lib.ListPattern;
import jchrest.lib.PairedPattern;
import jchrest.lib.Pattern;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.table.*;

/**
 * This panel provides an interface for running categorisation experiments.
 */
public class CategorisationExperiment extends JPanel {
  private final Chrest model;
  private final List<PairedPattern> patterns;

  public CategorisationExperiment (Chrest model, List<PairedPattern> patterns) {
    super ();
    
    this.model = model;
    this.patterns = patterns;

    setLayout (new GridLayout (1, 1));
    JSplitPane jsp = new JSplitPane (JSplitPane.HORIZONTAL_SPLIT, createRunExperimentView (), createProtocolView ());
    jsp.setOneTouchExpandable (true);

    add (jsp);
  }

  private Map<PairedPattern, JCheckBox> trainingSelection; 

  private JPanel createListView () {
    trainingSelection = new HashMap<PairedPattern, JCheckBox> ();

    JPanel panel = new JPanel ();
    panel.setBorder (new TitledBorder ("Categorisation data"));
    panel.setLayout (new GridLayout (1, 1));

    JPanel pairsPanel = new JPanel ();
    pairsPanel.setLayout (new GridLayout (patterns.size(), 3));
    for (PairedPattern pair : patterns) {
      pairsPanel.add (new JLabel (pair.getFirst().toString ()));
      pairsPanel.add (new JLabel (pair.getSecond().toString ()));
      JCheckBox cb = new JCheckBox ("Training");
      cb.setSelected (true);
      pairsPanel.add (cb);
      trainingSelection.put (pair, cb);
    }

    panel.add (new JScrollPane (pairsPanel));
    return panel;        
  } 

  private JCheckBox randomOrder;
  private List<List<ListPattern>> responses;
  private JScrollBar protocolHorizontalBar;
  private JTable protocol;

  class RestartAction extends AbstractAction implements ActionListener {
    RestartAction () {
      super ("Restart");
    }

    public void actionPerformed (ActionEvent e) {
      model.clear ();
      responses.clear ();

      updateControls ();
    }

    private static final long serialVersionUID = 1L;
  }

  class RunTrialAction extends AbstractAction implements ActionListener {
    RunTrialAction () {
      super ("Run Trial");
    }

    private List<PairedPattern> preparePatterns () {
      var result = new ArrayList<PairedPattern> ();
      java.util.Random gen = new java.util.Random ();
      for (PairedPattern pattern : patterns) {
        if (trainingSelection.get(pattern).isSelected ()) {
          if (randomOrder.isSelected ()) {
            result.add (gen.nextInt (patterns.size () + 1), pattern);
          } else {
            result.add (pattern);
          }
        }
      }

      return result;
    }

    private void collectResponses () {
      var theseResponses = new ArrayList<ListPattern> ();
      for (PairedPattern pair : patterns) {
        ListPattern response = model.namePattern (pair.getFirst ());
        if (response != null) {
          theseResponses.add (response);
        } else {
          theseResponses.add (Pattern.makeVisualList (new String[]{"NONE"}));
        }
      }
      responses.add (theseResponses);
    }

    public void actionPerformed (ActionEvent e) {
      model.freeze (); // save all gui updates to the end
      collectResponses ();
      for (PairedPattern pair : preparePatterns ()) {
        model.learnAndNamePatterns (pair.getFirst (), pair.getSecond ());
      }
      updateControls ();
    }

    private static final long serialVersionUID = 1L;
  }

  private void updateControls () {
    ((AbstractTableModel)protocol.getModel()).fireTableStructureChanged ();
    model.unfreeze ();
  }

  private JPanel createControls () {
    randomOrder = new JCheckBox ("Random order");
    randomOrder.setToolTipText ("Set this to pass pairs to model in a random order");
        JButton restart = new JButton (new RestartAction ());
    restart.setToolTipText ("Reset the experiment and clear the model");
    JButton runTrial = new JButton (new RunTrialAction ());
    runTrial.setToolTipText ("Pass each stimulus-response pair once against the model");

    JPanel controls = new JPanel ();
    controls.setLayout (new GridLayout (2, 2, 10, 3));
    controls.add (randomOrder);
    controls.add (restart);
    controls.add (new JLabel (""));
    controls.add (runTrial);

    return controls;
  }

  private JPanel createRunExperimentView () {
    JPanel panel = new JPanel ();
    panel.setLayout (new BorderLayout ());
    panel.add (createListView ());
    panel.add (createControls (), BorderLayout.SOUTH);

    return panel;
  }

  private void createProtocolTable () {
    TableModel tm = new AbstractTableModel () {
      // include a row for the number of errors
      public int getRowCount () {
        return 1 + patterns.size ();
      }
      
      // include two columns for the stimulus and target response
      public int getColumnCount () {
        return 2 + responses.size (); 
      }
      
      // compute the total number of errors in given column of responses
      private int getErrors (int trial) {
        int errors = 0;
        for (int i = 0, n = patterns.size (); i < n; ++i) {
          if (responses.get(trial).get(i).equals(patterns.get(i).getSecond ())) {
          } else {
            errors += 1;
          }
        }
        return errors;
      }

      public Object getValueAt (int row, int column) {
        if (column == 0) {
          if (row == patterns.size ()) {
            return "";
          } else {
            return patterns.get(row).getFirst ();
          }
        } else if (column == 1) {
          if (row == patterns.size ()) {
            return "Errors:";
          } else {
            return patterns.get(row).getSecond ();
          }
        } else {
          if (row == patterns.size ()) {
            return "" + getErrors (column-2);
          } else {
            return responses.get(column-2).get(row).toString ();
          }
        }
      }

      public String getColumnName (int column) {
        if (column == 0) {
          return "Source";
        } else if (column == 1) {
          return "Target";
        } else {
          return "Trial " + (column - 1);
        }
      }

      public void fireTableStructureChanged() {
        super.fireTableStructureChanged ();
        protocolHorizontalBar.setValue (protocolHorizontalBar.getMaximum ());
      }
    };
    protocol = new JTable (tm);
    protocol.setAutoResizeMode (JTable.AUTO_RESIZE_OFF);
  }

  private JPanel createProtocolView () {
    JPanel panel = new JPanel ();
    panel.setBorder (new TitledBorder ("Protocol"));
    panel.setLayout (new GridLayout (1, 1));
    responses = new ArrayList<List<ListPattern>> ();
    createProtocolTable ();
    JScrollPane scrollPane = new JScrollPane (protocol);
    protocolHorizontalBar = scrollPane.getHorizontalScrollBar ();
    panel.add (scrollPane);

    return panel;
  }

  private static final long serialVersionUID = 1L;
}
