package jchrest.gui;

import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.*;
import javax.swing.border.*;

import jchrest.architecture.Chrest;
import jchrest.architecture.Node;
import jchrest.lib.ChessDomain;
import jchrest.lib.FileUtilities;
import jchrest.lib.Fixation;
import jchrest.lib.GenericDomain;
import jchrest.lib.ItemSquarePattern;
import jchrest.lib.Scene;
import jchrest.lib.Scenes;

import org.knowm.xchart.XChartPanel;
import org.knowm.xchart.XYChart;
import org.knowm.xchart.XYChartBuilder;
import org.knowm.xchart.style.Styler.LegendPosition;

/**
 * This panel provides an interface for building and testing  
 * models on visual search problems.
 */
public class VisualSearchPane extends JPanel {
  private final Chrest model;
  private final Scenes scenes;
  private final SceneDisplay sceneDisplay;

  private List<Integer> patternCount; // number of patterns seen
  private List<Integer> networkSizes; // corresponding network size
  private final XYChart growthChart;
  private final XChartPanel<XYChart> chartPanel;

  public VisualSearchPane (Chrest model, Scenes scenes) {
    super ();
    assert scenes.size () > 0 : "VisualSearchPane needs at least one scene";

    this.model = model;
    this.scenes = scenes;
    this.model.getPerceiver().setScene (scenes.get (0));
    sceneDisplay = new SceneDisplay (scenes.get (0));
    domainSelector = new JComboBox<String> (new String[]{"Generic", "Chess"});
    domainSelector.addActionListener (new AbstractAction () {
      public void actionPerformed (ActionEvent e){
        int index = domainSelector.getSelectedIndex ();
        if (index == 0) {
          model.setDomain (new GenericDomain ());
        } else { // if (index == 1) 
          model.setDomain (new ChessDomain ());
        }
      }
    });

    // create variables for network growth chart
    patternCount = new ArrayList<Integer> ();
    patternCount.add (0);
    networkSizes = new ArrayList<Integer> ();
    networkSizes.add (0);
    growthChart = new XYChartBuilder().title("Plot of network size vs. number of training patterns")
      .xAxisTitle("Number of training patterns")
      .yAxisTitle("Network size")
      .build ();
    growthChart.getStyler().setChartTitleFont(new Font(Font.SANS_SERIF, Font.BOLD, 20));
    growthChart.getStyler().setAxisTitleFont(new Font(Font.SANS_SERIF, Font.BOLD, 16));
    growthChart.getStyler().setAxisTickLabelsFont(new Font(Font.SANS_SERIF, Font.PLAIN, 14));
    growthChart.addSeries ("CHREST Model", patternCount, networkSizes);
    chartPanel = new XChartPanel<XYChart> (growthChart);
    
    JTabbedPane jtb = new JTabbedPane ();
    jtb.addTab ("Train", trainPanel ());
    jtb.addTab ("Recall", recallPanel ());
    jtb.addTab ("Log", logPanel ());
    jtb.addTab ("Analyse", analysePanel ());

    setLayout (new BorderLayout ());
    add (jtb);
  }

  // -- set up the training panel
  private JPanel trainPanel () {
    JPanel panel = new JPanel ();
    panel.setLayout (new BoxLayout (panel, BoxLayout.Y_AXIS));

    panel.add (constructTrainingOptions ());
    panel.add (runTrainingButtons ());
    panel.add (chartPanel);

    return panel;
  }

  private JSpinner maxTrainingCycles;
  private JSpinner numFixations;
  private JSpinner maxNetworkSize;

  private JPanel constructTrainingOptions () {
    maxTrainingCycles = new JSpinner (new SpinnerNumberModel (5, 1, 1000, 1));
    numFixations = new JSpinner (new SpinnerNumberModel (20, 1, 1000, 1));
    maxNetworkSize = new JSpinner (new SpinnerNumberModel (100000, 1, 10000000, 1));

    JPanel panel = new JPanel ();
    panel.setLayout (new SpringLayout ());
    Utilities.addLabel (panel, "Domain of scenes:", domainSelector);
    Utilities.addLabel (panel, "Number of scenes:", new JLabel ("" + scenes.size ()));
    Utilities.addLabel (panel, "Maximum training cycles:", maxTrainingCycles);
    Utilities.addLabel (panel, "Number of fixations per scene:", numFixations);
    Utilities.addLabel (panel, "Maximum network size:", maxNetworkSize);

    Utilities.makeCompactGrid (panel, 5, 2, 3, 3, 10, 5);
    panel.setMaximumSize (panel.getPreferredSize ());

    JPanel ePanel = new JPanel ();
    ePanel.setLayout (new GridLayout (1, 1));
    ePanel.add (panel);

    return ePanel;
  }

  private JPanel runTrainingButtons () {
    JPanel panel = new JPanel ();

    trainAction = new TrainAction ();
    JButton trainButton = new JButton (trainAction);
    trainButton.setToolTipText ("Train a fresh CHREST model up to the given network size");
    stopAction = new StopAction ();
    stopAction.setEnabled (false);
    JButton stopButton = new JButton (stopAction);
    stopButton.setToolTipText ("Stop the current training");
    feedback = new JProgressBar (0, 100);
    feedback.setToolTipText ("Proportion of target network size");
    feedback.setValue (0);
    feedback.setStringPainted (true);
    panel.add (trainButton);
    panel.add (stopButton);
    panel.add (feedback);

    return panel;
  }

  private class Pair {
    private int first, second;

    Pair (int first, int second) {
      this.first = first;
      this.second = second;
    }

    int getFirst () {
      return first;
    }

    int getSecond () {
      return second;
    }
  }

  private class TrainingThread extends SwingWorker<List<Pair>, Pair> {
    private Chrest model;
    private Scenes scenes;
    private int maxCycles, maxSize, numFixations;

    TrainingThread (Chrest model, Scenes scenes, int maxCycles, int maxSize, int numFixations) {
      this.model = model;
      this.scenes = scenes;
      this.maxCycles = maxCycles;
      this.maxSize = maxSize;
      this.numFixations = numFixations;
    }

    @Override
      public List<Pair> doInBackground () {
        model.clear ();
        model.freeze ();
        List<Pair> results = new ArrayList<> ();
        Pair result = new Pair (0, 0);
        results.add (result);
        publish (result);

        int stepSize = (maxCycles * scenes.size ()) / 100;

        int cycle = 0;
        int positionsSeen = 0;
        while (
            (cycle < maxCycles) && 
            (model.getTotalLtmNodes () < maxSize) &&
            !isCancelled ()) {
          for (int i = 0, lastSceneIndex = scenes.size (); 
              i < lastSceneIndex && (model.getTotalLtmNodes () < maxSize) && !isCancelled (); 
              i++) {
            model.learnScene (scenes.get (i), numFixations);
            positionsSeen += 1;
            if (positionsSeen % stepSize == 0) {
              result = new Pair (positionsSeen, model.getTotalLtmNodes ());
              publish (result);
              setProgress (100 * model.getTotalLtmNodes () / maxSize);
              results.add (result);
            }
          }
          cycle += 1;
            }
        model.constructTemplates ();
        
        result = new Pair (positionsSeen, model.getTotalLtmNodes ());
        results.add (result);
        publish (result);

        return results;
      }

    @Override
      protected void process (List<Pair> results) {
        for (Pair pair : results) {
          patternCount.add (pair.getFirst ());
          networkSizes.add (pair.getSecond ());
        }
        growthChart.updateXYSeries ("CHREST Model", patternCount, networkSizes, null);
        chartPanel.repaint ();
      }

    protected void done () {
      feedback.setValue (100);
      stopAction.setEnabled (false);
      trainAction.setEnabled (true);
      model.unfreeze ();
    }

    private static final long serialVersionUID = 1L;    
  }

  private class TrainAction extends AbstractAction implements ActionListener {
    public TrainAction () {
      super ("Train");
    }

    private int getNumFixations () {
      return ((SpinnerNumberModel)(numFixations.getModel())).getNumber().intValue ();
    }

    private int getMaxCycles () {
      return ((SpinnerNumberModel)(maxTrainingCycles.getModel())).getNumber().intValue ();
    }

    private int getMaxNetworkSize () {
      return ((SpinnerNumberModel)(maxNetworkSize.getModel())).getNumber().intValue ();
    }

    public void actionPerformed (ActionEvent e) {
      task = new TrainingThread (model, scenes, getMaxCycles (), getMaxNetworkSize (), getNumFixations ());
      task.addPropertyChangeListener(
          new java.beans.PropertyChangeListener() {
            public  void propertyChange(java.beans.PropertyChangeEvent evt) {
              if ("progress".equals(evt.getPropertyName())) {
                feedback.setValue ((Integer)evt.getNewValue());
              }
            }
          });

      patternCount.clear (); // make sure we start graph afresh
      patternCount.add (0);
      networkSizes.clear (); // make sure we start graph afresh
      networkSizes.add (0);

      feedback.setValue (0);
      trainAction.setEnabled (false);
      stopAction.setEnabled (true);

      task.execute ();
    }

    private static final long serialVersionUID = 1L;
  }

  private TrainingThread task;
  private JProgressBar feedback;
  private TrainAction trainAction;
  private StopAction stopAction;

  private class StopAction extends AbstractAction implements ActionListener {
    public StopAction () {
      super ("Stop");
    }

    public void actionPerformed (ActionEvent e) {
      task.cancel (true);
      trainAction.setEnabled (true);
      stopAction.setEnabled (false);
    }

    private static final long serialVersionUID = 1L;
  }

  private JLabel recallSceneLabel;
  private SceneDisplay recalledSceneDisplay;

  // -- set up the recall panel
  private JPanel recallPanel () {
    JPanel panel = new JPanel ();
    panel.setLayout (new GridLayout (1, 1));

    JSplitPane sp = new JSplitPane (
        JSplitPane.VERTICAL_SPLIT, 
        recallSetupPanel (),
        recallResultsPanel ());

    panel.add (sp);

    return panel;
  }

  private JPanel recallSetupPanel () {
    JPanel panel = new JPanel ();
    panel.setLayout (new BorderLayout ());

    panel.add (constructSelector (), BorderLayout.NORTH);
    panel.add (sceneDisplay);
    panel.add (constructButtons (), BorderLayout.EAST);

    return panel;
  }

  private JLabel precision, recall, omission, commission;
  private JPanel recallResultsPanel () {
    recallSceneLabel = new JLabel ("RECALLED SCENE");
    recalledSceneDisplay = new SceneDisplay (new Scene ("empty", 
          scenes.get(0).getHeight (), 
          scenes.get(0).getWidth ()));
    precision = new JLabel ("");
    recall = new JLabel ("");
    omission = new JLabel ("");
    commission = new JLabel ("");

    JPanel panel = new JPanel ();
    panel.setLayout (new BorderLayout ());

    JPanel statistics = new JPanel ();
    // Note: GridLayout used because it keeps a fixed size for the 
    // right-hand side, whereas SpringLayout dynamically changes.
    statistics.setLayout (new GridLayout (4, 2));
    statistics.add (new JLabel ("Precision: ", SwingConstants.RIGHT));
    statistics.add (precision);
    statistics.add (new JLabel ("Recall: ", SwingConstants.RIGHT));
    statistics.add (recall);
    statistics.add (new JLabel ("Errors of omission: ", SwingConstants.RIGHT));
    statistics.add (omission);
    statistics.add (new JLabel ("Errors of commission: ", SwingConstants.RIGHT));
    statistics.add (commission);

    // stop the statistics panel spreading out
    JPanel statisticsPanel = new JPanel ();
    statisticsPanel.setLayout (new BorderLayout ());
    statisticsPanel.add (statistics, BorderLayout.NORTH);

    panel.add (recallSceneLabel, BorderLayout.NORTH);
    panel.add (recalledSceneDisplay);
    panel.add (statisticsPanel, BorderLayout.EAST);

    return panel;
  }

  private JComboBox<String> domainSelector;
  private JComboBox<String> sceneSelector;

  private JPanel constructSelector () {
    JPanel panel = new JPanel ();
    panel.setLayout (new BorderLayout ());
    panel.add (new JLabel ("Scene: "), BorderLayout.WEST);

    sceneSelector = new JComboBox<String> (scenes.getSceneNames ());
    sceneSelector.addActionListener (new AbstractAction () {
      public void actionPerformed (ActionEvent e) {
        Scene newScene = scenes.get (sceneSelector.getSelectedIndex ());
        model.getPerceiver().setScene (newScene);
        sceneDisplay.updateScene (newScene);
      }
    });
    panel.add (sceneSelector);

    return panel;
  }

  class AnalyseAction extends AbstractAction implements ActionListener {
    private JSpinner numFixations;

    public AnalyseAction (JSpinner numFixations) {
      super ("Analyse Scenes");

      this.numFixations = numFixations;
    }

    public void actionPerformed (ActionEvent e) {
       analysisTask = new AnalysisThread (numFixations);
       analysisTask.addPropertyChangeListener(
           new java.beans.PropertyChangeListener() {
             public void propertyChange(java.beans.PropertyChangeEvent evt) {
               if ("progress".equals(evt.getPropertyName())) {
                 feedbackAnalysis.setValue ((Integer)evt.getNewValue());
               }
             }
           });
       feedbackAnalysis.setValue (0);
       analyseAction.setEnabled (false);
       stopAnalysisAction.setEnabled (true);

       analysisTask.execute ();
    }

    private static final long serialVersionUID = 1L;    
  }

  private class AnalysisThread extends SwingWorker<Void, Void> {
    private Map<Integer, Integer> recallFrequencies;
    private JSpinner numFixations;

    public AnalysisThread (JSpinner numFixations) {
      this.numFixations = numFixations;
    }

    @Override
      public Void doInBackground () {
        recallFrequencies = new HashMap<Integer, Integer> ();

        // loop through each scene, doing recall
        for (int i = 0; i < scenes.size () && !isCancelled (); i++) {
          Scene scene = scenes.get (i);
          model.scanScene (scene, ((SpinnerNumberModel)(numFixations.getModel())).getNumber().intValue ());
          for (Node node : model.getPerceiver().getRecognisedNodes ()) {
            int id = node.getReference ();
            if (recallFrequencies.containsKey (id)) {
              recallFrequencies.put (id, recallFrequencies.get(id) + 1);
            } else {
              recallFrequencies.put (id, 1);
            }
          }
          setProgress (100 * i / scenes.size ());
        }
        return null;
      }

    protected void done () {
      // finally, show results in window
      for (Integer key : recallFrequencies.keySet ()) {
        analysisScreen.append ("" + key + ", " + recallFrequencies.get(key) + "\n");
      }

      feedbackAnalysis.setValue (100);
      stopAnalysisAction.setEnabled (false);
      analyseAction.setEnabled (true);
    }
  }

  class RecallAction extends AbstractAction implements ActionListener {
    private JSpinner numFixations;

    public RecallAction (JSpinner numFixations) {
      super ("Recall Scene");

      this.numFixations = numFixations;
    }

    public void actionPerformed (ActionEvent e) {
      Scene scene =  scenes.get(sceneSelector.getSelectedIndex ());
      Scene recalledScene = model.scanScene (scene, ((SpinnerNumberModel)(numFixations.getModel())).getNumber().intValue ());
      recallSceneLabel.setText (recalledScene.getName ());
      recalledSceneDisplay.updateScene (recalledScene);
      precision.setText ("" + scene.computePrecision (recalledScene));
      recall.setText ("" + scene.computeRecall (recalledScene));
      omission.setText ("" + scene.computeErrorsOfOmission (recalledScene));
      commission.setText ("" + scene.computeErrorsOfCommission (recalledScene));
      sceneDisplay.setFixations (model.getPerceiver().getFixations ());
      // log results
      addLog ("\n" + recalledScene.getName ());
      addLog ("Fixations: ");
      for (Fixation fixation : model.getPerceiver().getFixations ()) {
        addLog ("   " + fixation.toString ());
      }
      addLog ("Chunks used: ");
      for (Node node : model.getVisualStm()) {
        addLog ("   " + "Node: " + node.getReference() + " " + node.getImage().toString ());
        if (model.getCreateTemplates() && node.isTemplate ()) {
          addLog ("     Template:");
          addLog ("        filled item slots: ");
          for (ItemSquarePattern isp : node.getFilledItemSlots ()) {
            addLog ("         " + isp.toString ());
          }
          addLog ("        filled position slots: ");
          for (ItemSquarePattern isp : node.getFilledPositionSlots ()) {
            addLog ("         " + isp.toString ());
          }

        }
      }
      addLog ("Performance: ");
      addLog ("   Precision: " + scene.computePrecision (recalledScene));
      addLog ("   Recall: " + scene.computeRecall (recalledScene));
      addLog ("   Errors of Omission: " + scene.computeErrorsOfOmission (recalledScene));
      addLog ("   Errors of Commission: " + scene.computeErrorsOfCommission (recalledScene));
    }

    private static final long serialVersionUID = 1L;    
  }

  private JPanel constructButtons () {

    Box buttons = Box.createVerticalBox ();
    JSpinner numFixations = new JSpinner (new SpinnerNumberModel (20, 1, 1000, 1));
    
    JPanel labelledSpinner = new JPanel ();
    labelledSpinner.setLayout (new GridLayout (1, 2));
    labelledSpinner.add (new JLabel ("Number of fixations: "));
    labelledSpinner.add (numFixations);

    JButton recallButton = new JButton (new RecallAction (numFixations));
    recallButton.setToolTipText ("Scan shown scene and display results");

    final JCheckBox showFixations = new JCheckBox ("Show fixations", false);
    showFixations.setToolTipText ("Show fixations for recalled scene");
    showFixations.addActionListener (new ActionListener () {
      public void actionPerformed (ActionEvent e) {
        sceneDisplay.setShowFixations (showFixations.isSelected ());
      }
    });

    buttons.add (Box.createRigidArea (new Dimension (0, 20)));
    buttons.add (labelledSpinner);
    buttons.add (recallButton);
    buttons.add (Box.createRigidArea (new Dimension (0, 20)));
    buttons.add (showFixations);
    buttons.add (Box.createRigidArea (new Dimension (0, 20)));

    JPanel panel = new JPanel ();
    panel.setLayout (new BorderLayout ());
    panel.add (buttons, BorderLayout.NORTH);

    return panel;
  }

  // -- setup the log display
  private JTextArea logScreen;

  private JPanel logPanel () {
    logScreen = new JTextArea ();
    JPanel panel = new JPanel ();
    panel.setLayout (new BorderLayout ());
    panel.add (new JScrollPane (logScreen));

    Box buttons = Box.createHorizontalBox ();
    buttons.add (new JButton (new SaveAction (logScreen)));
    buttons.add (new JButton (new ClearAction (logScreen)));
    panel.add (buttons, BorderLayout.SOUTH);

    return panel;
  }

  private class SaveAction extends AbstractAction implements ActionListener {
    private JTextArea text;

    public SaveAction (JTextArea text) {
      super ("Save");
      this.text = text;
    }

    public void actionPerformed (ActionEvent e) {
      File file = FileUtilities.getSaveFilename (text);
      if (file != null) {
        try {
          FileWriter fw = new FileWriter (file);
          text.write (fw);
          fw.close ();
        } catch (IOException ioe) {
          ; // ignore any problems
        }
      }
    }

    private static final long serialVersionUID = 1L;
  }

  private class ClearAction extends AbstractAction implements ActionListener {
    private JTextArea text;

    public ClearAction (JTextArea text) {
      super ("Clear");
      this.text = text;
    }

    public void actionPerformed (ActionEvent e) {
      text.setText ("");
    }

    private static final long serialVersionUID = 1L;    
  }

  private void addLog (String string) {
    logScreen.append (string + "\n");
  }

  // -- setup the analyse display
  private JTextArea analysisScreen;

  private JPanel analysePanel () {
    analysisScreen = new JTextArea ();

    Box buttons = Box.createVerticalBox ();
    buttons.add (new JLabel ("Find frequency of nodes used by model when scanning scenes"));

    JSpinner numFixations = new JSpinner (new SpinnerNumberModel (20, 1, 1000, 1));

    JPanel labelledSpinner = new JPanel ();
    labelledSpinner.setLayout (new GridLayout (1, 2));
    labelledSpinner.add (new JLabel ("Number of fixations: ", SwingConstants.RIGHT));
    labelledSpinner.add (numFixations);

    buttons.add (labelledSpinner);
    buttons.add (runAnalysisButtons (numFixations));

    // main panel
    JPanel panel = new JPanel ();
    panel.setLayout (new BorderLayout ());

    panel.add (buttons, BorderLayout.NORTH);

    panel.add (new JScrollPane (analysisScreen));

    Box displayButtons = Box.createHorizontalBox ();
    displayButtons.add (new JButton (new SaveAction (analysisScreen)));
    displayButtons.add (new JButton (new ClearAction (analysisScreen)));
    panel.add (displayButtons, BorderLayout.SOUTH);

    return panel;
  }

  private AnalysisThread analysisTask;
  private AnalyseAction analyseAction;
  private StopAnalysisAction stopAnalysisAction;
  private JProgressBar feedbackAnalysis;

  private JPanel runAnalysisButtons (JSpinner numFixations) {
    JPanel panel = new JPanel ();

    analyseAction = new AnalyseAction (numFixations);
    JButton analyseButton = new JButton (analyseAction);
    analyseButton.setToolTipText ("Analyse CHREST model on all scenes");
    stopAnalysisAction = new StopAnalysisAction ();
    stopAnalysisAction.setEnabled (false);
    JButton stopAnalysisButton = new JButton (stopAnalysisAction);
    stopAnalysisButton.setToolTipText ("Stop the current analysis");
    feedbackAnalysis = new JProgressBar (0, 100);
    feedbackAnalysis.setToolTipText ("Proportion of scenes analysed");
    feedbackAnalysis.setValue (0);
    feedbackAnalysis.setStringPainted (true);
    panel.add (analyseButton);
    panel.add (stopAnalysisButton);
    panel.add (feedbackAnalysis);

    return panel;
  }

  private class StopAnalysisAction extends AbstractAction implements ActionListener {
    public StopAnalysisAction () {
      super ("Stop");
    }

    public void actionPerformed (ActionEvent e) {
      analysisTask.cancel (true);
      analyseAction.setEnabled (true);
      stopAnalysisAction.setEnabled (false);
    }

    private static final long serialVersionUID = 1L;
  }

  private static final long serialVersionUID = 1L;
}

class SceneDisplay extends JPanel {
  private Scene scene;
  private SceneView sceneView;
  private List<Fixation> fixations;
  private boolean showFixations;

  public SceneDisplay (Scene scene) {
    super ();

    setLayout (new GridLayout (1, 1));
    this.scene = scene;
    sceneView = new SceneView ();
    fixations = new ArrayList<Fixation> ();
    showFixations = false;
    add (new JScrollPane (sceneView));
  }

  public void updateScene (Scene scene) {
    this.scene = scene;
    sceneView.update ();
  }

  public void setFixations (List<Fixation> fixations) {
    this.fixations = fixations;
    sceneView.update ();
  }

  public void setShowFixations (boolean showFixations) {
    this.showFixations = showFixations;
    sceneView.update ();
  }

  class SceneView extends JPanel {
    private int maxX, maxY;

    SceneView () {
      super ();

      setBackground (Color.WHITE);
      update ();
    }

    private int offsetX = 10;
    private int offsetY = 10;
    private int scale = 20;

    public void update () {

      maxX = scale * (scene.getWidth () + 2); // + 2 for row heading and gaps
      maxY = scale * (scene.getHeight () + 2); // + 2 for column heading and gaps

      setPreferredSize (new Dimension (maxX, maxY));
      if (getParent () != null) ((JComponent)getParent ()).updateUI ();
      repaint ();
    }

    public void paint (Graphics g) {
      ((Graphics2D)g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
      super.paint (g); // make sure the background of the JPanel is drawn
      Graphics2D g2 = (Graphics2D)g;
      int fov = 2; // TODO ???

      g2.setBackground (Color.WHITE);
      g2.clearRect (0, 0, maxX, maxY);

      // draw lines of grid
      for (int i = 0; i <= scene.getHeight (); ++i) {
        g2.drawLine (offsetX, offsetY + scale * i, offsetX + scale * scene.getWidth(), offsetY + scale * i);
      }
      for (int i = 0; i <= scene.getWidth (); ++i) {
        g2.drawLine (offsetX + scale * i, offsetY, offsetX + scale * i, offsetY + scale * scene.getHeight ());
      }
      // add row labels
      for (int i = 0; i < scene.getHeight (); ++i) {
        g2.drawString ("" + (i+1), offsetX + scale * (scene.getWidth() + 1), offsetY + scale * (i+1) - 5);
      }
      // add column labels
      for (int i = 0; i < scene.getWidth (); ++i) {
        g2.drawString ("" + (i+1), offsetX + scale * i + 5, offsetY + scale * (scene.getHeight() + 1));
      }

      // draw entries within grid
      for (int i = 0; i < scene.getHeight (); ++i) {
        for (int j = 0; j < scene.getWidth (); ++j) {
          if (!scene.isEmpty (i, j)) {
            g2.drawString (scene.getItem (i, j), offsetX + 5 + scale * j, offsetY + scale - 5 + scale * i);
          }
        }
      }

      // draw fixations
      int prevX = -1;
      int prevY = -1;
      if (showFixations) {
        g2.setColor (Color.RED); // first fixation in red
        g2.setStroke (new BasicStroke (6)); // with thick border
        for (Fixation fixation : fixations) {
          int nextX = offsetX + scale * fixation.getX () + 5;
          int nextY = offsetY + scale * fixation.getY () + 5;
          if (prevX == -1 && prevY == -1) {
            ; // draw nothing for first oval
          } else {
            g2.drawLine (prevX, prevY, nextX+5, nextY+5);
          }
          g2.drawOval (nextX, nextY, scale-10, scale-10); 
          prevX = nextX+5; 
          prevY = nextY+5;
          g2.setColor (Color.BLUE); // remaining fixations in blue
          g2.setStroke (new BasicStroke (2)); // and narrower
        }
      }
    }

    private static final long serialVersionUID = 1L;
  }

  private static final long serialVersionUID = 1L;
}
