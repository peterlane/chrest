package jchrest.gui;

import java.awt.Graphics2D;
import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import jchrest.architecture.Node;

/**
 * Display a node of the discrimination network.
 */
class NodeDisplay implements LtmTreeViewNode {
  private Node node;
  private List<LtmTreeViewNode> children;

  public NodeDisplay (Node node) {
    this.node = node;
    children = new ArrayList<LtmTreeViewNode> ();
  }

  private final int ROOTNODE_SIZE = 11;

  public List<LtmTreeViewNode> getChildren () {
    return children;
  }

  private String toDisplay () {
    if (node.getReference () == 0) {
      return node.getImage().getModalityString ();
    } else {
      return node.getImage().toString ();
    }
  }

  public int getWidth (Graphics2D g, Size size) {
    int width =  2 * size.getMargin ();

    if (isRoot ()) {
      width += ROOTNODE_SIZE;
    } else if ( size.isSmall () ) {
      width = size.getSmallSize ();
    } else {
      int fixedItemsWidth = Math.max (getNodeNumberWidth (g, size), 
          size.getWidth (toDisplay (), g));
      if (node.getAssociatedNode () != null) {
        fixedItemsWidth = Math.max (fixedItemsWidth, 
            size.getWidth (node.getAssociatedNode().getReference() + "", g));
      }
      if (node.getNamedBy () != null) {
        fixedItemsWidth = Math.max (fixedItemsWidth, 
            size.getWidth (node.getNamedBy().getReference() + "", g));
      }
      width += fixedItemsWidth;
    }

    return width;
  }

  public int getHeight (Graphics2D g, Size size) {
    int height = 2 * size.getMargin ();

    if (isRoot ()) {
      height += ROOTNODE_SIZE;
    } else if ( size.isSmall () ) {
      height = size.getSmallSize ();
    } else {
      height += getNodeNumberHeight (g, size);
      height += size.getMargin (); // gap between the two
      height += size.getHeight (toDisplay ().toString (), g);
      if (node.getAssociatedNode() != null) {
        height += size.getMargin ();
        height += size.getHeight (node.getAssociatedNode().getReference() + "", g);
      }
      if (node.getNamedBy() != null) {
        height += size.getMargin ();
        height += size.getHeight (node.getNamedBy().getReference() + "", g);
      }
    }
    return height;
  }

  public void draw (Graphics2D g, int x, int y, int w, int h, Size size) {
    if (isRoot ()) {
      drawRootNode (g, x, y, size);
    } else if ( size.isSmall () ) {
      drawSmallNode (g, x, y, w, h);
    } else {
      drawRegularNode (g, x, y, w, h, size);
    }
  }

  public boolean isRoot () {
    return node == null;
  }

  public void add (LtmTreeViewNode node) {
    children.add (node);
  }

  private void drawRootNode (Graphics2D g, int x, int y, Size size) {
    int m = size.getMargin ();
    g.setColor (Color.BLACK);
    g.fillOval (x+m, y+m, ROOTNODE_SIZE, ROOTNODE_SIZE);
  }

  private void drawSmallNode (Graphics2D g, int x, int y, int w, int h) {
    g.setColor (Color.BLUE);
    g.fillRect (x, y, w, h);
  }

  private void drawRegularNode (Graphics2D g, int x, int y, int w, int h, Size size) {
    g.setBackground (Color.WHITE);
    g.clearRect (x+1, y+1, w-1, h-1);
    g.setColor (Color.BLACK);

    size.drawText (g, x, y, getNodeNumberString ());

    int textHeight = size.getHeight (getNodeNumberString (), g);
    size.drawText (g, x, y + textHeight + size.getMargin (), toDisplay ().toString ());
    if (node.getAssociatedNode () != null) {
      textHeight += size.getMargin () + size.getHeight ((node.getAssociatedNode().getReference() + ""), g);
      g.setColor (Color.BLUE);
      size.drawText (g, x, y + textHeight + size.getMargin (), node.getAssociatedNode().getReference() + "");
    }
    if (node.getNamedBy () != null) {
      textHeight += size.getMargin () + size.getHeight ((node.getNamedBy().getReference() + ""), g);
      g.setColor (Color.GREEN);
      size.drawText (g, x, y + textHeight + size.getMargin (), node.getNamedBy().getReference() + "");
    }
  }

  private String getNodeNumberString () { 
    return "Node: " + node.getReference ();
  }

  private int getNodeNumberWidth (Graphics2D g, Size size) {
    return (int)(size.getTextBounds(getNodeNumberString (), g).getWidth ());
  }

  private int getNodeNumberHeight (Graphics2D g, Size size) {
    return (int)(size.getTextBounds(getNodeNumberString (), g).getHeight ());
  }
}


