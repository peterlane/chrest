package jchrest.gui;

import jchrest.architecture.Chrest;
import jchrest.lib.ListPattern;
import jchrest.lib.Modality;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.*;
import java.util.List;
import javax.swing.*;
import javax.swing.border.*;

/**
 * This panel provides an interface for a simple demonstration of recognising 
 * and learning the given set of panels.
 */
class RecogniseAndLearnDemo extends JPanel {
  private Chrest model;
  private List<ListPattern> patterns;

  public RecogniseAndLearnDemo (Chrest model, List<ListPattern> patterns) {
    this.model = model;
    // for simplicity, turn off the more confusing aspects of learning
    this.model.setRho (1.0f);
    this.model.setCreateSemanticLinks (false);
    this.model.setCreateTemplates (false);
    this.patterns = patterns;

    setLayout (new BorderLayout ());
    add (constructPatternList (), BorderLayout.CENTER);
    add (constructFeedbackPanel (), BorderLayout.SOUTH);
    add (constructButtons (), BorderLayout.EAST);
  }

  private JList<ListPattern> patternList;
  private JPanel constructPatternList () {
    JPanel panel = new JPanel ();
    panel.setBorder (new TitledBorder ("Patterns"));
    panel.setLayout (new GridLayout (1, 1));

    patternList = new JList<ListPattern> (patterns.toArray (new ListPattern[0]));
    patternList.setSelectedIndex (0);

    panel.add (new JScrollPane (patternList));

    return panel;
  }

  abstract class PatternAction extends AbstractAction implements ActionListener {
    PatternAction (String label) {
      super (label);
    }

    boolean isSelected () {
      return (patternList.getSelectedIndex () != -1);
    }

    ListPattern selectedPattern () {
      ListPattern pattern = null;
      if (isSelected ()) {
        pattern = patternList.getSelectedValue ();
        pattern.setModality ((Modality)modeButton.getSelectedItem ());
      }
      return pattern;
    }

    public abstract void actionPerformed (ActionEvent e);

    private static final long serialVersionUID = 1L;
  }

  class LearnPatternAction extends PatternAction {
    LearnPatternAction () {
      super ("Learn");
    }

    public void actionPerformed (ActionEvent e) {
      if (isSelected ()) {
        model.recogniseAndLearn (selectedPattern());
        feedback.setText ("Learning " + selectedPattern().toString ());
      }
    }

    private static final long serialVersionUID = 1L;
  }

  class LearnAllPatternAction extends AbstractAction implements ActionListener {
    LearnAllPatternAction () {
      super ("Learn all"); 
    }

    public void actionPerformed (ActionEvent e) {
      for (ListPattern pattern : patterns) {
        ListPattern patternInCorrectMode = pattern.clone ();
        patternInCorrectMode.setModality ((Modality)modeButton.getSelectedItem ());
        model.recogniseAndLearn (patternInCorrectMode);
      }
      feedback.setText ("Learnt all patterns");
    }

    private static final long serialVersionUID = 1L;
  }

  class RecognisePatternAction extends PatternAction {
    RecognisePatternAction () {
      super ("Recognise");
    }

    public void actionPerformed (ActionEvent e) {
      if (isSelected ()) {
        feedback.setText ("Recalled " + 
            model.recallPattern (selectedPattern()).toString () +
            " for " +
            selectedPattern().toString ());
      }
    }

    private static final long serialVersionUID = 1L;
  }

  private JComboBox<Modality> modeButton;
  private int currentMode;

  private Box constructButtons () {
    Box buttons = Box.createVerticalBox ();
    modeButton = new JComboBox<Modality> (Modality.values ()); 
    JButton learnButton = new JButton (new LearnPatternAction ());
    JButton learnAllButton = new JButton (new LearnAllPatternAction ());
    JButton recogniseButton = new JButton (new RecognisePatternAction ());
    
    learnButton.setToolTipText ("Train model on currently selected pattern");
    learnAllButton.setToolTipText ("Train model on all patterns");
    recogniseButton.setToolTipText ("Recall currently selected pattern from model");

    modeButton.setMaximumSize (recogniseButton.getPreferredSize ());
    modeButton.setAlignmentX(0.0f); // correct the alignment of buttons and combobox
    learnButton.setMaximumSize (recogniseButton.getPreferredSize ());
    learnAllButton.setMaximumSize (recogniseButton.getPreferredSize ());

    buttons.add (Box.createGlue ());
    buttons.add (new JLabel ("Pattern type:"));
    buttons.add (modeButton);
    buttons.add (Box.createVerticalStrut (20)); // add a small gap before buttons
    buttons.add (learnButton);
    buttons.add (learnAllButton);
    buttons.add (recogniseButton);
    buttons.add (Box.createGlue ());

    return buttons;
  }

  private JLabel feedback;

  private JLabel constructFeedbackPanel () {
    feedback = new JLabel ("FEEDBACK");
    feedback.setFont (new Font ("Arial", Font.PLAIN, 18));
    feedback.setBorder (new EmptyBorder (10, 50, 10, 50));
    return feedback;
  }

  private static final long serialVersionUID = 1L;
}

