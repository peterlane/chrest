package jchrest.gui;

import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

import jchrest.architecture.Chrest;
import jchrest.architecture.Node;

public class ListNodeRenderer extends JLabel implements ListCellRenderer<Object> {
  private Chrest model;

  ListNodeRenderer (Chrest model) {
    this.model = model;
  }

  public Component getListCellRendererComponent (
      JList<?> list,
      Object value,
      int index,
      boolean isSelected,
      boolean cellHasFocus) {
    JLabel cell = new JLabel ("");
    cell.setBorder (new CompoundBorder (new EmptyBorder (3, 3, 3, 3), new EtchedBorder ()));
    cell.setIcon (new NodeIcon ((Node)value, list));

    return cell;

      }

  private static final long serialVersionUID = 1L;  
}

