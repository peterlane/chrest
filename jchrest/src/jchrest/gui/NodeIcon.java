package jchrest.gui;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import javax.swing.Icon;

import jchrest.architecture.Node;

public class NodeIcon implements Icon {
  private NodeDisplay node;
  private Component parent;

  public NodeIcon (Node node, Component parent) {
    this.node = new NodeDisplay (node);
    this.parent = parent;
  }

  public void paintIcon (Component c, Graphics g, int x, int y) {
    ((Graphics2D)g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    node.draw ((Graphics2D)g, x, y, getIconWidth(), getIconHeight(), Size.getValues().get (1));
  }

  public int getIconWidth  () { return node.getWidth ( (Graphics2D)parent.getGraphics(), Size.getValues().get (1)); }
  public int getIconHeight () { return node.getHeight( (Graphics2D)parent.getGraphics(), Size.getValues().get (1)); }
}

