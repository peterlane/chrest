package jchrest.gui;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JSplitPane;

import jchrest.architecture.Chrest;
import jchrest.lib.FileUtilities;

public class ChrestView extends JFrame implements PropertyChangeListener {
  private Shell shell;
  private Chrest model;
  private ChrestLtmView ltmView;
  private ChrestStmView stmView;
  private ChrestTimeView timeView;

  public ChrestView (Chrest model) {
    this (new Shell (), model);
  }

  public ChrestView (Shell shell, Chrest model) {
    super ("CHREST Model View");
    this.shell = shell;
    this.model = model;
    this.model.addPropertyChangeListener (this);
    timeView = new ChrestTimeView (this.model);
    ltmView = new ChrestLtmView (this.model);
    stmView = new ChrestStmView (this.model);

    // catch close-window event
    addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent ev) { 
        closeView (); 
      }
    });
    createMenuBar ();

    // layout the components
    JPanel leftSide = new JPanel ();
    leftSide.setLayout (new BorderLayout ());
    leftSide.add (timeView, BorderLayout.NORTH);
    leftSide.add (stmView, BorderLayout.CENTER);
    JSplitPane jsp = new JSplitPane (JSplitPane.HORIZONTAL_SPLIT, leftSide, ltmView);
    jsp.setOneTouchExpandable (true);

    setLayout (new GridLayout (1, 1));
    add (jsp);

    // finalise display settings
    setSize (550, 400);
    setVisible (true);
    // prompt the long-term memory to draw itself
    ltmView.setStandardDisplay ();
  }

  private void createMenuBar () {
    JMenuBar mb = new JMenuBar ();
    mb.add (createViewMenu ());
    setJMenuBar (mb);
  }

  public void saveLongTermMemory (File file) {
    ltmView.saveLongTermMemory (file);
  }

  class SaveLtmAction extends AbstractAction implements ActionListener {
    private ChrestView parent;

    public SaveLtmAction (ChrestView parent) {
      super ("Save LTM", new ImageIcon (Shell.class.getResource ("icons/SaveAs16.gif")));

      this.parent = parent;
    }

    public void actionPerformed (ActionEvent e) {
      File file = FileUtilities.getSaveFilename (shell);
      if (file != null) {
        parent.saveLongTermMemory (file);
      }
    }
    
    private static final long serialVersionUID = 1L;
  }

  class CloseAction extends AbstractAction implements ActionListener {
    private ChrestView view;

    public CloseAction (ChrestView view) {
      super ("Close");
      this.view = view;
    }

    public void actionPerformed (ActionEvent e) {
      view.closeView ();
    }

    private static final long serialVersionUID = 1L;
  }

  private JMenu createViewMenu () {
    JMenu menu = new JMenu ("View");
    menu.setMnemonic (KeyEvent.VK_V);
    menu.add (new SaveLtmAction (this));
    menu.getItem(0).setMnemonic (KeyEvent.VK_S);
    menu.add (new CloseAction (this));
    menu.getItem(1).setMnemonic (KeyEvent.VK_C);

    return menu;
  }

  /** 
   * Implement the PropertyChangeListener interface, and update the view whenever 
   * the underlying model has changed.
   */
  public void propertyChange (PropertyChangeEvent e) {
    ltmView.update ();
    stmView.update ();
    timeView.update ();
  }

  /**
   * When closing the view, make sure the observer is detached from the model.
   */
  private void closeView () {
      model.removePropertyChangeListener (this);
      setVisible (false);
      dispose ();
    }

  private static final long serialVersionUID = 1L;
}

