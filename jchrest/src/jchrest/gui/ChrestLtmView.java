package jchrest.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.font.TextLayout;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.imageio.ImageIO;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import javax.swing.SwingWorker;
import javax.swing.border.TitledBorder;

import jchrest.architecture.Chrest;
import jchrest.architecture.Link;
import jchrest.architecture.Node;
import jchrest.lib.Pattern;

/**
 * This panel displays the model long-term memory within a tree view, 
 * supporting some interactions to alter the display.
 */
public class ChrestLtmView extends JPanel {
  private Chrest model;
  private TreeViewPane ltmView;

  public ChrestLtmView (Chrest model) {
    super ();

    this.model = model;
    setBorder (new TitledBorder ("LTM"));
    setLayout (new BorderLayout ());

    // -- the treeview pane
    if (model.getTotalLtmNodes () > 5000) {
      ltmView = null;
      add (new JLabel ("Sorry - LTM too large to display"));
    } else {
      ltmView = new TreeViewPane (new TreeViewNode (new NodeDisplay (null)));
      constructingTreeThread = new ConstructTreeThread ();
      constructingTreeThread.execute ();
      add (new JScrollPane (ltmView));
    }

    add (createToolBar (), BorderLayout.SOUTH);
  }

  private ConstructTreeThread constructingTreeThread;

  private class ConstructTreeThread extends SwingWorker<Void, Void> {
    LtmTreeViewNode newTree;
    
    @Override
      public Void doInBackground () {
        newTree = constructTree ();
        return null;
      }

    @Override
      protected void done () {
        if (newTree != null) 
          ltmView.changeRoot (new TreeViewNode (newTree));
        constructingTreeThread = null;
      }
  }

  public void update () {
    if (ltmView != null) {
      if (model.getTotalLtmNodes () > 5000) {
        // TODO : change display if number of nodes is too large
        //        this.add (new JLabel ("Sorry - LTM too large to display"));
      } else {
        constructingTreeThread = new ConstructTreeThread ();
        constructingTreeThread.execute ();
      }
    }
  }

  private JComboBox<String> createOrientationBox () {
    JComboBox<String> box = new JComboBox<> (new String[] { "Horizontal", "Vertical" });
    box.setSelectedIndex (0); // Display begins in horizontal orientation
    box.addActionListener (new ActionListener () {
      public void actionPerformed (ActionEvent e) {
        updateOrientation (box.getSelectedIndex () == 0 ? 
          Orientation.HORIZONTAL :
          Orientation.VERTICAL);
      }
    });

    return box;
  }

	private JComboBox<String> createSizeBox () {
		JComboBox<String> box = new JComboBox<> ();
		for (Size size : Size.getValues ()) {
			box.addItem (size.toString ());
		}
		box.setSelectedIndex (1); // Display begins in Medium size
		box.addActionListener (new ActionListener () {
			public void actionPerformed (ActionEvent e) {
				updateSize (Size.getValues().get (box.getSelectedIndex ()));
			}
		});
		return box;
	}
	
	private JToolBar createToolBar () {
		JToolBar tools = new JToolBar ();

		tools.add (new JLabel ("Orientation: "));
		tools.add (createOrientationBox ());
		tools.addSeparator();
		tools.add (new JLabel ("Size: "));
		tools.add (createSizeBox ());

		return tools;
	}

	public void setStandardDisplay () {
    if (ltmView != null) {
      updateOrientation (Orientation.HORIZONTAL);
      updateSize (Size.getValues().get (1));
    }
	}

  /**
   * Relayout and draw the treeview nodes.
   */
	public void drawTreeView () {
    if (ltmView != null) {
      ltmView.relayout();
    }
	}

  /**
   * Save the network as an image file.  Currently, only .png format is supported.
   *
   * @param file to save network to
   */
  public void saveLongTermMemory (File file) {
    BufferedImage img = new BufferedImage(ltmView.getExtentWidth (), ltmView.getExtentHeight (), BufferedImage.TYPE_INT_RGB);  
    img.createGraphics();  
    ltmView.paint (img.getGraphics());
    try {
      String format = "png"; // TODO Extend range  
      ImageIO.write(img, format, file);
    } catch (IOException e) {  
      JOptionPane.showMessageDialog (this, 
          "Failed to write image",
          "Failed to write image",
          JOptionPane.ERROR_MESSAGE);
    }  
  }

  /**
   * Change the orientation of the displayed network.
   *
   * @param newOrientation for the displayed network
   */
	public void updateOrientation (Orientation newOrientation) {
		ltmView.setOrientation (newOrientation);
	}
	
  /**
   * Change the displayed size of the network.
   *
   * @param newSize for the displayed network
   */
	public void updateSize (Size newSize) {
		ltmView.setSize (newSize);
	}

  /**
   * Wrap the model's LTM as a set of LtmTreeVewNode objects,
   * joining the three types of LTM into a single tree.
   */
  private LtmTreeViewNode constructTree () {
    LtmTreeViewNode baseTreeViewNode = new NodeDisplay (null);
    baseTreeViewNode.add (constructTree (model.getLtmByModality(Pattern.makeVisualList (new String[]{}))));
    baseTreeViewNode.add (constructTree (model.getLtmByModality(Pattern.makeVerbalList (new String[]{}))));
    baseTreeViewNode.add (constructTree (model.getLtmByModality(Pattern.makeActionList (new String[]{}))));
    return baseTreeViewNode;
  }

  /** 
   * Wrap the model's LTM as a set of LtmTreeViewNode objects.
   */
  private LtmTreeViewNode constructTree (Node baseNode) {
    LtmTreeViewNode baseTreeViewNode = new NodeDisplay (baseNode);
    for (Link link : baseNode.getChildren ()) {
      LtmTreeViewNode linkNode = new LinkDisplay (link);
      linkNode.add (constructTree (link.getChildNode ()));
      baseTreeViewNode.add (linkNode);
    }

    return baseTreeViewNode;
  }

  private static final long serialVersionUID = 1L;
}

/**
 * Display a link within the LTM view.  Note that a link can 
 * never be a root node.
 */
class LinkDisplay implements LtmTreeViewNode {
  private Link link;
  private List<LtmTreeViewNode> children;

  public LinkDisplay (Link link) {
    this.link = link;
    children = new ArrayList<LtmTreeViewNode> ();
  }

  public List<LtmTreeViewNode> getChildren () {
    return children;
  }

  public int getWidth (Graphics2D g, Size size) {
    int width =  2 * size.getMargin ();

    if ( size.isSmall () ) {
      width = size.getSmallSize ();
    } else {
      width += size.getWidth (link.getTest().toString (), g);
    }

    return width;
  }

  public int getHeight (Graphics2D g, Size size) {
    int height = 2 * size.getMargin ();

    if ( size.isSmall () ) {
      height = size.getSmallSize ();
    } else {
      height += size.getHeight (link.getTest().toString (), g);
    }
    return height;
  }

  public void draw (Graphics2D g, int x, int y, int w, int h, Size size) {
    if ( size.isSmall () ) {
      drawSmallNode (g, x, y, w, h);
    } else {
      drawRegularNode (g, x, y, w, h, size);
    }
  }

  public boolean isRoot () {
    return false;
  }

  public void add (LtmTreeViewNode node) {
    children.add (node);
  }

  private void drawSmallNode (Graphics2D g, int x, int y, int w, int h) {
    g.setColor (Color.LIGHT_GRAY);
    g.fillRect (x, y, w, h);
  }

  private void drawRegularNode (Graphics2D g, int x, int y, int w, int h, Size size) {
    g.setBackground (Color.LIGHT_GRAY);
    g.clearRect (x+1, y+1, w-1, h-1);
    g.setColor (Color.BLACK);

    size.drawText (g, x, y, link.getTest().toString ());
  }
}

/** Display a given TreeViewNode with a JPanel.  */
class TreeViewPane extends JPanel {
	private final static long serialVersionUID = 2;
	
	private int maxX;
	private int maxY;
	private TreeViewNode rootnode;
	private Orientation orientation;
	private Size size;

	public TreeViewPane (TreeViewNode rootNode) {
		super();
		
		setBackground(Color.white);

		this.rootnode = rootNode;
		orientation = Orientation.HORIZONTAL;
		size = Size.getValues().get (1);

		// give some preferred size to get us started
		maxX = 100;
		maxY = 100;
		setPreferredSize (new Dimension (maxX, maxY));
	}

	public void setOrientation (Orientation newOrientation) {
		orientation = newOrientation;
		relayout();
	}

	public void setSize (Size newSize) {
		size = newSize;
		relayout();
	}
	
	public void changeRoot (TreeViewNode newRoot) {
		rootnode = newRoot;
		relayout();
	}

	public void relayout () {
		rootnode.layoutNode (getGraphics (), 10, 10, orientation, size);
		maxX = 20 + rootnode.getExtentWidth (getGraphics (), orientation, size);
		maxY = 20 + rootnode.getExtentHeight (getGraphics (), orientation, size);
		setPreferredSize (new Dimension (maxX, maxY));
		// notify parent container (which is probably a JScrollPane) to update itself
		if (getParent () != null) ((JComponent)getParent ()).updateUI ();
		repaint ();
	}

  public void paint(Graphics g) {
    ((Graphics2D)g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    super.paint (g); // make sure the background of the JPanel is drawn
		Graphics2D g2 = (Graphics2D)g;

		g2.setBackground (Color.WHITE);
		g2.clearRect (0, 0, this.getExtentWidth (), this.getExtentHeight ());
		rootnode.drawNode (g, size, orientation);
	}

  int getExtentWidth () {
    return 20 + rootnode.getExtentWidth (getGraphics (), orientation, size);
  }

  int getExtentHeight () {
    return 20 + rootnode.getExtentHeight (getGraphics (), orientation, size);
  }
}

/** The TreeViewNode is a wrapper around a Node, 
 * managing the layout and display of the node and its children.
 */
class TreeViewNode {
	int x;  // x position on the canvas
	int y;  // y position 
	int w;  // the width
	int h;  // the height
	// the extent of a node is the amount of space taken up by itself
	// and its children
	int extentWidth;
	int extentHeight;
	// hold a pointer to the object being displayed
	LtmTreeViewNode object;
	ArrayList<TreeViewNode> children;
	
  public TreeViewNode (LtmTreeViewNode object) {
    this.object = object;
    children = new ArrayList<TreeViewNode> ();
    // work through any children of node, turning them into TreeViewNodes
    if (object != null && object.getChildren () != null) {
      for (LtmTreeViewNode child : object.getChildren ()) {
        children.add (new TreeViewNode (child));
      }
    }

    setDefaults();
  }

	private void setDefaults () {
		// the position and size of the node will be created during layout
		x = 0;
		y = 0;
  	w = 0; 
		h = 0;
		extentWidth = 0;
		extentHeight = 0;
	}

	// return true/false depending if this node has children
	private boolean hasChildren () { return (!(children.isEmpty ())); }

	/** Return the visible extent of this node as a Rectangle */
	public Rectangle getVisibleExtent () {
		return new Rectangle(x, y, w, h);
	}

	/** Compute the extent in width of a TreeViewNode.  Different routines compute width
	  * depending on direction of layout.
	  */
	public int getExtentWidth (Graphics g, Orientation orientation, Size size) {
		// return cached value, if present
		if (extentWidth != 0) return extentWidth;

		if (orientation == Orientation.HORIZONTAL) {
			extentWidth = getExtentWidthHorizontalLayout (g, orientation, size);
		} else {
			extentWidth = getExtentWidthVerticalLayout (g, orientation, size);
		}
		return extentWidth;
	}

	/** Compute the extent in height of a TreeViewNode.  Different routines compute height 
	  * depending on direction of layout.
	  */
	public int getExtentHeight (Graphics g, Orientation orientation, Size size) {
		// return a cached value, if present
		if (extentHeight != 0) return extentHeight;

		if (orientation == Orientation.HORIZONTAL) {
			extentHeight = getExtentHeightHorizontalLayout (g, orientation, size);
		} else {
			extentHeight = getExtentHeightVerticalLayout (g, orientation, size);
		}
		return extentHeight;
	}

	// for horizontal layout, extent is width of widest child
	private int getExtentWidthHorizontalLayout (Graphics g, Orientation orientation, Size size) {
		int width = object.getWidth ((Graphics2D)g, size);
		if (hasChildren ()) {
			int maxChildWidth = 0;

			for (TreeViewNode child : children) {
				if (maxChildWidth < child.getExtentWidth (g, orientation, size)) {
					maxChildWidth = child.getExtentWidth (g, orientation, size);
				}
			}

			width += size.getHorizontalSeparator (orientation) + maxChildWidth;
		}

		return width;
	}

	// for vertical layout, add all the widths of the children and gaps to get extent
	private int getExtentWidthVerticalLayout (Graphics g, Orientation orientation, Size size) {
		int totalChildWidth = 0;
		if (hasChildren ()) {
			for (TreeViewNode child : children) {
				totalChildWidth += child.getExtentWidth (g, orientation, size);
			}
			// add n-1 gaps
			totalChildWidth += size.getHorizontalSeparator (orientation) *
					(children.size () - 1);
		}
		return Math.max (totalChildWidth, object.getWidth ((Graphics2D)g, size));
	}

	// for horizontal layout, add all the heights of the children and gaps to get extent
	private int getExtentHeightHorizontalLayout (Graphics g, Orientation orientation, Size size) {
		int totalChildHeight = 0;
		if (hasChildren ()) {
			for (TreeViewNode child : children) {
				totalChildHeight += child.getExtentHeight (g, orientation, size);
			}
			// add n-1 gaps
			totalChildHeight += size.getVerticalSeparator (orientation) *
				(children.size () - 1);
		}
		return Math.max (totalChildHeight, object.getHeight ((Graphics2D)g, size));
	}

	// for horizontal layout, extent is height of tallest child
	private int getExtentHeightVerticalLayout (Graphics g, Orientation orientation, Size size) {
		int height = object.getHeight((Graphics2D)g, size);
		if (hasChildren ()) {
			int maxChildHeight = 0;

			for (TreeViewNode child : children) {
				if (maxChildHeight < child.getExtentHeight (g, orientation, size)) {
					maxChildHeight = child.getExtentHeight (g, orientation, size);
				}
			}	

			height += size.getVerticalSeparator (orientation) + maxChildHeight; 
		}

		return height;
	}

	// layoutNode assigns new x and y values to the node, respecting new orientation and size
	public void layoutNode (Graphics g, int x, int y, Orientation orientation, Size size) {
		this.x = x;
		this.y = y;
		this.w = object.getWidth((Graphics2D)g, size);
		this.h = object.getHeight((Graphics2D)g, size);

		// clear cached values, to force recomputation
		extentWidth = 0;
		extentHeight = 0;
		if (orientation == Orientation.HORIZONTAL) {
			layoutChildrenHorizontally (g, orientation, size);
		} else {
			layoutChildrenVertically (g, orientation, size);
		}
	}

	// horizontal layout means children displayed vertically
	private void layoutChildrenHorizontally (Graphics g, Orientation orientation, Size size) {
		if (!hasChildren ()) return ; // nothing to do, if no children
		int nextY = y;
		// each child's x position is to the right of this node
		int thisX = x + size.getHorizontalSeparator(orientation) + w;
		// nextY is incremented by child's height + verticalSeparator
		//       to get the vertical position of the next child
		//       to get the vertical position of the next child
		for (TreeViewNode child : children) {
			child.layoutNode(g, thisX, nextY, orientation, size);
			nextY += size.getVerticalSeparator (orientation);
			nextY += child.getExtentHeight(g, orientation, size);
		}
		// move the node DOWN to center it
		y += 0.5*(getExtentHeight(g, orientation, size) - h);
	}

	// vertical layout means children displayed horizontally
	private void layoutChildrenVertically (Graphics g, Orientation orientation, Size size) {
		if (!hasChildren ()) return; // nothing to do, if no children
		int thisY = y + size.getVerticalSeparator(orientation) + h;
		int nextX = x;
		// nextX is incremented by child's width + horizontalSeparator
		//       to get horizontal position of the next child
		for (TreeViewNode child : children) {
			child.layoutNode(g, nextX, thisY, orientation, size);
			nextX += size.getHorizontalSeparator(orientation);
			nextX += child.getExtentWidth(g, orientation, size);
		}
		// move the node RIGHT to center it
		x += 0.5 * (getExtentWidth(g, orientation, size) - w);
	}

	public int getLeftX () { return x; }
	public int getMidX () { return x + (Math.round (w / 2)); }
	public int getRightX () { return x + w; }
	public int getTopY () { return y; }
	public int getMidY () { return y + (Math.round (h / 2)); }
	public int getBottomY () { return y + h; }

	/** A TreeViewNode draws itself by requesting its object to draw 
	  * itself within the specified area, on the given context.
	  */
	public void drawNode (Graphics g, Size size, Orientation orientation) {
		Graphics2D g2 = (Graphics2D)g;
		drawLinks (g2, orientation); // draw the links first, so objects overwrite them
		drawNodeBorder (g2);
		object.draw (g2, x, y, w, h, size);
		drawChildren (g, size, orientation);
	}

	private void drawNodeBorder (Graphics2D g2) {
		if (object.isRoot ()) return; // allow rootnode to be drawn differently
		g2.clearRect (x, y, w, h);
		g2.setColor (Color.BLACK);
    g2.drawRect (x, y, w, h);
	}

	private void drawChildren (Graphics g, Size size, Orientation orientation) {
		if (hasChildren ()) {
			for (TreeViewNode child : children) {
				child.drawNode (g, size, orientation);
			}
		}
	}

	private void drawLinks (Graphics2D g2, Orientation orientation) {
		if (!hasChildren ()) return; // nothing to do if no children
		g2.setColor(Color.black);
		
		if (orientation == Orientation.HORIZONTAL) {
			drawHorizontalLinks (g2, getFromX (orientation), getFromY (orientation));
		} else {
			drawVerticalLinks (g2, getFromX (orientation), getFromY (orientation));
		}
	}

	private int getFromX (Orientation orientation) {
		if (object.isRoot () || orientation == Orientation.VERTICAL) {
			return getMidX ();
		} else {
			return getRightX ();
		}
	}

	private int getFromY (Orientation orientation) {
		if (object.isRoot () || orientation == Orientation.HORIZONTAL) {
			return getMidY ();
		} else {
			return getBottomY ();
		}
	}
	
	private void drawHorizontalLinks (Graphics2D g2, int from_x, int from_y) {
		for (TreeViewNode child : children) {
			g2.drawLine (from_x, from_y, child.getLeftX (), child.getMidY ());
		}
	}

	private void drawVerticalLinks (Graphics2D g2, int from_x, int from_y) {
		for (TreeViewNode child : children) {
			g2.drawLine (from_x, from_y, child.getMidX (), child.getTopY ());
		}
	}
}

