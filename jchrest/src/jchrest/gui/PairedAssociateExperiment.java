package jchrest.gui;

import jchrest.architecture.Chrest;
import jchrest.lib.ListPattern;
import jchrest.lib.PairedPattern;
import jchrest.lib.Pattern;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.table.*;

/**
 * This panel provides an interface for running paired associate
 * experiments.
 */
public class PairedAssociateExperiment extends JPanel {
  private final Chrest model;
  private final List<PairedPattern> patterns;

  public PairedAssociateExperiment (Chrest model, List<PairedPattern> patterns) {
    super ();
    
    this.model = model;
    this.patterns = patterns;

    setLayout (new GridLayout (1, 1));
    JSplitPane jsp = new JSplitPane (JSplitPane.HORIZONTAL_SPLIT, createRunExperimentView (), createProtocolView ());
    jsp.setOneTouchExpandable (true);

    add (jsp);
  }

  /**
   * Convert a list of ListPatterns into a list of stimulus-response pairs.
   * If patterns are A-B-C-D-E then list of pairs will be: A-B B-C C-D D-E.
   *
   * @param patterns to convert into pairs
   * @return list of pairs of patterns
   */
  public static List<PairedPattern> makePairs (List<ListPattern> patterns) {
    List<PairedPattern> pairs = new ArrayList<PairedPattern> ();
    for (int i = 1; i < patterns.size (); ++i) {
      pairs.add (new PairedPattern (patterns.get(i-1), patterns.get(i)));
    }

    return pairs;
  }

  private JPanel createListView () {
    JPanel panel = new JPanel ();
    panel.setBorder (new TitledBorder ("Stimulus-response pairs"));
    panel.setLayout (new GridLayout (1, 1));

    JPanel pairsPanel = new JPanel ();
    pairsPanel.setLayout (new GridLayout (patterns.size(), 2));
    for (PairedPattern pair : patterns) {
      pairsPanel.add (new JLabel (pair.getFirst().toString ()));
      pairsPanel.add (new JLabel (pair.getSecond().toString ()));
    }

    panel.add (new JScrollPane (pairsPanel));
    return panel;        
  }

  class RestartAction extends AbstractAction implements ActionListener {
    RestartAction () {
      super ("Restart");
    }

    public void actionPerformed (ActionEvent e) {
      model.clear ();
      responses.clear ();
      exptClock = 0;

      updateControls ();
    }

    private static final long serialVersionUID = 1L;
  }

  private List<List<ListPattern>> responses;

  class RunTrialAction extends AbstractAction implements ActionListener {
    RunTrialAction () {
      super ("Run Trial");

      exptClock = 0;
    }

    private List<PairedPattern> preparePatterns () {
      var result = new ArrayList<PairedPattern> ();
      var gen = new java.util.Random ();
      for (PairedPattern pattern : patterns) {
        if (randomOrder.isSelected ()) {
          result.add (gen.nextInt (patterns.size () + 1), pattern);
        } else {
          result.add (pattern);
        }
      }

      return result;
    }

    private void collectResponses () {
      var theseResponses = new ArrayList<ListPattern> ();
      for (PairedPattern pair : patterns) {
        ListPattern response = model.associatePattern (pair.getFirst ());
        if (response != null) {
          theseResponses.add (response);
        } else {
          theseResponses.add (Pattern.makeVisualList (new String[]{"NONE"}));
        }
      }
      responses.add (theseResponses);
    }

    public void actionPerformed (ActionEvent e) {
      model.freeze (); // save all gui updates to the end
      collectResponses ();
      for (PairedPattern pair : preparePatterns ()) {
        model.associateAndLearn (pair.getFirst (), pair.getSecond (), exptClock);
        exptClock += ((SpinnerNumberModel)interItemTime.getModel()).getNumber().intValue ();
      }
      exptClock += ((SpinnerNumberModel)endTrialTime.getModel()).getNumber().intValue ();
      updateControls ();
    }

    private static final long serialVersionUID = 1L;
  }

  private int exptClock;
  private JLabel experimentTimeLabel;
  private JSpinner endTrialTime;
  private JSpinner interItemTime;
  private JCheckBox randomOrder;
  private JScrollBar protocolHorizontalBar;
  private JTable protocol;

  private void updateControls () {
    ((AbstractTableModel)protocol.getModel()).fireTableStructureChanged ();
    experimentTimeLabel.setText ("" + exptClock);
    model.unfreeze ();
  }

  private JPanel createControls () {
    experimentTimeLabel = new JLabel ("0");
    endTrialTime = new JSpinner (new SpinnerNumberModel (2000, 1, 50000, 1));
    interItemTime = new JSpinner (new SpinnerNumberModel (2000, 1, 50000, 1));
    randomOrder = new JCheckBox ("Random order");
    randomOrder.setToolTipText ("Set this to pass pairs to model in a random order");
    JButton restart = new JButton (new RestartAction ());
    restart.setToolTipText ("Reset the experiment and clear the model");
    JButton runTrial = new JButton (new RunTrialAction ());
    runTrial.setToolTipText ("Pass each stimulus-response pair once against the model");

    JPanel controls = new JPanel ();
    controls.setLayout (new GridLayout (5, 2, 10, 3));
    controls.add (new JLabel ("Experiment time (ms)", SwingConstants.RIGHT));
    controls.add (experimentTimeLabel);
    controls.add (new JLabel ("End trial time (ms)", SwingConstants.RIGHT));
    controls.add (endTrialTime);
    controls.add (new JLabel ("Inter item time (ms)", SwingConstants.RIGHT));
    controls.add (interItemTime);
    controls.add (randomOrder);
    controls.add (restart);
    controls.add (new JLabel (""));
    controls.add (runTrial);

    return controls;
  }

  private JPanel createRunExperimentView () {
    JPanel panel = new JPanel ();
    panel.setLayout (new BorderLayout ());
    panel.add (createListView ());
    panel.add (createControls (), BorderLayout.SOUTH);

    return panel;
  }

  private void createProtocolTable () {
    TableModel tm = new AbstractTableModel () {
      // include a row for the number of errors
      public int getRowCount () {
        return 1 + patterns.size ();
      }
      // include two columns for the stimulus and target response
      public int getColumnCount () {
        return 2 + responses.size (); 
      }
      // compute the total number of errors in given column of responses
      private int getErrors (int trial) {
        int errors = 0;
        for (int i = 0, n = patterns.size (); i < n; ++i) {
          ListPattern target = responses.get(trial).get(i).clone ();
          ListPattern response = patterns.get(i).getSecond().clone ();
          target.setNotFinished ();
          response.setNotFinished ();
          if (response.equals (target)) {
          } else {
            errors += 1;
          }
        }
        return errors;
      }
      public Object getValueAt (int row, int column) {
        if (column == 0) {
          if (row == patterns.size ()) {
            return "";
          } else {
            return patterns.get(row).getFirst ();
          }
        } else if (column == 1) {
          if (row == patterns.size ()) {
            return "Errors:";
          } else {
            return patterns.get(row).getSecond ();
          }
        } else {
          if (row == patterns.size ()) {
            return "" + getErrors (column-2);
          } else {
            return responses.get(column-2).get(row).toString ();
          }
        }
      }
      public String getColumnName (int column) {
        if (column == 0) {
          return "Stimulus";
        } else if (column == 1) {
          return "Target";
        } else {
          return "Trial " + (column - 1);
        }
      }
      public void fireTableStructureChanged() {
        super.fireTableStructureChanged ();
        protocolHorizontalBar.setValue (protocolHorizontalBar.getMaximum ());
      }
    };
    protocol = new JTable (tm);
    protocol.setAutoResizeMode (JTable.AUTO_RESIZE_OFF);
  }

  private JPanel createProtocolView () {
    JPanel panel = new JPanel ();
    panel.setBorder (new TitledBorder ("Protocol"));
    panel.setLayout (new GridLayout (1, 1));
    responses = new ArrayList<List<ListPattern>> ();
    createProtocolTable ();
    JScrollPane scrollPane = new JScrollPane (protocol);
    protocolHorizontalBar = scrollPane.getHorizontalScrollBar ();
    panel.add (scrollPane);

    return panel;
  }

  private static final long serialVersionUID = 1L;  
}

