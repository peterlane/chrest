package jchrest.gui;

import jchrest.architecture.Chrest;

import javax.swing.*;
import javax.swing.border.TitledBorder;

/**
  * This panel displays the model's current clock time.
  */
public class ChrestTimeView extends JPanel {
  private Chrest model;
  private JLabel display;

  public ChrestTimeView (Chrest model) {
    super ();

    setBorder (new TitledBorder ("Clock"));

    this.model = model;
    display = new JLabel ("Time (ms): " + model.getClock ());

    add (display);
  }

  public void update () {
    display.setText ("Time (ms): " + model.getClock ());
  }

  private static final long serialVersionUID = 1L;
}
