package jchrest.gui;

import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.border.TitledBorder;

import jchrest.architecture.Chrest;
import jchrest.architecture.Node;

public class ChrestStmView extends JPanel {
  private Chrest model;
  private DefaultListModel<Node> visualStmView, verbalStmView;
  private JList<Node> visualStmList, verbalStmList;

  public ChrestStmView (Chrest model) {
    super ();

    this.model = model;
    setLayout (new GridLayout (1, 1));

    JPanel visualPanel = new JPanel ();
    visualPanel.setLayout (new GridLayout (1, 1));
    visualPanel.setBorder (new TitledBorder ("Visual STM"));
    visualStmView = new DefaultListModel<Node> ();
    visualStmList = new JList<Node> (visualStmView);
    visualStmList.setCellRenderer (new ListNodeRenderer (model));
    visualStmList.addMouseListener(new MouseAdapter() {
      public void mouseClicked(MouseEvent evt) {
        if (evt.getClickCount() == 2) { 
          int index = visualStmList.locationToIndex(evt.getPoint());
          new NodeView (model, visualStmView.getElementAt (index));
        }
      }
    });
    visualPanel.add (new JScrollPane (visualStmList));

    JPanel verbalPanel = new JPanel ();
    verbalPanel.setLayout (new GridLayout (1, 1));
    verbalPanel.setBorder (new TitledBorder ("Verbal STM"));
    verbalStmView = new DefaultListModel<Node> ();
    verbalStmList = new JList<Node> (verbalStmView);
    verbalStmList.setCellRenderer (new ListNodeRenderer (model));
    verbalStmList.addMouseListener(new MouseAdapter() {
      public void mouseClicked(MouseEvent evt) {
        if (evt.getClickCount() == 2) { 
          int index = verbalStmList.locationToIndex(evt.getPoint());
          new NodeView (model, verbalStmView.getElementAt (index));
        }
      }
    });
    verbalPanel.add (new JScrollPane (verbalStmList));
  
    JSplitPane jsp = new JSplitPane (JSplitPane.VERTICAL_SPLIT, visualPanel, verbalPanel);
    jsp.setOneTouchExpandable (true);
    add (jsp);

    update ();
  }

  public void update () {
    visualStmView.clear ();
    for (Node node : model.getVisualStm ()) {
      visualStmView.addElement (node);
    }
    visualStmList.setModel (visualStmView);

    verbalStmView.clear ();
    for (Node node : model.getVerbalStm ()) {
      verbalStmView.addElement (node);
    }
    verbalStmList.setModel (verbalStmView);
  }

  private static final long serialVersionUID = 1L;
}

