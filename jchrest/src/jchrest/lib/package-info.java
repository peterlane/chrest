/**
 * Provides supporting data structures and processes.
 * The supporting classes can be divided into groups, as follows:
 *
 * <ol>
 * <li>Patterns: the patterns used within Chrest are complex. They can be of 
 *     different modalities, and consist of string or numeric objects. The 
 *     main class is ListPattern, which is a list containing a number of 
 *     PrimitivePattern instances: examples include StringPattern and NumberPattern.
 *     </li>
 * <li>Scenes: classes to manage collections of and individual scenes, for the 
 *     model to scan and learn from.
 *     </li>
 * <li>Domain specific information: GenericDomain and ChessDomain are provided. These 
 *     classes allow Chrest's treatment of patterns and heuristics for scanning 
 *     scenes to be varied, to suit different applications.
 *     </li>
 * <li>Utility classes: including classes to help with files, eye fixations and 
 *     other definitions.
 *     </li>
 * </ol>
*/
package jchrest.lib;

