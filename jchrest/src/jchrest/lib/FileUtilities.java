package jchrest.lib;

import java.awt.Component;
import java.io.*;
import java.lang.Character;
import javax.swing.*;

/** A collection of static methods used when handling files. */
public class FileUtilities {
  // -- fields and methods to handle a file dialog
	private final static JFileChooser fileChooser = new JFileChooser (System.getProperty("user.dir"));

  /**
   * Request user for a filename to save to.  Argument 'parent' centres the dialog on the 
   * given component.
   *
   * @param parent used for centering the dialog.
   * @return File reference
   */
  public static File getSaveFilename (Component parent) {
    return FileUtilities.getSaveFilename (parent, "Save");
  }

  /**
   * Request user for a filename to save to.  Argument 'parent' centres the dialog on the 
   * given component.  Argument 'title' gives a title for the dialog.
   *
   * @param parent used for centering the dialog.
   * @param title for filechooser dialog
   * @return File reference
   */
  public static File getSaveFilename (Component parent, String title) {
    fileChooser.setDialogTitle (title);
    fileChooser.setMultiSelectionEnabled (false);
    fileChooser.setSelectedFile (new File ("x")); // clear the previous selection
    fileChooser.setSelectedFile (new File (""));
    int option = fileChooser.showSaveDialog (parent);
    if (option == JFileChooser.APPROVE_OPTION) {
      File filename = fileChooser.getSelectedFile ();
      if (filename.exists()) {
        int overwrite = JOptionPane.showConfirmDialog(parent, 
            "File " + filename.getName() +
						" exists.  Are you sure you want to overwrite it?",
						"Warning: Overwriting file",
						JOptionPane.YES_NO_OPTION);
				if (overwrite == JOptionPane.YES_OPTION) {
					return filename;
				} else {
					return getSaveFilename (parent);
				}
			} else {
				return filename;
			}
		}
		return null;
	}

  /**
   * Get a filename to load from.
   *
   * @param parent used for centering the dialog.
   * @return File reference
   */
  public static File getLoadFilename (Component parent) {
    fileChooser.setMultiSelectionEnabled (false);
    fileChooser.setSelectedFile (new File ("x")); // clear the previous selection
    fileChooser.setSelectedFile (new File (""));
    int option = fileChooser.showOpenDialog (parent);
    if (option == JFileChooser.APPROVE_OPTION) {
      File filename = fileChooser.getSelectedFile ();
      if (filename.exists()) {
        return filename;
      } else {
        JOptionPane.showMessageDialog(parent, 
            "File " + filename.getName() +
            " does not exist.  You need to select an existing file.",
            "Error: No file exists",
            JOptionPane.ERROR_MESSAGE);
        return getLoadFilename (parent);
      }
		}
		return null;
	}

}

