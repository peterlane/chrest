package jchrest.lib;

/**
 * A Fixation is an (x,y) location on a scene, and the type records the reason for the 
 * fixation being made.
 */
public class Fixation {

  /**
   * Constructor for fixation.
   *
   * @param type defines the type of this fixation
   * @param x coordinate
   * @param y coordinate
   */
  public Fixation (FixationType type, int x, int y) {
    this.type = type;
    this.x = x;
    this.y = y;
  }

  /**
   * Retrieve the type of fixation.
   *
   * @return type of fixation
   */
  public FixationType getType () {
    return type;
  }

  /**
   * Retrieve a string describing this fixation type.
   *
   * @return string describing this fixation type
   */
  public String getHeuristicDescription () {
    return type.toString ();
  }
 
  /**
   * Retrieve the x coordinate of this fixation.
   *
   * @return x coordinate of this fixation
   */
  public int getX () {
    return x;
  }

  /**
   * Retrieve the y coordinate of this fixation.
   *
   * @return y coordinate of this fixation
   */
  public int getY () {
    return y;
  }

  /**
   * Returns a string describing the location and type of this fixation.
   * Note: added 1 to x,y coordinates to match up with the display of visual scenes.
   *
   * @return string representation of this fixation
   */
  public String toString () {
    return "(" + (x+1) + ", " + (y+1) + ") " + type;
  }

  // private fields
  private final FixationType type;
  private final int x;
  private final int y;
}

