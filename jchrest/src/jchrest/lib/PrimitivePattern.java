package jchrest.lib;

/** 
 * The PrimitivePattern is an abstract class for a group of pattern 
 * types which can be used within a compound pattern.  
 */
public abstract class PrimitivePattern extends Pattern {
}
