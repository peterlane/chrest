package jchrest.lib;

/**
 * Hold the move of a piece to a position on a scene.
 */
public class Move {

  public Move (String piece, int row, int column) {
    this.piece = piece;
    this.row = row;
    this.column = column;
  }

  public ListPattern asListPattern () {
    ListPattern result = new ListPattern (Modality.ACTION);
    result.add (new ItemSquarePattern (piece, column, row));
    result.setFinished ();
    return result;
  }

  public String toString () { return piece + " " + row + " " + column; }

  public String getPiece () { return piece; }
  public int getRow () { return row; }
  public int getColumn () { return column; }

  private final String piece;
  private final int row;
  private final int column;
}

