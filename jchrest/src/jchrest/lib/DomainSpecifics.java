package jchrest.lib;

import java.util.List;
import java.util.Set;
import jchrest.architecture.Chrest;

/**
  * An interface for defining domain-specific methods. 
  * The internal pattern representation and eye-movement heuristics can 
  * each be specialised to a specific domain or experiment.
  */
public interface DomainSpecifics {
  /**
   * In some domains, the ListPattern should be in a specific form. 
   * This method will apply whatever transformation is needed to new 
   * ListPattern instances before their use.
   *
   * @param pattern to transform.
   * @return a new instance of ListPattern.
   */
  public ListPattern normalise (ListPattern pattern);

  /**
   * Identifies salient squares in the scene. Depending on the domain, 
   * some squares will appear more salient than others in a given scene.
   *
   * @param scene is the scene the model is currently looking at.
   * @param model is the current model.
   * @return a Set of squares regarded as salient.
   */
  public Set<Square> proposeSalientSquareFixations (Scene scene, Chrest model);

  /**
   * Identifies squares in the scene which may be the next location of the 
   * examined object moving in the scene. This heuristic is used to simulate 
   * how the future location of an object may be predicted, and its trajectory
   * examined.
   *
   * @param scene is the scene the model is currently looking at.
   * @param square is the square currently being examined.
   * @return a List of squares to examine.
   */
  public List<Square> proposeMovementFixations (Scene scene, Square square);
}

