package jchrest.lib;

/**
 * The PairedPattern holds two associated patterns.  The typical use for 
 * this structure is within a categorisation experiment, where one pattern 
 * will be the visual pattern to name and the second will be the verbal 
 * pattern to name it.
 */
public class PairedPattern {
  private ListPattern first, second;

  public PairedPattern (ListPattern first, ListPattern second) {
    this.first = first;
    this.second = second;
  }

  public ListPattern getFirst () {
    return first;
  }

  public ListPattern getSecond () {
    return second;
  }
}

