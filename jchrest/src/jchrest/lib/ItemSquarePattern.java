package jchrest.lib;

import java.util.Objects;

/**
 * The ItemSquarePattern is a type of PrimitivePattern used to hold 
 * objects places on a square array.  The item-on-square is treated 
 * as a single object.  Instances of this class are immutable.
 */
public class ItemSquarePattern extends PrimitivePattern {

  /**
   * Constructor takes a string to identify the item, and a column and row
   * to identify the square.
   *
   * @param item describing the item on the square
   * @param column coordinate
   * @param row coordinate
   */
  public ItemSquarePattern (String item, int column, int row) {
    this.item = item;
    this.column = column;
    this.row = row;
  }

  /** 
   * Accessor method for the stored item.
   *
   * @return description of item on the square
   */
  public String getItem () {
    return item;
  }

  /**
   * Accessor method for the column.
   *
   * @return column coordinate of this item
   */
  public int getColumn () {
    return column;
  }

  /**
   * Accessor method for the row.
   *
   * @return row coordinate of this item
   */
  public int getRow () {
    return row;
  }

  /**
   * Two ItemSquarePatterns are only equal if all their parts are the same.
   *
   * @param object to compare with
   * @return true if the given pattern equals this pattern
   */
  @Override
  public boolean equals (Object object) {
    if (object instanceof ItemSquarePattern) {
      ItemSquarePattern ios = (ItemSquarePattern)object;
      return (item.equals (ios.getItem ()) &&
          column == ios.getColumn () &&
          row == ios.getRow ());
    } else {
      return false;
    }
  }

  /** 
   * Two ItemSquarePatterns only match if they are the same.
   *
   * @param givenPattern to compare with
   * @return true if this pattern matches the given pattern
   */
  public boolean matches (Pattern givenPattern) {
    if (!(givenPattern instanceof ItemSquarePattern)) return false;
    return this.equals ((ItemSquarePattern)givenPattern);
  }

  /**
   * Return a string representation of the item on square.
   * @return string representation of this pattern
   */
  public String toString () {
    return "[" + item + " " + column + " " + row + "]";
  }

  /**
   * As we override equals, we must also implement hashCode.
   *
   * @return hash code for this pattern
   */
  @Override
  public int hashCode () {
    return Objects.hash (item, column, row);
  }

  // private fields
  private final String item;
  private final int column;
  private final int row;

  private static final long serialVersionUID = 1L;
}

