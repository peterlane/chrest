package jchrest.lib;

// TODO: Clarify order of row/column in methods calls/displays.

/**
 * Stores information about an individual scene.
 * Scenes are represented as two-dimensional grids, on which named items are placed.
 */
public class Scene {
  private String name;
  private int height;
  private int width;
  private String[][] scene;

  /**
   * Create a new, empty scene of given size and name.
   *
   * @param name for the scene
   * @param height of the scene (the number of rows)
   * @param width of the scene (the number of columns)
   */
  public Scene (String name, int height, int width) {
    this.name = name;
    this.height = height;
    this.width = width;
    scene = new String[height][width];
    // make scene empty to start
    for (int row = 0; row < height; row++) {
      for (int col = 0; col < width; col++) {
        scene[row][col] = ".";
      }
    }
  }

  /**
   * Returns the scene's name.
   * @return name of the scene
   */
  public String getName () {
    return name;
  }

  /**
   * Returns the scene's height.
   * @return height of the scene
   */
  public int getHeight () {
    return height;
  }

  /**
   * Returns the scene's width.
   * @return width of the scene
   */
  public int getWidth () {
    return width;
  }

  /**
   * Given a list of characters, adds these items as contents of given row.
   *
   * @param row to add the items to
   * @param items to add to the given row
   */
  public void addRow (int row, char [] items) {
    for (int i = 0; i < items.length; ++i) {
      scene[row][i] = items[i] + "";
    }
  }

  /**
   * Retrieve the name of the item at the given location.
   *
   * @param row index of item to retrieve
   * @param column index of item to retrieve
   * @return name of item at given location, or "" if location out of bounds
   */
  public String getItem (int row, int column) {
    if (row >= 0 && row < height && column >= 0 && column < width) {
      return scene[row][column];
    } else {
      return "";
    }
  }

  /**
   * Puts item onto scene.
   *
   * @param row index of item on scene
   * @param column index of item on scene
   * @param item to place onto scene at given location
   */
  public void setItem (int row, int column, String item) {
    assert (row >= 0 && row < height && column >= 0 && column < width);
    scene[row][column] = item;
  }

  /**
   * Returns true if given location is empty or off scene.
   *
   * @param row index of square on scene
   * @param column index of square on scene
   * @return true if given location is empty or off scene
   */
  public boolean isEmpty (int row, int column) {
    if (row >= 0 && row < height && column >= 0 && column < width) {
      return scene[row][column].equals (".");
    } else {
      return true; // no item off scene (!)
    }
  }
  
  /**
   * Retrieve all items within given row +/- size, column +/- size
   * TODO: Convert this to use a circular field of view.
   *
   * @param startRow index on scene
   * @param startColumn index on scene
   * @param size of field of view
   * @return pattern of items within field of view
   */
  public ListPattern getItems (int startRow, int startColumn, int size) {
    ListPattern items = new ListPattern ();

    for (int col = startColumn - size; col <= startColumn + size; ++col) {
      if (col >= 0 && col < width) {
        for (int row = startRow - size; row <= startRow + size; ++row) {
          if (row >= 0 && row < height) {
            if (!scene[row][col].equals(".")) {
              items.add (new ItemSquarePattern (scene[row][col], col+1, row+1));

            }
          }
        }
      }
    }

    return items;
  }

  /**
   * Count the number of non-empty squares in the scene.
   * @return number of non-empty squares in scene
   */
  public int countItems () {
    int items = 0;
    for (int row = 0; row < height; row++) {
      for (int col = 0; col < width; col++) {
        if (isEmpty (row, col)) {
          ;
        } else {
          items += 1;
        }
      }
    }
    return items;
  }

  /**
   * Compare this and given scenes, and return a count of the number of 
   * pieces in common to both.
   *
   * @param comparisonScene scene to compare this scene against
   * @return count of pieces in common to this and given scene
   */
  public int countOverlappingPieces (Scene comparisonScene) {
    int items = 0;
    for (int row = 0; row < height; row++) {
      for (int col = 0; col < width; col++) {
        if (isEmpty (row, col)) {
          ;
        } else if (this.scene[row][col].equals (comparisonScene.getItem (row, col))) {
          items += 1;
        } else {
          ;
        }
      }
    }
    return items;
  }         

  /**
   * Compute precision of given scene against this one.
   * Precision is the proportion of pieces in given scene which are correct.
   *
   * @param scene to compare this scene against
   * @return precision of this and given scenes
   */
  public float computePrecision (Scene scene) {
    if (scene.countItems() == 0) {
      return 0.0f;
    } else {
      return (float)countOverlappingPieces(scene) / (float)scene.countItems();
    }
  }

  /**
   * Compute recall of given scene against this one.
   * Recall is the proportion of pieces in this scene which have been correctly recalled.
   *
   * @param scene to compare this scene against
   * @return recall of this and given scenes
   */
  public float computeRecall (Scene scene) {
    if (this.countItems() == 0) {
      return 0.0f;
    } else {
      return (float)countOverlappingPieces(scene) / (float)this.countItems();
    }
  }
  
  /**
   * Compute errors of omission of given scene against this one.
   * Omission is the number of pieces which are in this scene but not in the given one.
   *
   * @param comparisonScene to compare this scene against
   * @return number of errors of omission
   */
  public int computeErrorsOfOmission (Scene comparisonScene) {
    int errors = 0;
    for (int row = 0; row < height; row++) {
      for (int col = 0; col < width; col++) {
        if (isEmpty(row, col)) {
          ; // do nothing for empty squares
        } else if (this.scene[row][col].equals (comparisonScene.getItem (row, col))) {
          ; // no error if this and given scene have the same item
        } else { // an item in this scene is not in given scene
          errors += 1;
        }
      }
    }
    return errors;
  }

  /**
   * Compute errors of commission of given scene against this one.
   * Commission is the number of pieces which are in the given scene but not in this one.
   *
   * @param comparisonScene to compare this scene against
   * @return number of errors of commission
   */
  public int computeErrorsOfCommission (Scene comparisonScene) {
    int errors = 0;
    for (int row = 0; row < height; row++) {
      for (int col = 0; col < width; col++) {
        if (comparisonScene.isEmpty (row, col)) {
          ; // do nothing for empty squares in given scene
        } else if (comparisonScene.getItem(row, col).equals (this.scene[row][col])) {
          ; // no error if given and this scene have the same item
        } else { // an item in given scene is not in this scene
          errors += 1;
        }
      }
    }

    return errors;
  }
}

