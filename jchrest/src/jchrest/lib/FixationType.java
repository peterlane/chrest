package jchrest.lib;

/**
 * Enumerated type to manage the fixation type.
 */
public enum FixationType {
  /** default - no heuristic used */
  none ("No heuristic"),
  /** heuristic for choosing first fixation point */
  start ("First heuristic"),
  /** heuristic using salient locations */
  salient ("Salient location heuristic"),
  /** heuristic using long-term memory links */
  ltm ("LTM heuristic"),
  /** chooses a random item in periphery */
  randomItem ("Random item heuristic"),
  /** chooses a random place in periphery */
  randomPlace ("Random place heuristic"),
  /** heuristic guiding eye to scan otherwise unseen parts of a scene */
  global ("Global strategy"),
  /** heuristic to move eye based on a predicted movement */
  proposedMove ("Proposed movement heuristic") // TODO: this needs a better name
  ;

  private FixationType (String description) {
    this.description = description;
  }

  private final String description;

  public String toString () {
    return description;
  }
}

