package jchrest.lib;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import jchrest.lib.FileUtilities;

/**
 * The ListPattern is the primary datatype used to represent compound 
 * patterns within {@link jchrest.architecture.Chrest}.  
 * A ListPattern holds an ordered list of 
 * instances of other pattern types.  The ListPattern may optionally 
 * indicate that it cannot be extended by setting the finished flag.
 * Note that once a pattern is 'finished', it cannot be added to.
 * <p>
 * TODO: Think about if ListPatterns can be embedded within ListPatterns
 *       - would have to look inside ListPattern to make the match.
 */
public class ListPattern extends Pattern implements Iterable<PrimitivePattern> {
  private List<PrimitivePattern> list;  // items within the pattern
  private Modality modality;   // record type of ListPattern
  private boolean finished;    // marker to indicate if pattern complete

  /** Create an empty visual pattern */
  public ListPattern () {
    this (Modality.VISUAL);
  }

  /** 
   * Create an empty pattern of given modality
   *
   * @param modality of pattern to create
   */
  public ListPattern (Modality modality) {
    list = new ArrayList<PrimitivePattern> ();
    this.modality = modality;
    finished = false;
  }

  /** 
   * Used in constructing instances by {@link Pattern} class.
   * Add pattern to list, unless the pattern is 'finished'.
   *
   * @param pattern to add 
   */
  public void add (PrimitivePattern pattern) {
    if (!finished) {
      list.add (pattern);
    }
  }

  /**
   * Construct a copy of this pattern, so that it can be modified 
   * without affecting the original.
   *
   * @return clone of this pattern
   */
  public ListPattern clone () {
    ListPattern result = new ListPattern (modality);
    for (PrimitivePattern pattern : list) {
      result.add (pattern);
    }
    if (isFinished ()) {
      result.setFinished ();
    }
    return result;
  }

  /**
   * Return the number of patterns held inside the list pattern.
   * @return the number of patterns held inside the list pattern.
   */
  public int size () {
    return list.size ();
  }

  /**
   * Check if the list pattern is empty, holding no patterns.
   * @return true if pattern is empty
   */
  public boolean isEmpty () {
    return list.isEmpty ();
  }

  /**
   * Retrieve the indexed item from the list pattern.
   * There is no check on the validity of the index.
   *
   * @param index of pattern to retrieve
   * @return pattern at given index position
   */
  public PrimitivePattern getItem (int index) {
    return list.get (index);
  }

  /**
   * Accessor method to finished property. 
   * A pattern is <em>finished</em> if no more items 
   * can be added to it.
   *
   * @return true if pattern is finished.
   */
  public boolean isFinished () {
    return finished;
  }

  /**
   * Class level method to check if two patterns have the same modality.
   *
   * @param pattern1 the first of the two patterns to compare
   * @param pattern2 the second of the two patterns to compare
   * @return true if the two patterns are the same modality
   */
  static public boolean isSameModality (ListPattern pattern1, ListPattern pattern2) {
    return pattern1.modality == pattern2.modality;
  }

  /**
   * Accessor to retrieve the modality of the pattern.
   *
   * @return modality of pattern
   */
  public Modality getModality () {
    return modality;
  }

  /**
   * Mutator to change modality of pattern.
   *
   * @param modality for this pattern to become
   */
  public void setModality (Modality modality) {
    this.modality = modality;
  }

  /**
   * Accessor method to check visual modality.
   *
   * @return true if this pattern is a visual pattern
   */
  public boolean isVisual () {
    return modality == Modality.VISUAL;
  }

  /**
   * Accessor method to check verbal modality.
   *
   * @return true if this pattern is a verbal pattern
   */
  public boolean isVerbal () {
    return modality == Modality.VERBAL;
  }

  /**
   * Accessor method to check action modality.
   *
   * @return true if this pattern is an action pattern
   */
  public boolean isAction () {
    return modality == Modality.ACTION;
  }

  /**
   * Convert the modality into a string.
   *
   * @return string for modality
   */
  public String getModalityString () {
    if (isVisual ()) {
      return "Visual";
    } else if (isVerbal ()) {
      return "Verbal";
    } else { // if (isAction ())
      return "Action";
    }
  }

  /**
   * Set the finished property to true.
   */
  public void setFinished () {
    finished = true;
  }

  /**
   * Set the finished property to false.
   */
  public void setNotFinished () {
    finished = false;
  }

  /** 
   * Two patterns are equal if they contain the same items.
   *
   * @param pattern to compare to
   * @return true if given pattern equals this pattern
   */
  public boolean equals (ListPattern pattern) { 
    if (modality != pattern.modality) return false;

    // patterns must be equal size to be equal
    if (size () != pattern.size ()) return false;

    for (int i = 0, n = size (); i < n; ++i) {
      if (!pattern.getItem(i).equals(getItem(i))) {
        return false; // false if any item not the same
      }
    }
    // else, they must both have the 'finished' property the same
    return finished == pattern.isFinished ();
  }

  /** 
   * Two patterns match if they are both ListPatterns and this ListPattern
   * is a presequence of given pattern. 
   *
   * @param givenPattern to compare to
   * @return true if this pattern matches given pattern
   */
  public boolean matches (Pattern givenPattern) {
    if (!(givenPattern instanceof ListPattern)) return false;
    ListPattern pattern = (ListPattern)givenPattern;

    if (modality != pattern.modality) return false;

    // check relative sizes of patterns
    if (isFinished ()) {
      if (size () != pattern.size ()) return false;
      if (!pattern.isFinished ()) return false;

    } else {
      // this pattern cannot be larger than given pattern to match it.
      if (size () > pattern.size ()) return false;
    }
    // now just check that the items in this pattern match up with the given pattern
    for (int i = 0, n = size (); i < n; ++i) {
      if (!pattern.getItem(i).equals(getItem (i))) {
        return false; // false if any item not the same
      }
    }
    return true;

  }

  /**
   * Return a new ListPattern forming the parts of this pattern without 
   * the matching elements of the given pattern.
   *
   * @param pattern to compare with
   * @return pattern formed by removing matching elements of given pattern
   */
  public ListPattern remove (ListPattern pattern) {
    ListPattern result = new ListPattern (modality);

    int i = 0;
    boolean takingItems = false;
    while (i < size ()) {
      if (takingItems) {
        result.add (getItem (i));
      } else if (i < pattern.size () && pattern.getItem(i).equals(getItem (i))) {
        ;
      } else {
        takingItems = true;
        result.add (getItem (i));
      }
      i += 1;
    }
    if (isFinished () && !(result.isEmpty () && pattern.isFinished ())) {
      result.setFinished ();
    }

    return result;
  }

  /**
   * Return a new ListPattern formed from the contents of this list pattern and the 
   * contents of the given pattern appended to it.
   *
   * @param pattern to append to this pattern
   * @return new pattern formed from appending given pattern to this pattern
   */
  public ListPattern append (ListPattern pattern) {
    ListPattern result = new ListPattern (modality);

    for (PrimitivePattern item : list) {
      result.add (item);
    }

    for (PrimitivePattern item : pattern) { 
      result.add (item);
    }

    if (pattern.isFinished ()) {
      result.setFinished ();
    }

    return result;
  }

  /** Return a new ListPattern formed from the contents of this list pattern and 
   * the given PrimitivePattern appended to it.
   *
   * @param pattern to append to this pattern
   * @return new pattern formed by appended given pattern to this pattern 
   */
  public ListPattern append (PrimitivePattern pattern) {
    ListPattern result = new ListPattern (modality);

    for (PrimitivePattern item : list) {
      result.add (item);
    }
    result.add (pattern);

    return result;
  }

  /**
   * Construct a new pattern containing just the first item in this one.
   *
   * @return new pattern formed from the first item of this pattern
   */
  public ListPattern getFirstItem () {
    ListPattern pattern = new ListPattern (modality);
    if (size () > 0) {
      pattern.add (getItem (0));
    }
    pattern.setFinished ();

    return pattern;
  }

  /**
   * Render the list pattern as a string.
   *
   * @return string representation of this pattern
   */
  public String toString () {
    String result = "< ";
    for (PrimitivePattern pattern : list) {
      result += pattern.toString () + " ";
    }
    if (finished) result += "$ ";

    return result + ">";
  }

  /**
   * Check if this pattern contains the given primitive pattern.
   *
   * @param given primitive pattern to check
   * @return true if given is contained in this pattern
   */
  public boolean contains (PrimitivePattern given) {
    for (PrimitivePattern item : list) {
      if (item.equals (given)) return true;
    }
    return false;
  }

  /**
   * Compare this list pattern with a given list pattern, returning true if 
   * the two share k or more items.
   *
   * @param pattern to compare with
   * @param k number of similar items to check for
   * @return true if given pattern and this pattern share k or more items
   */
  public boolean isSimilarTo (ListPattern pattern, int k) {
    int count = 0;

    for (PrimitivePattern item : list) {
      if (pattern.contains (item)) {
        count += 1;
        // remove the matching item from pattern
        ListPattern itemPattern = new ListPattern (modality);
        itemPattern.add (item);
        pattern = pattern.remove (itemPattern);
      }
      if (count >= k) return true;
    }

    return false;
  }

  /**
   * Return a new list pattern with the items sorted using the given comparator.
   *
   * @param comparator used to sort the items
   * @return new pattern formed from this pattern's elements, but sorted 
   *         according to the <em>comparator</em>
   */
  public ListPattern sort (Comparator<PrimitivePattern> comparator) {
    ListPattern result = new ListPattern (modality);
    List<PrimitivePattern> items = new ArrayList<PrimitivePattern> ();
    for (PrimitivePattern pattern : list) {
      items.add (pattern);
    }
    Collections.sort (items, comparator);
    for (PrimitivePattern pattern : items) {
      result.add (pattern);
    }
    if (isFinished ()) {
      result.setFinished ();
    }
    return result;
  }

  /** 
   * Support iteration over the items of a list pattern.
   *
   * @return an iterator over the pattern
   */
  public Iterator<PrimitivePattern> iterator () {
    return new ListPatternIterator (list);
  }
  
  class ListPatternIterator implements Iterator<PrimitivePattern> {
    private int index = 0;
    private List<PrimitivePattern> items;

    ListPatternIterator (List<PrimitivePattern> items) {
      this.items = items;
    }

    public boolean hasNext () {
      return index < items.size ();
    }

    public PrimitivePattern next () {
      if (hasNext ()) {
        index += 1;
        return items.get(index-1);
      }
      throw new java.util.NoSuchElementException();
    }

    public void remove () {
      throw new UnsupportedOperationException ();
    }
  }
}

