package jchrest.lib;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * The StringPattern is a type of PrimitivePattern used to hold 
 * Strings.  The String is treated as a single object, and 
 * cannot be decomposed into smaller elements, such as letters.
 * Instances of this class are immutable.
 */
public class StringPattern extends PrimitivePattern {

  /**
   * Static creator method tries to retrieve a cached pattern for given string,
   * else creates and returns a new instance of StringPattern.
   *
   * @param name to convert into a string pattern
   * @return the new string pattern
   */
  public static StringPattern create (String name) {
    if (!cache.containsKey (name)) {
      cache.put (name, new StringPattern (name));
    }
    return cache.get (name);
  }

  /** 
   * Constructor takes a String to define the contents of this pattern.
   *
   * @param name to wrap
   */
  private StringPattern (String name) {
    this.name = name;
  }

  /**
   * Accessor method for the stored name.
   * @return the string for this string pattern
   */
  public String getString () {
    return name;
  }

  /**
   * Two StringPatterns are only equal if their stored names 
   * are the same.
   *
   * @param pattern to compare with
   * @return true if the given pattern equals this pattern
   */
  @Override
  public boolean equals (Object pattern) {
    if (pattern instanceof StringPattern) {
      return name.equals (((StringPattern)pattern).getString ());
    } else {
      return false;
    }
  }

  /** 
   * Two StringPatterns only match if their stored names are the same.
   *
   * @param pattern to compare with
   * @return true if this pattern matches the given pattern
   */
  public boolean matches (Pattern pattern) {
    if (pattern instanceof StringPattern) {
      return name.equals (((StringPattern)pattern).getString ());
    } else {
      return false;
    }
  }

  /**
   * Return a string representation of this pattern.
   * @return string representation of this pattern
   */
  public String toString () {
    return name;
  }

  /**
   * As we override equals, we must also implement hashCode.
   *
   * @return hash code for this pattern
   */
  @Override 
  public int hashCode () { 
    return Objects.hash (name);
  }

  // private fields
  private final String name;
  private static final Map<String, StringPattern> cache = new HashMap<> ();

  private static final long serialVersionUID = 1L;
}

