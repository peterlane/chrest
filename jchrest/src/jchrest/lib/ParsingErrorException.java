package jchrest.lib;

/** 
 * Exception to signal an error when reading in a model definition file.
 */
public class ParsingErrorException extends Exception {
  private String message;

  public ParsingErrorException () {
    message = "UNKNOWN";
  }

  public ParsingErrorException (String message) {
    this.message = message;
  }

  public String getMessage () {
    return message;
  }

  private static final long serialVersionUID = 1L;
}
