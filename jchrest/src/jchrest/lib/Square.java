package jchrest.lib;

/**
 * Square is a convenience class to hold a row and column.
 */
public class Square {
  private int column;
  private int row;

  /**
   * Constructor makes an instance from given row and column.
   *
   * @param row for square
   * @param column for square
   */
  public Square (int row, int column) {
    this.column = column;
    this.row = row;
  }

  /**
   * Accessor method for the column.
   * @return column value
   */
  public int getColumn () {
    return column;
  }

  /**
   * Accessor method for the row.
   * @return row value
   */
  public int getRow () {
    return row;
  }

  public String toString () {
    return "(" + row + ", " + column + ")";
  }
}

