package jchrest.lib;

/** 
 * Parent class of all patterns.
 */
public abstract class Pattern {

  /**
   * Factory method to make a NumberPattern.
   *
   * @param number to wrap
   * @return number pattern
   */
  public static NumberPattern makeNumber (int number) {
    return NumberPattern.create (number);
  }

  /**
   * Factory method to make a StringPattern.
   *
   * @param str to wrap
   * @return string pattern
   */
  public static StringPattern makeString (String str) {
    return StringPattern.create (str);
  }

  private static ListPattern makeList (int[] numbers, Modality modality) {
    ListPattern list = new ListPattern (modality);
    for (int i = 0; i < numbers.length; ++i)
    {
      list.add (NumberPattern.create (numbers[i]));
    }
    return list;
  }

  /** Factory method to make a visual ListPattern given an array of numbers.
   * Each number is converted into a NumberPattern and added to the 
   * ListPattern.
   *
   * @param numbers an array of numbers, items in the pattern
   * @return a new pattern containing the given numbers
   */
  public static ListPattern makeVisualList (int[] numbers) {
    return makeList (numbers, Modality.VISUAL);
  }

  /** Factory method to make a verbal ListPattern given an array of numbers.
   * Each number is converted into a NumberPattern and added to the 
   * ListPattern.
   *
   * @param numbers an array of numbers, items in the pattern
   * @return a new pattern containing the given numbers
   */
  public static ListPattern makeVerbalList (int[] numbers) {
    return makeList (numbers, Modality.VERBAL);
  }

  private static ListPattern makeList (String[] strings, Modality modality) {
    ListPattern list = new ListPattern (modality);
    for (int i = 0; i < strings.length; ++i)
    {
      list.add (StringPattern.create (strings[i]));
    }
    return list;
  }

  /** Factory method to make a visual ListPattern given an array of Strings.
   * Each number is converted into a StringPattern and added to the 
   * ListPattern.
   *
   * @param strings an array of strings, items in the pattern
   * @return a new pattern containing the given strings
   */
  public static ListPattern makeVisualList (String[] strings) {
    return makeList (strings, Modality.VISUAL);
  }

  /** Factory method to make a verbal ListPattern given an array of Strings.
   * Each number is converted into a StringPattern and added to the 
   * ListPattern.
   *
   * @param strings an array of strings, items in the pattern
   * @return a new pattern containing the given strings
   */
  public static ListPattern makeVerbalList (String[] strings) {
    return makeList (strings, Modality.VERBAL);
  }

  /** Factory method to make an action ListPattern given an array of Strings.
   * Each number is converted into a StringPattern and added to the 
   * ListPattern.
   *
   * @param strings an array of strings, items in the pattern
   * @return a new pattern containing the given strings
   */
  public static ListPattern makeActionList (String[] strings) {
    return makeList (strings, Modality.ACTION);
  }

  public abstract boolean matches (Pattern pattern);
  public abstract String toString ();
}

