package jchrest.lib;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * The NumberPattern is a type of PrimitivePattern used to hold 
 * numbers.  The number is treated as a single object.  Instances 
 * of this class are immutable.
 */
public class NumberPattern extends PrimitivePattern {

  /**
   * Static creator method attempts to retrieve a cached instance for given 
   * number, else creates and returns a new NumberPattern instance.
   *
   * @param num to convert into a number pattern
   * @return the new number pattern
   */
  public static NumberPattern create (int num) {
    Integer number = Integer.valueOf (num);
    if (!cache.containsKey (number)) {
      cache.put (number, new NumberPattern (num));
    }
    return cache.get (number);
  }

  /** 
   * Constructor takes an int to define the contents of this pattern.
   * @param number to wrap
   */
  private NumberPattern (int number) {
    this.number = number;
  }

  /** 
   * Accessor method for the stored number.
   * @return the int value for this number pattern
   */
  public int getNumber () {
    return number;
  }

  /**
   * Two NumberPatterns are only equal if their stored numbers are the same.
   *
   * @param pattern to compare with
   * @return true if the given pattern equals this pattern
   */
  @Override
  public boolean equals (Object pattern) {
    if (pattern instanceof NumberPattern) {
      return number == ((NumberPattern)pattern).getNumber ();
    } else {
      return false;
    }
  }

  /**
   * Two NumberPatterns only match if their stored numbers are the same.
   *
   * @param pattern to compare with
   * @return true if this pattern matches the given pattern
   */
  public boolean matches (Pattern pattern) {
    if (pattern instanceof NumberPattern) {
      return number == ((NumberPattern)pattern).getNumber ();
    } else {
      return false;
    }
  }

  /**
   * Return a string representation of this pattern.
   * @return string representation of this pattern.
   */
  public String toString () {
    return "" + number;
  }

  /**
   * As we override equals, we must also implement hashCode.
   *
   * @return hash code for this pattern
   */
  @Override
  public int hashCode () {
    return Objects.hash (number);
  }

  // private fields
  private final int number;
  private static final Map<Integer, NumberPattern> cache = new HashMap<> ();

  private static final long serialVersionUID = 1L;
}

