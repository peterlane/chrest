package jchrest.architecture;

import java.io.*;
import java.lang.Math;
import java.util.*;

/**
 * Represents a complex emotional tag
 *
 * @author Marvin Schiller
 */
public class EmotionalTrace {
    
    // private float _strength;
    // private Node source; not needed, is in the association list
    private Map<Integer,Emotion> emotionHistory = new HashMap<Integer,Emotion>();
    private Emotion RescorlaWagnerEmotion;
    
    public void addToHistory(int time, Emotion emotion){
        emotionHistory.put(time,emotion);
    } 
    
    public void setRescorlaWagnerEmotion(Emotion emotion){
        RescorlaWagnerEmotion = emotion;
    }
    
    public Emotion getRescorlaWagnerEmotion(){
        return RescorlaWagnerEmotion;
    } 
    
    public void printToStdOut(){
        System.out.println("== Emotional Trace ==");
        System.out.println("= RW Emotion =");
        if (RescorlaWagnerEmotion==null){
            System.out.println("empty");
        }
        else{
            System.out.println(RescorlaWagnerEmotion.displayNONUTF());
        }
        System.out.println("= Emotion History =");
        Set<Integer> timepoints = emotionHistory.keySet();
        Set<Integer> sorted_timepoints = new TreeSet<Integer>(timepoints);
        for (Integer i : sorted_timepoints){
            System.out.print("t=");
            System.out.print(i);
            System.out.print(": ");
            System.out.println(emotionHistory.get(i).displayNONUTF());
        }
        return;
    }
    

}
