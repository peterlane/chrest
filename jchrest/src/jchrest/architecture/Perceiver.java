
// TODO: 
// 1. use fixations instead of separate fixationsX ... fields    DONE
// 2. include 'initial 4' eye fixations heuristics               DONE
// 3. Make fixation heuristics 'like' table 8.1 in book          DONE
//    (except forces 'random place' if others fail)
// 4. Include 'global strategies' for experienced model
// 5. Make learning of pattern be based on fixation sequence,    DONE
//    as with CHREST 2

package jchrest.architecture;

import jchrest.lib.Fixation;
import jchrest.lib.FixationType;
import jchrest.lib.ItemSquarePattern;
import jchrest.lib.ListPattern;
import jchrest.lib.Modality;
import jchrest.lib.Pattern;
import jchrest.lib.Scene;
import jchrest.lib.Square;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Perceiver class manages the model's interaction with an external, two-dimensional 
 * scene.
 */
public class Perceiver {
  private final static java.util.Random random = new java.util.Random ();

  private final Chrest model;
  private int fixationX, fixationY, fieldOfView;
  FixationType lastHeuristic;
  private Scene currentScene;
  private List<Node> recognisedNodes;

  protected Perceiver (Chrest model) {
    this.model = model;
    fixationX = 0;
    fixationY = 0;
    fieldOfView = 2;
    lastHeuristic = FixationType.none;
    fixations = new ArrayList<Fixation> ();
    recognisedNodes = new ArrayList<Node> ();
  }

  public int getFieldOfView () {
    return fieldOfView;
  }

  public void setFieldOfView (int fov) {
    fieldOfView = fov;
  }

  public void setScene (Scene scene) {
    currentScene = scene;
    clearFixations ();
  }

  /** 
   * Initial fixation point - the centre of the scene.
   *
   * @param targetNumberFixations to use in scanning the scene
   */
  public void start (int targetNumberFixations) {
    recognisedNodes.clear ();
    this.targetNumberFixations = targetNumberFixations;

    fixationX = currentScene.getWidth () / 2;
    fixationY = currentScene.getHeight () / 2;
    lastHeuristic = FixationType.start;
    addFixation (new Fixation (lastHeuristic, fixationX, fixationY));
  }

  private boolean doInitialFixation () {
    Set<Square> squares = model.getDomainSpecifics().proposeSalientSquareFixations (currentScene, model);
    if (squares.isEmpty ()) {
      return false;
    } else {
      Square square = (new ArrayList<Square>(squares)).get ((new java.util.Random()).nextInt (squares.size ()));
      addFixation (new Fixation (FixationType.salient, square.getColumn (), square.getRow ()));
      return true;
    }
  }

  /**
   * Try to move eye using LTM heuristic, return true if:
   *   -- square suggested by first child yields a piece which 
   *      allows model to follow a test link.
   * Note: Chrest-2.1 has three further facilities
   *   1. does a call to discrimination first, to update STM
   *   2. checks that move from previous square to proposed square 
   *      has not been done before in this fixation cycle
   *   3. keeps track of how often this hypothesis node has been queried,
   *      so repeat queries will suggest alternate squares
   *      (i.e. proposed square does not lead to descending a link,
   *            so hypothesis not changed, and so next link in sequence will
   *            be tried.)
   */
  private boolean ltmHeuristic () {
    if (model.getVisualStm().getCount () >= 1) {
      List<Link> hypothesisChildren = model.getVisualStm().getItem(0).getChildren ();
      if (hypothesisChildren.isEmpty ()) return false;
      //        System.out.println ("Checking LTM heuristic");
      for (int i = 0; i < hypothesisChildren.size () && i < 1; ++i) { // *** i == 0 only
        ListPattern test = hypothesisChildren.get(i).getTest ();
        if (test.isEmpty ()) continue; // return false;
        Pattern first = test.getItem (0);
        //        System.out.println ("Checking: " + first);
        if (first instanceof ItemSquarePattern) {
          ItemSquarePattern ios = (ItemSquarePattern)first;

          // check if we should make the fixation
          // 1. is it a different square?
          if (ios.getColumn()-1 == fixationX && 
              ios.getRow()-1 == fixationY) {
            ; // return false; // we are already at this square
          } else {
            // all ok, so we make the fixation
            fixationX = ios.getColumn ()-1; // because ios start from 1
            fixationY = ios.getRow ()-1; 
            lastHeuristic = FixationType.ltm;

            addFixation (new Fixation (lastHeuristic, fixationX, fixationY));
            // look at square given by first test link
            // then look to see if a test link has the same square and observed piece
            for (Link link : hypothesisChildren) {
              if (link.getTest().size () == 1) {
                // Note: using first test created gives more uses of LTM heuristic
                if (link.getTest().getItem (link.getTest().size() - 1) instanceof ItemSquarePattern) {
                  ItemSquarePattern testIos = (ItemSquarePattern)link.getTest().getItem (0);
                  // check all details of test are correct
                  if (testIos.getColumn () - 1 == fixationX && 
                      testIos.getRow () - 1 == fixationY &&
                      testIos.getItem().equals (currentScene.getItem (fixationY, fixationX))) {
                    model.getVisualStm().replaceHypothesis (link.getChildNode ());
                      }
                }
              }
            }
            // return true, as we made the fixation
            return true;
          }
        }
      }
    }
    return false;
  }

  /**
   * Try to move eye to random item in periphery.
   */
  private boolean randomItemHeuristic () {

    for (int i = 0; i < 3; ++i) { // *** Parameter controls how likely 'item' over 'place'
      int xDisplacement = random.nextInt (fieldOfView * 2 + 1) - fieldOfView;
      int yDisplacement = random.nextInt (fieldOfView * 2 + 1) - fieldOfView;
      if (!currentScene.isEmpty (fixationY + yDisplacement, fixationX + xDisplacement)
          && fixationX < currentScene.getWidth ()
          && fixationY < currentScene.getHeight ()) {
        fixationX += xDisplacement;
        fixationY += yDisplacement;
        lastHeuristic = FixationType.randomItem;

        return true;
          }
    }
    return false;
  }

  /**
   * Move eye to random position in periphery.
   */
  private void randomPlaceHeuristic () {
    int xDisplacement = random.nextInt (fieldOfView * 2 + 1) - fieldOfView;
    int yDisplacement = random.nextInt (fieldOfView * 2 + 1) - fieldOfView;

    lastHeuristic = FixationType.randomPlace;

    if ((xDisplacement == 0 && yDisplacement == 0) || 
        (fixationX + xDisplacement < 0) ||
        (fixationY + yDisplacement < 0) ||
        (fixationX + xDisplacement >= currentScene.getWidth ()) ||
        (fixationY + yDisplacement >= currentScene.getHeight ())) {
      fixationX += 1;
      // check legality of new fixation
      if (fixationX >= currentScene.getWidth ()) {
        fixationY += 1;
        fixationX = 0;
      }
      if (fixationY >= currentScene.getHeight ()) {
        fixationX = 0;
        fixationY = 0;
      }
    } else {
      fixationX += xDisplacement;
      fixationY += yDisplacement;
    }
  }

  /**
   * Find the next fixation point using one of the available 
   * heuristics.
   */
  private void moveEyeUsingHeuristics () {
    double r = Math.random ();
    boolean fixationDone = false;
    if (r < 0.3333) { // try movement fixation
      List<Square> pieceMoves = model.getDomainSpecifics().proposeMovementFixations (
          currentScene, 
          new Square (fixationY, fixationX)
          );
      if (pieceMoves.size () > 0) { 
        int move = (new java.util.Random ()).nextInt (pieceMoves.size ());
        fixationX = pieceMoves.get(move).getColumn ();
        fixationY = pieceMoves.get(move).getRow ();
        lastHeuristic = FixationType.proposedMove;
        fixationDone = true;
      }
    }
    if (r >= 0.3333 && r < 0.6667) { // try random item fixation
      fixationDone = randomItemHeuristic ();
    }
    if (!fixationDone) { // else try random place/global strategy
      if (model.isExperienced ()) {
        // TODO: include global strategy
        randomPlaceHeuristic ();
      } else {
        randomPlaceHeuristic ();
      }
    }
    // randomPlace / globalStrategy guaranteed to succeed

    addFixation (new Fixation (lastHeuristic, fixationX, fixationY));
  }

  /**
   * Find the next fixation point using one of the available 
   * heuristics, and then learn from the new pattern.
   */
  public void moveEyeAndLearn () {
    boolean fixationDone = false;
    if (doingInitialFixations ()) {
      fixationDone = doInitialFixation ();
    }
    if (!fixationDone) {
      fixationDone = ltmHeuristic ();
    }
    if (!fixationDone) {
      moveEyeUsingHeuristics ();
    }
    // learn pattern found from fixations
    if (shouldLearnFixations ()) {
      learnFixatedPattern ();
    }

    // simplified version of learning, learns pattern at current point
//    model.recogniseAndLearn (model.getDomainSpecifics().normalise (currentScene.getItems (fixationX, fixationY, 2)));

    // NB: template construction is only assumed to occur after training, so 
    // template completion code is not included here
  }

  /**
   * Find the next fixation point using one of the available 
   * heuristics, and simply move the eye to that point.
   */
  public void moveEye () {
    Node node = model.getVisualLtm ();
    boolean fixationDone = false;
    if (doingInitialFixations ()) {
      fixationDone = doInitialFixation ();
      if (fixationDone) {
        node = model.recognise (model.getDomainSpecifics().normalise (currentScene.getItems (fixationX, fixationY, 2)));
      }
    }
    if (!fixationDone) {
      fixationDone = ltmHeuristic ();
      if (fixationDone && model.getVisualStm().getCount () >= 1) {
        node = model.getVisualStm().getItem(0);
      }
    }
    if (!fixationDone) {
      moveEyeUsingHeuristics ();
      node = model.recognise (model.getDomainSpecifics().normalise (currentScene.getItems (fixationX, fixationY, 2)));
    }
    recognisedNodes.add (node);
    // Attempt to fill out the slots on the top-node of visual STM with the currently 
    // fixated items
    if (model.getVisualStm().getCount () >= 1) {
      model.getVisualStm().getItem(0).fillSlots (currentScene.getItems (fixationX, fixationY, 2));
    }
  }

  List<Fixation> fixations = new ArrayList<> ();
  private int fixationsLearnFrom = 0; // used to mark first fixation to learn from
  private int targetNumberFixations = 20; // used to store the number of fixations in a scene

  /**
   * Clears the stored fixations.
   */
  public void clearFixations () {
    fixations.clear ();
    fixationsLearnFrom = 0;
  }

  /**
   * Returns the stored fixations.
   * @return list of stored fixations
   */
  public List<Fixation> getFixations () {
    return fixations;
  }

  /**
   * Returns the number of stored fixations.
   * @return the number of stored fixations.
   */
  public int getNumberFixations () {
    return fixations.size ();
  }

  public int getFixationsX (int index) {
    assert (index < fixations.size () && index >= 0);
    return fixations.get(index).getX ();
  }

  public int getFixationsY (int index) {
    assert (index < fixations.size () && index >= 0);
    return fixations.get(index).getY ();
  }

  private void addFixation (Fixation fixation) {
    fixations.add (fixation);
  }

  // learn pattern found from fixations
  // -- in CHREST 2 this is triggered by:
  //    a. last fixation an empty square
  //    b. last fixation a result of random item or global strategy
  //    c. reached max number of fixations
  //    d. cycle in fixations
  private boolean shouldLearnFixations () {
    int lastFixationIndex = fixations.size () - 1;
    if (lastFixationIndex <= fixationsLearnFrom) { // nothing to learn
      return false;
    }
    Fixation lastFixation = fixations.get (lastFixationIndex);
    // is last fixation to an empty square?
    if (currentScene.isEmpty (lastFixation.getX (), lastFixation.getY ())) {
      return true;
    }
    // is fixation a global strategy?
    if (lastFixation.getType().equals (FixationType.global)) {
     return true;
    }
    // is fixation a random item?
    if (lastFixation.getType().equals (FixationType.randomItem)) {
      return true;
    }
    // reached limit of fixations?
    if (lastFixationIndex == targetNumberFixations) {
      return true;
    }
    // does last fixation form a cycle?
    for (int i = 0; i < lastFixationIndex; ++i) {
      if (lastFixation.getX () == fixations.get(i).getX () &&
          lastFixation.getY () == fixations.get(i).getY ()) {
        return true;
      }
    }
    // otherwise, nothing to learn
    return false;
  }

  private void learnFixatedPattern () {
    ListPattern fixatedPattern = new ListPattern (Modality.VISUAL);
    for (int i = fixationsLearnFrom; i < fixations.size () - 1; ++i) {
      if (!currentScene.isEmpty (fixations.get(i).getX (), fixations.get(i).getY ())) {
        fixatedPattern.add (new ItemSquarePattern (
              currentScene.getItem (fixations.get(i).getX (), fixations.get(i).getY ()),
              fixations.get(i).getY () + 1,
              fixations.get(i).getX () + 1
              ));
      }
    }
    model.recogniseAndLearn (model.getDomainSpecifics().normalise (fixatedPattern.append(currentScene.getItems(fixationX, fixationY, 2))));
    // begin cycle again, from point where we stopped
    fixationsLearnFrom = fixations.size () - 1;
  }

  // indicator for if the eye is processing the initial fixations
  // arbitrarily set to '4', from de Groot and Gobet (1996)
  private boolean doingInitialFixations () {
    return fixations.size () <= 4;
  }

  public List<Node> getRecognisedNodes () {
    return recognisedNodes;
  }
}

