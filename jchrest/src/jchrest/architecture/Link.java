package jchrest.architecture;

import jchrest.lib.ListPattern;

/**
 * Represents a test link within the model's long-term memory.
 * The link has a test, which must be passed when sorting a pattern 
 * through to the child node.
 */
public class Link {

  /**
   * Constructor sets the link's test and child node.
   * 
   * @param test pattern
   * @param child node
   */
  public Link (ListPattern test, Node child) {
    this.test = test;
    this.child = child;
  }

  /**
   * Accessor to the link's child node.
   * @return child node
   */
  public Node getChildNode () {
    return child;
  }

  /**
   * Accessor to the link's test.
   * @return pattern used in link test.
   */
  public ListPattern getTest () {
    return test;
  }

  /**
   * Test if the given pattern can be sorted through this test link.
   * A test passes if the test matches the given pattern.
   *
   * @param pattern to test to see if it passes
   * @return result of test
   */
  public boolean passes (ListPattern pattern) {
    return test.matches (pattern);
  }

  // private fields
  private final ListPattern test;
  private final Node child;
}

