package jchrest.architecture;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Class manages the short-term memory for one modality of a Chrest model.
 * Each short-term memory has a maximum capacity, and stores a list of nodes.
 */
public class Stm implements Iterable<Node> {
  private int size;
  private List<Node> items;

  /**
   * Constructor requires the maximum capacity to be set.
   *
   * @param size the maximum capacity for this short-term memory
   */
  public Stm (int size) {
    this.size = size;
    items = new ArrayList<Node> ();
  }

  /**
   * Accessor for the maximum capacity.
   *
   * @return the size of this short-term memory, its maximum capacity
   */
  public int getSize () {
    return size;
  }

  /**
   * Alter the maximum capacity.  When the size is changed, all items in the 
   * short-term memory are cleared.
   *
   * @param size the new maximum capacity for this short-term memory
   */
  public void setSize (int size) {
    this.size = size;
    items.clear ();
  }

  /**
   * Return a count of how many items are actually in the short-term memory.
   *
   * @return count of items actually present in this short-term memory
   */
  public int getCount () {
    return items.size ();
  }

  /**
   * Retrieve a node within the short-term memory by its index position.
   * There is no error checking on the retrieval.
   *
   * @param index of node in short-term memory to retrieve
   * @return node in this short-term memory at given index
   */
  public Node getItem (int index) {
    return items.get (index);
  }

  /**
   * When adding a new node to STM, the new node is added to the top of STM 
   * with the queue cut at the bottom to keep STM to the fixed size constraints.
   * However, the most informative node is maintained in the list, by re-adding 
   * it to STM, if lost.
   *
   * @param node to add to this short-term memory
   */
  public void add (Node node) {
    // find the most informative node which also matches this node's contents
    Node hypothesis = node;
    for (Node check : items) {
      if (hypothesis.getContents ().matches (check.getContents ()) &&
          check.information () > hypothesis.information ()) {
        hypothesis = check;
      }
    }
    // put this node at the front of STM, and remove any duplicate
    items.remove (node);
    items.add (0, node);

    // truncate STM to be of at most size elements
    while (items.size () > size) {
      items.remove (items.size () - 1);
    }
    // if most informative node not in STM, then add it back in to top
    if (!items.contains (hypothesis)) {
      items.remove (items.size () - 1); // losing bottom item
      items.add (0, hypothesis);
    }
  }

  /**
   * Replace the topmost (hypothesis) node with the given one.
*
   * @param node to use as new hypothesis
*/
  public void replaceHypothesis (Node node) {
    if (items.size () > 0) {
      items.remove (0);
    }
    // make sure there are no duplicates
    if (items.contains (node)) {
      items.remove (node);
    }
    // add to top of STM
    items.add (0, node);
  }

  /**
   * Remove all items from STM.
   */
  public void clear () {
    items.clear ();
  }

  /**
   * Add a lateral link indicating that the second node in this STM 
   * is associated with the top node.  The link is only added if not already 
   * present, and the model's clock is advanced by the time to add a link.
   * Returns boolean to indicate if learning occurred or not.
   *
   * @param model for this short-term memory, to update its clock
   * @return true if a lateral link was learned
   */
  public boolean learnLateralLinks (Chrest model) {
    if (items.size () >= 2 && 
        items.get(1).getAssociatedNode () != items.get(0)) {
      items.get(1).setAssociatedNode (items.get(0));
      model.advanceClock (model.getAddLinkTime ());
      return true;
    } else {
      return false;
    }
  }

  /* 
   * Support iteration over the nodes in STM.
   */

  /**
   * Create iterator over items in short-term memory.
   * @return iterator over items in short-term memory
   */
  public Iterator<Node> iterator () {
    return new StmIterator (items);
  }
  
  class StmIterator implements Iterator<Node> {
    private int index = 0;
    private List<Node> items;

    StmIterator (List<Node> items) {
      this.items = items;
    }

    public boolean hasNext () {
      return index < items.size ();
    }

    public Node next () {
      if (hasNext ()) {
        index += 1;
        return items.get(index-1);
      }
      throw new java.util.NoSuchElementException();
    }

    public void remove () {
      throw new UnsupportedOperationException ();
    }
  }
}

