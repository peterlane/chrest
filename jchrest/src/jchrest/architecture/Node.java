package jchrest.architecture;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jchrest.lib.FileUtilities;
import jchrest.lib.ItemSquarePattern;
import jchrest.lib.ListPattern;
import jchrest.lib.ParsingErrorException;
import jchrest.lib.Pattern;
import jchrest.lib.PrimitivePattern;

/**
 * Represents a node within the model's long-term memory discrimination network.
 * Methods support learning and also display.
 *
 * There are various links supported between nodes:
 *
 * <ul>
 * <li><em>action links</em>: one node can be linked with many actions. 
 *     Action links correspond to movements or outputs that the model can take based 
 *     on the information in this node.</li>
 * <li><em>associated node</em>: a single node that this node is 'associated' with. For example, 
 *     this node can be used to represent sequences between nodes in verbal learning
 *     applications.</li>
 * <li><em>child links</em>: are test links, representing the usual hierarchy within LTM.</li>
 * <li><em>named by node</em>: A single node that 'names' this node. The idea is for a visual 
 *     node to be 'named by' a verbal node, for example, so a visual pattern can be 
 *     associated with a word name.</li>
 * <li><em>semantic links</em>: one node can have many semantic links. 
 *     Semantic links are typically formed when two nodes are deemed sufficiently "similar".
 *     </li>
 * </ul>
 */
public class Node {

  /**
   * Constructor to construct a new root node for the model.  
   *
   * @param model which this node is part of
   * @param reference unique id for this node
   * @param type a list pattern whose modality will set the type for this node 
   */
  public Node (Chrest model, int reference, ListPattern type) {
    this (model, reference, type, type);
  }

  /**
   * When constructing non-root nodes in the network, the new contents and image 
   * must be defined.  Assume that the image always starts empty.
   *
   * @param model which this node is part of
   * @param contents for this node
   * @param image for this node
   */
  public Node (Chrest model, ListPattern contents, ListPattern image) {
    this (model, model.getNextNodeNumber (), contents, image);
  }

  /**
   * Constructor to build a new Chrest node with given reference, contents and image.
   *
   * @param model which this node is part of
   * @param reference unique id for this node
   * @param contents for this node
   * @param image for this node
   */
  public Node (Chrest model, int reference, ListPattern contents, ListPattern image) {
    this.model = model;
    this.reference = reference;
    this.contents = contents.clone ();
    this.image = image;
    this.children = new ArrayList<Link> ();
    this.semanticLinks = new ArrayList<Node> ();
    this.associatedNode = null;
    this.namedBy = null;
    this.actionLinks = new ArrayList<Node> ();
  }

  /**
   * When the model is reset, all listeners of individual nodes must be closed.
   * This method notifies listeners to close themselves, and then 
   * requests child nodes to do the same.
   */
  void clear () {
    pcs.firePropertyChange ("close", null, null);
    for (Link child : children) {
      child.getChildNode().clear ();
    }
  }

  /**
   * Accessor to reference number of node.
   * @return unique reference for this node
   */
  public int getReference () {
    return reference;
  }

  /**
   * Accessor to contents of node.
   * @return the contents pattern of this node
   */
  public ListPattern getContents () {
    return contents;
  }

  /**
   * Accessor to image of node.
   * @return the image pattern of this node
   */
  public ListPattern getImage () {
    return image;
  }

  /**
   * Change the node's image to given value.  
   * Also notifies any listeners.
   *
   * @param image for this node
   */
  public void setImage (ListPattern image) {
    this.image = image;
    pcs.firePropertyChange ("setImage", null, null);
  }

  /**
   * Accessor to children of node.
   * @return a list of the child links from this node
   */
  public List<Link> getChildren () {
    return children;
  }

  /**
   * Add a new test link with given test pattern and child node.
   *
   * @param test pattern for link
   * @param child node for link
   */
  void addTestLink (ListPattern test, Node child) {
    children.add (0, new Link (test, child));
    pcs.firePropertyChange ("addTestLink", null, null);
  }

  /**
   * Make a semantic link between this node and given node.  
   * Does not add duplicates.
   *
   * @param node to make a link to
   */
  void addSemanticLink (Node node) {
    if (!semanticLinks.contains (node)) {
      semanticLinks.add (node);
      pcs.firePropertyChange ("addSemanticLink", null, null);
    }
  }

  /**
   * Accessor to list of semantic links.
   * @return a list of nodes that are linked to this node by semantic links
   */
  public List<Node> getSemanticLinks () {
    return semanticLinks;
  }

  /**
   * Accessor to node that is associated with this node.
   * @return the node that this node is associated with
   */
  public Node getAssociatedNode () {
    return associatedNode;
  }

  /**
   * Modify node that is associated with this node.
   * @param node for this node to be associated with
   */
  public void setAssociatedNode (Node node) {
    associatedNode = node;
    pcs.firePropertyChange ("setAssociatedNode", null, null);
  }

  /**
   * Accessor to node that names this node.
   * (This link is typically created with a verbal node.)
   *
   * @return the node that names this node
   */
  public Node getNamedBy () {
    return namedBy;
  }

  /**
   * Modify node that names this node.
   *
   * @param node for this node to be named by
   */
  public void setNamedBy (Node node) {
    namedBy = node;
    pcs.firePropertyChange ("setNamedBy", null, null);
  }

  /**
   * Add a node to the list of action links for this node.
   * Do not add the node if already present.
   *
   * @param node to add 
   */
  public void addActionLink (Node node) {
    if (actionLinks.contains (node)) { 
      ;
    } else {
      actionLinks.add (node);
    }
  }

  /**
   * Retrieve list of nodes linked to as actions.
   *
   * @return list of nodes linked to as actions
   */
  public List<Node> getActionLinks () {
    return actionLinks;
  }

  /** 
   * Compute the size of the network below the current node, 
   * following test links only.
   *
   * @return size of network below current node
   */
  public int size () {
    int count = 1; // for self
    for (Link link : children) {
      count += link.getChildNode().size ();
    }

    return count;
  }

  /**
   * Compute the amount of information in current node.  
   * Information is based on the size of the image + the number of slots.
   *
   * @return amount of information in this node
   */
  public int information () {
    if (reference == 0) return 0; // root node has 0 information
    int information = image.size ();
    if (itemSlots != null) {
      information += itemSlots.size ();
    }
    if (positionSlots != null) {
      information += positionSlots.size ();
    }

    return information;
  }

  /**
   * Add to a map of content sizes to node counts for this node and its children.
   *
   * @param size is a map to add new count to
   */
  protected void getContentCounts (Map<Integer, Integer> size) {
    int csize = contents.size ();
    if (size.containsKey (csize)) {
      size.put (csize, size.get(csize) + 1);
    } else {
      size.put (csize, 1);
    }

    for (Link child : children) {
      child.getChildNode().getContentCounts (size);
    }
  }

  /**
   * Add to a map of image sizes to node counts for this node and its children.
   *
   * @param size is a map to add new count to
   */
  protected void getImageCounts (Map<Integer, Integer> size) {
    int csize = image.size ();
    if (size.containsKey (csize)) {
      size.put (csize, size.get(csize) + 1);
    } else {
      size.put (csize, 1);
    }

    for (Link child : children) {
      child.getChildNode().getImageCounts (size);
    }
  }

  /**
   * Add to a map from number of semantic links to frequency, for this node and its children.
   *
   * @param size is a map to add new count to
   */
  protected void getSemanticLinkCounts (Map<Integer, Integer> size) {
    int csize = semanticLinks.size ();
    if (csize > 0) { // do not count nodes with no semantic links
      if (size.containsKey (csize)) {
        size.put (csize, size.get(csize) + 1);
      } else {
        size.put (csize, 1);
      }
    }

    for (Link child : children) {
      child.getChildNode().getSemanticLinkCounts (size);
    }
  }

  // private fields
  private final Chrest model;
  private final int reference;
  private final ListPattern contents;
  private ListPattern image;
  private List<Link> children;
  private List<Node> semanticLinks;
  private Node associatedNode;
  private Node namedBy;
  private List<Node> actionLinks;

  /**
   * Compute the total size of images below the current node.
   */
  private int totalImageSize () {
    int size = image.size ();
    for (Link link : children) {
      size += link.getChildNode().totalImageSize ();
    }

    return size;
  }

  /**
   * If this node is a child node, then add its depth to depths.  
   * Otherwise, continue searching through children for the depth.
   */
  private void findDepth (int currentDepth, List<Integer> depths) {
    if (children.isEmpty ()) {
      depths.add (currentDepth);
    } else {
      for (Link link : children) {
        link.getChildNode().findDepth (currentDepth + 1, depths);
      }
    }
  }

  /**
   * Compute the average depth of nodes below this point,
   * considering only nodes reachable using test links.
   *
   * @return average depth of nodes below this node
   */
  public double averageDepth () {
    List<Integer> depths = new ArrayList<Integer> ();
    // -- find every depth
    for (Link link : children) {
      link.getChildNode().findDepth(1, depths);
    }

    // -- compute the average of the depths
    int sum = 0;
    for (Integer depth : depths) {
      sum += depth;
    }
    if (depths.isEmpty ()) {
      return 0.0;
    } else {
      return (double)sum / (double)depths.size ();
    }
  }

  /**
   * Compute the average size of the images in nodes below this point,
   * considering only nodes reachable through test links.
   *
   * @return average size of images in nodes below this node
   */
  public double averageImageSize () {
    return (double)totalImageSize() / size();
  }

  /**
   * Count templates in part of network rooted at this node, 
   * considering only nodes reachable through test links.
   *
   * @return count of templates in nodes at or below this node
   */
  public int countTemplates () {
    int count = 0;
    if (isTemplate ()) count += 1;

    for (Link link : children) {
      count += link.getChildNode().countTemplates ();
    }

    return count;
  }

  private List<ItemSquarePattern> itemSlots;
  private List<ItemSquarePattern> positionSlots;
  private List<ItemSquarePattern> filledItemSlots;
  private List<ItemSquarePattern> filledPositionSlots;

  /**
   * Return list of filled item slots. 
   * Each slot can hold a piece seen in the current scene.
   *
   * @return list of filled item slots
   */
  public List<ItemSquarePattern> getFilledItemSlots () {
    return filledItemSlots;
  }

  /**
   * Return list of filled position slots. 
   * Each slot can hold a piece seen in the current scene.
   *
   * @return list of filled position slots
   */
  public List<ItemSquarePattern> getFilledPositionSlots () {
    return filledPositionSlots;
  }

  /**
   * Returns true if this node is a template.  To be a template, the node 
   * must have at least one slot of any kind.
   *
   * @return true if this node is a template
   */
  public boolean isTemplate () {
    if (itemSlots == null || positionSlots == null) {
      return false;
    }

    // is a template if there is at least one slot
    if (itemSlots.size () > 0) return true;
    if (positionSlots.size () > 0) return true;

    return false;
  }

  /**
   * Clear out the template slots.
   */
  public void clearTemplate () {
    if (itemSlots != null) itemSlots.clear ();
    if (positionSlots != null) positionSlots.clear ();
  }

  /**
   * Attempt to fill some of the slots using the items in the given pattern.
   *
   * @param pattern for which the slots are checked
   */
  public void fillSlots (ListPattern pattern) {
    // create arraylists only when required, as most nodes do not need to 
    // waste the storage space.
    if (itemSlots == null) {
      itemSlots = new ArrayList<ItemSquarePattern> ();
    }
    if (positionSlots == null) {
      positionSlots = new ArrayList<ItemSquarePattern> ();
    }
    if (filledItemSlots == null) {
      filledItemSlots = new ArrayList<ItemSquarePattern> ();
    }
    if (filledPositionSlots == null) {
      filledPositionSlots = new ArrayList<ItemSquarePattern> ();
    }
    for (int index = 0; index < pattern.size (); index++) {
      boolean slotFilled = false;
      if (pattern.getItem(index) instanceof ItemSquarePattern) {
        ItemSquarePattern item = (ItemSquarePattern)(pattern.getItem (index));
        // only try to fill a slot if item is not already in image or slot
        if (!image.contains (item) && 
            !filledItemSlots.contains (item) && 
            !filledPositionSlots.contains (item)) { 
          // 1. check the item slots
          for (ItemSquarePattern slot : itemSlots) {
            if (!slotFilled) {
              if (slot.getItem().equals(item.getItem ())) {
                filledItemSlots.add (item);
                slotFilled = true;
              }
            }
          }

          // 2. check the position slots
          for (ItemSquarePattern slot : positionSlots) {
            if (!slotFilled) {
              if (slot.getRow () == item.getRow () &&
                  slot.getColumn () == item.getColumn ()) {
                filledPositionSlots.add (item);
                slotFilled = true;
                  }
            }
          }
            }
      }
    }
  }

  /**
   * Clear the filled slots.
   */
  public void clearFilledSlots () {
    if (filledItemSlots == null) filledItemSlots = new ArrayList<ItemSquarePattern> ();
    if (filledPositionSlots == null) filledPositionSlots = new ArrayList<ItemSquarePattern> ();

    filledItemSlots.clear ();
    filledPositionSlots.clear ();
  }

  /**
   * Retrieve all primitive items stored in slots of template as a ListPattern.
   * The retrieved pattern may contain duplicate primitive items, but will be 
   * untangled in Chrest#scanScene.
   *
   * @return pattern for filled slots
   */
  ListPattern getFilledSlots () {
    ListPattern filledSlots = new ListPattern ();
    for (ItemSquarePattern filledSlot : filledItemSlots) {
      filledSlots.add (filledSlot);
    }
    for (ItemSquarePattern filledSlot : filledPositionSlots) {
      filledSlots.add (filledSlot);
    }
    return filledSlots;
  }

  /**
   * Converts this node into a template, if appropriate, and repeats for 
   * all child nodes.
   * Note: usually, this process is done as a whole at the end of training, but 
   * can also be done on a node-by-node basis, during training.
   */
  public void constructTemplates () {
    itemSlots = new ArrayList<ItemSquarePattern> ();
    positionSlots = new ArrayList<ItemSquarePattern> ();

    if (canFormTemplate ()) {
      // gather images of current node, test links and similar nodes together, 
      // removing the contents from them
      List<ListPattern> patterns = new ArrayList<ListPattern> ();
      patterns.add (image.remove (contents));
      for (Link link : children) {
        patterns.add (link.getChildNode().getImage().remove (contents));
      }
      for (Node node : semanticLinks) {
        patterns.add (node.getImage().remove (contents));
      }
      // create a hashmap of counts of occurrences of items and of squares
      Map<String,Integer> countItems = new HashMap<String,Integer> ();
      Map<Integer,Integer> countPositions = new HashMap<Integer,Integer> ();
      for (ListPattern pattern : patterns) {
        for (PrimitivePattern pattern_item : pattern) {
          if (pattern_item instanceof ItemSquarePattern) {
            ItemSquarePattern item = (ItemSquarePattern)pattern_item;
            if (countItems.containsKey (item.getItem ())) {
              countItems.put (item.getItem (), countItems.get(item.getItem ()) + 1);
            } else {
              countItems.put (item.getItem (), 1);
            }
            // TODO: Check construction of 'posn_key', try 1000 = scene.getWidth ?
            Integer posn_key = item.getRow () + 1000 * item.getColumn ();
            if (countPositions.containsKey (posn_key)) {
              countPositions.put (posn_key, countPositions.get(posn_key) + 1);
            } else {
              countPositions.put (posn_key, 1);
            }
          }
        }
      }

      // make slots
      // 1. from items which repeat more than minimumNumberOccurrences
      for (String itemKey : countItems.keySet ()) {
        if (countItems.get(itemKey) >= model.getMinTemplateOccurrences ()) {
          itemSlots.add (new ItemSquarePattern (itemKey, -1, -1));
        }
      }
      // 2. from locations which repeat more than minimumNumberOccurrences
      for (Integer posnKey : countPositions.keySet ()) {
        if (countPositions.get(posnKey) >= model.getMinTemplateOccurrences ()) {
          positionSlots.add (new ItemSquarePattern ("slot", posnKey / 1000, posnKey - (1000 * (posnKey/1000))));
        }
      }
    }

    // continue conversion for children of this node
    for (Link link : children) {
      link.getChildNode().constructTemplates ();
    }

  }

  /** Return true if template conditions are met:
   * 1. contents size &gt; model.getMinTemplateLevel ()
   * then:
   * 2. gather together current node image and images of all nodes 
   * linked by the test and semantic links
   *    remove the contents of current node from those images
   *    see if any piece or square repeats more than once
   *
   * @return true if this node meets the conditions to form a template
   */
  public boolean canFormTemplate () {
    // return false if node is too shallow in network
    if (contents.size () <= model.getMinTemplateLevel ()) return false;
    // gather images of current node and test links together, removing the contents from them
    List<ListPattern> patterns = new ArrayList<ListPattern> ();
    patterns.add (image.remove (contents));
    for (Link link : children) {
      patterns.add (link.getChildNode().getImage().remove (contents));
    }
    for (Node node : semanticLinks) {
      patterns.add (node.getImage().remove (contents));
    }
    // create a hashmap of counts of occurrences of items and of squares
    Map<String,Integer> countItems = new HashMap<String,Integer> ();
    Map<Integer,Integer> countPositions = new HashMap<Integer,Integer> ();
    for (ListPattern pattern : patterns) {
      for (PrimitivePattern pattern_item : pattern) {
        if (pattern_item instanceof ItemSquarePattern) {
          ItemSquarePattern item = (ItemSquarePattern)pattern_item;
          if (countItems.containsKey (item.getItem ())) {
            countItems.put (item.getItem (), countItems.get(item.getItem ()) + 1);
          } else {
            countItems.put (item.getItem (), 1);
          }
          Integer posn_key = item.getRow () + 1000 * item.getColumn ();
          if (countPositions.containsKey (posn_key)) {
            countPositions.put (posn_key, countPositions.get(posn_key) + 1);
          } else {
            countPositions.put (posn_key, 1);
          }
        }
      }
    }

    // make slots
    // 1. from items which repeat more than minimumNumberOccurrences
    for (String itemKey : countItems.keySet ()) {
      if (countItems.get(itemKey) >= model.getMinTemplateOccurrences ()) {
        return true;
      }
    }
    // 2. from locations which repeat more than minimumNumberOccurrences
    for (Integer posnKey : countPositions.keySet ()) {
      if (countPositions.get(posnKey) >= model.getMinTemplateOccurrences ()) {
        return true;
      }
    }
    return false;
  }

  /**
   * LearnPrimitive is used to construct a test link and node containing 
   * precisely the given pattern.  It is assumed the given pattern contains 
   * a single primitive item, and is finished.
   *
   * @param pattern to learn
   * @return node learnt from pattern
   */
  public Node learnPrimitive (ListPattern pattern) {
    assert (pattern.isFinished () && pattern.size () == 1);
    ListPattern contents = pattern.clone ();
    contents.setNotFinished ();
    Node child = new Node (model, contents, new ListPattern (pattern.getModality ()));
    addTestLink (contents, child);
    model.advanceClock (model.getDiscriminationTime ());

    return child;
  }

  /**
   * addTest is used to construct a test link using the given pattern, 
   * with a new empty child node.  It is assumed the given pattern is 
   * non-empty and constitutes a valid, new test for the current Node.
   */
  private Node addTest (ListPattern pattern) {
    // ignore if already a test
    for (Link child : children) {
      if (child.getTest().equals (pattern)) {
        return this;
      }
    }

    Node child;
    if (reference == 0) { // parent is root node
      child = new Node (model, pattern, pattern);
    } else { // new node is an internal node
      ListPattern newContents = model.getDomainSpecifics().normalise (contents.append(pattern));
      // NB: there have been three schools of thought here for the new image!
      // 1. Use contents of parent node
      //    -- follows rec-fam-red-clock-3.lisp
      child = new Node (model, newContents, contents.clone ());
      // 2. Use contents of new node (less any end marker)
      // ncp = new_contents.clone
      // ncp.delete('$')
      // child = new Node (model, newContents, newContents.clone().setNotFinished ());
      // 3. An empty image (used in lisp version 3, ++)
      // child = new Node (model, newContents, new ListPattern (newContents.getModality ()));
    }
    addTestLink (pattern, child);
    model.advanceClock (model.getDiscriminationTime ());
    return child;
  }

  /**
   * extendImage is used to add new information to the node's image.
   * It is assumed the given pattern is non-empty and is a valid extension.
   */
  private Node extendImage (ListPattern newInformation) {
    setImage (model.getDomainSpecifics().normalise (image.append (newInformation)));
    model.advanceClock (model.getFamiliarisationTime ());

    return this;
  }

  /**
   * Discrimination learning extends the LTM network by adding new 
   * nodes.
   * Note: in CHREST 2 tests are pointers to nodes.  This can be 
   * implemented using a Link interface, and having a LinkNode class, 
   * so that checking if test passed is done through the interface.
   * This may be needed later for semantic/template learning.
   */
  Node discriminate (ListPattern pattern) {
    ListPattern newInformation = pattern.remove (contents);

    // cases 1 & 2 if newInformation is empty
    if (newInformation.isEmpty ()) {
      // change for conformance
      newInformation.setFinished ();
      // 1. is < $ > known?
      if (model.recognise (newInformation).getContents ().equals (newInformation) ) {
        // 2. if so, use as test
        return addTest (newInformation);
      } else {
        // 3. if not, then learn it, within the appropriate input modality
        Node child = new Node (model, newInformation, newInformation);
        model.getLtmByModality(pattern).addTestLink (newInformation, child);
        return child;
      }
    }

    Node retrievedChunk = model.recognise (newInformation);
    if (retrievedChunk == model.getLtmByModality (pattern)) {
      // 4. if root node is retrieved, then the primitive must be learnt
      return model.getLtmByModality(newInformation).learnPrimitive (newInformation.getFirstItem ());
    } else if (retrievedChunk.getContents().matches (newInformation)) {
      // 5. retrieved chunk can be used as a test
      ListPattern testPattern = retrievedChunk.getContents().clone ();
      return addTest (testPattern);
    } else { 
      // 6. mismatch, so use only the first item for test
      // NB: first-item must be in network as retrievedChunk was not the root node
      ListPattern firstItem = newInformation.getFirstItem ();
      firstItem.setNotFinished ();
      return addTest (firstItem);
    }
  }

  /**
   * Familiarisation learning extends the image in a node by adding new 
   * information from the given pattern.
   */
  Node familiarise (ListPattern pattern) {
    ListPattern newInformation = pattern.remove (image).getFirstItem ();
    newInformation.setNotFinished ();
    // EXIT if nothing to learn
    if (newInformation.isEmpty ()) { 
      return this;
    }
    //
    // Note: CHREST 2 had the idea of not familiarising if image size exceeds 
    // the max of 5 and 2*contents-size.  This avoids overly large images.
    // This idea is not implemented here.
    //
    Node retrievedChunk = model.recognise (newInformation);
    if (retrievedChunk == model.getLtmByModality (pattern)) {
      // primitive not known, so learn it
      return model.getLtmByModality(newInformation).learnPrimitive (newInformation);
    } else {
      // extend image with new item
      return extendImage (newInformation);
    }
  }

  /**
   * Search this node's semantic links for a more informative node, and return one if 
   * found.
   *
   * @param maximumSemanticDistance limits the breadth of search
   * @return most informative node within the semantic links searched
   */
  public Node searchSemanticLinks (int maximumSemanticDistance) {
    if (maximumSemanticDistance <= 0) return this; // reached limit of search
    Node bestNode = this;
    for (Node compare : semanticLinks) {
      Node bestChild = compare.searchSemanticLinks (maximumSemanticDistance - 1);
      if (bestChild.information () > bestNode.information ()) {
        bestNode = bestChild;
      }
    }

    return bestNode;
  }

  /**
   * Write node data to writer in dot format.
   *
   * @param writer to write information to
   * @throws IOException in case of write error
   */
  public void writeAsDot (Writer writer) throws IOException {
    String name = "  ";

    if (reference == 0) {
      var mode = "";
      switch (contents.getModality ()) {
        case VISUAL: mode = "vis"; break;
        case VERBAL: mode = "ver"; break;
        case ACTION: mode = "act"; break;
      }
      name += mode;
    } else {
      name += "n" + reference;
    }

    if (reference == 0) { // display modal root node differently
      writer.write (name + "  [shape=box,label=\"" + contents.getModality() + "\"]\n");
    } else {
      writer.write (name + " [label=\"" + image.toString() + "\"]\n");
    }

    for (Link link : children) {
      writer.write (name + " -> n" + link.getChildNode().getReference() + 
          " [label=\"" + link.getTest().toString() + "\"]\n");
      link.getChildNode().writeAsDot (writer);
    }
    if (associatedNode != null) {
      writer.write(name + " -> n" + associatedNode.getReference() + " [label=\"A\",color=cornflowerblue,style=dashed]\n");
    }
    if (namedBy != null) {
      writer.write(name + " -> n" + namedBy.getReference() + " [label=\"N\",color=darkgreen,style=dashed]\n");
    }
    for (Node node: semanticLinks) {
      if (reference < node.getReference()) { // only draw these links in one direction
        writer.write(name + " -> n" + node.getReference() + " [dir=none,label=\"S\",color=brown,style=dashed]\n");
      }
    }
    for (Node node: actionLinks) {
      writer.write(name + " -> n" + associatedNode.getReference() + " [label=\"N\",color=red,style=dashed]\n");
    }
  }

  /**
   * Write node information in VNA format.
   *
   * @param writer to write information to
   * @throws IOException in case of write error
   */
  public void writeNodeAsVna (Writer writer) throws IOException {
    writer.write ("" + reference + " \"" + contents.toString() + "\"\n");
    for (Link link : children) {
      link.getChildNode().writeNodeAsVna (writer);
    }
  }

  /**
   * Write test links in VNA format.
   *
   * @param writer to write information to
   * @throws IOException in case of write error
   */
  public void writeLinksAsVna (Writer writer) throws IOException {
    // write my links
    for (Link link : children) {
      writer.write ("" + reference + " " + link.getChildNode().getReference () + "\n");
    }
    // repeat for children
    for (Link link : children) {
      link.getChildNode().writeLinksAsVna (writer);
    }
  }

  /**
   * Write semantic links in VNA format.
   *
   * @param writer to write information to
   * @throws IOException in case of write error
   */
  public void writeSemanticLinksAsVna (Writer writer) throws IOException {
    // write my links
    for (Node node : semanticLinks) {
      writer.write ("" + reference + " " + node.getReference () + "\n");
    }
    // repeat for children
    for (Link link : children) {
      link.getChildNode().writeSemanticLinksAsVna (writer);
    }
  }

  // support for property change listeners
  private final PropertyChangeSupport pcs = new PropertyChangeSupport (this);

  /**
   * Add an object to respond to any changes made to this Chrest instance. 
   * Typically used to update GUI views.
   *
   * @param listener object
   */
  public void addPropertyChangeListener (PropertyChangeListener listener) {
    pcs.addPropertyChangeListener (listener);
  }

  /**
   * Remove an object previously set to respond to any changes made to this 
   * Chrest instance. Typically used to update GUI views.
   *
   * @param listener object
   */
  public void removePropertyChangeListener (PropertyChangeListener listener) {
    pcs.removePropertyChangeListener (listener);
  }
}

