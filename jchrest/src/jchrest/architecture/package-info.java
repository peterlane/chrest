/**
 * Provides the core Chrest architecture.
 * The main class is the Chrest class itself, which has four main components:
 * <ol>
 * <li>Long-term memory: a discrimination-net structure, defined by the Node and Link classes</li>
 * <li>Short-term memory: a limited queue of pointers into long-term memory, defined by the Stm class</li>
 * <li>Perceiver: a set of heuristics for scanning a given Scene, defined by the Perceiver class</li>
 * <li>Emotions: supporting emotional responses within the architecture, Emotion, EmotionalTrace, EmotionAssociator
 * </ol>
*/
package jchrest.architecture;

