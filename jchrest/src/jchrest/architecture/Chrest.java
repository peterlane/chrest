package jchrest.architecture;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jchrest.lib.DomainSpecifics;
import jchrest.lib.GenericDomain;
import jchrest.lib.ItemSquarePattern;
import jchrest.lib.ListPattern;
import jchrest.lib.Move;
import jchrest.lib.Pattern;
import jchrest.lib.PrimitivePattern;
import jchrest.lib.Scene;

/**
 * An instance of Chrest defines a complete Chrest model.
 * The model consists of short-term and long-term memories for 
 * visual, verbal and action inputs. An internal clock keeps 
 * track of the model-specific time, and timing parameters 
 * affect the period of operation of most processes within the 
 * model. The model has a perceiver module, for scanning scenes,
 * and an emotion module, for modelling the effect of emotions.
 * <p>
 * Long-term memory consists of nodes, linked with test links.
 * In addition, nodes may be linked with associated, semantic, 
 * naming and action links. For more on these, see documentation 
 * on Nodes.
 * <p>
 * If enabled, certain conditions enable nodes to be converted into 
 * <em>templates</em>, which improve the model's ability to recall 
 * new scenes through a core+slots representation.
 *
 * @see Node
 */
public class Chrest {
  // Domain definitions, if used
  private DomainSpecifics domainSpecifics;
  // internal clock
  private int clock;  
  // timing parameters
  private int addLinkTime;
  private int discriminationTime;
  private int familiarisationTime;
  // rho is the probability that a given learning operation will occur
  private float rho;
  // parameter for construction of semantic link
  private boolean createSemanticLinks;
  // - determines number of overlapping items in node images
  private int similarityThreshold;
  // - determines maximum distance to search semantic links
  private int maximumSemanticDistance = 1;
  // template construction parameters
  private boolean createTemplates;
  private int minTemplateLevel = 3;
  private int minTemplateOccurrences = 2;
  // long-term-memory holds information within the model permanently
  private int totalNodes;
  private Node visualLtm;
  private Node verbalLtm;
  private Node actionLtm;
  // short-term-memory holds information within the model temporarily, usually within one experiment
  private final Stm visualStm;
  private final Stm verbalStm;
  private final Stm actionStm; // TODO: Incorporate into displays
  // Perception module
  private final Perceiver perceiver;
  // Emotions module
  private EmotionAssociator emotionAssociator;

  /**
   * Creates an instance of Chrest to use as a model. All parameters are 
   * set to default values, and empty long-term and short-memories constructed.
   */
  public Chrest () {
    domainSpecifics = new GenericDomain ();
    addLinkTime = 10000;
    discriminationTime = 10000;
    familiarisationTime = 2000;
    rho = 1.0f;
    similarityThreshold = 4;

    clock = 0;
    totalNodes = 0;
    visualLtm = new Node (this, 0, Pattern.makeVisualList (new String[]{"Root"}));
    verbalLtm = new Node (this, 0, Pattern.makeVerbalList (new String[]{"Root"}));
    actionLtm = new Node (this, 0, Pattern.makeActionList (new String[]{"Root"}));
    totalNodes = 0; // Node constructor will have incremented totalNodes, so reset to 0
    visualStm = new Stm (4);
    verbalStm = new Stm (2);
    actionStm = new Stm (4);
    emotionAssociator = new EmotionAssociator ();

    createTemplates = true;
    createSemanticLinks = true;
    perceiver = new Perceiver (this);
  }

  /**
   * Retrieve the model's current domain specification: information
   * about the domain used, e.g., in learning or eye-movements.
   * @return domain specific information used in the current model.
   * @see DomainSpecifics
   */
  public DomainSpecifics getDomainSpecifics () {
    return domainSpecifics;
  }

  /**
   * Set the domain specification: information
   * about the domain used, e.g., in learning or eye-movements..
   * @param domainSpecifics the domain information to use.
   * @see DomainSpecifics
   */
  public void setDomain (DomainSpecifics domainSpecifics) {
    this.domainSpecifics = domainSpecifics;
  }

  /**
   * Accessor to retrieve time to add a new link.
   * @return the time to add a new link.
   */
  public int getAddLinkTime () {
    return addLinkTime;
  }

  /**
   * Modify time to add a new link.
   * @param time to add a new link.
   */
  public void setAddLinkTime (int time) {
    addLinkTime = time;
  }

  /**
   * Accessor to retrieve time to discriminate a new node.
   * @return the time for discrimination to complete
   */
  public int getDiscriminationTime () {
    return discriminationTime;
  }

  /**
   * Modify time to discriminate a new node.
   * @param time for discrimination to complete
   */
  public void setDiscriminationTime (int time) {
    discriminationTime = time;
  }

  /**
   * Accessor to retrieve time to familiarise image of a node.
   * @return time for familiarisation to complete
   */
  public int getFamiliarisationTime () {
    return familiarisationTime;
  }

  /**
   * Modify time to familiarise image of a node.
   * @param time for familiarisation to complete
   */
  public void setFamiliarisationTime (int time) {
    familiarisationTime = time;
  }

  /**
   * Accessor to retrieve value of rho, the probability of learning an item.
   * @return the probability of learning an item
   */
  public float getRho () {
    return rho;
  }

  /**
   * Modify value of rho, the probability of learning an item.
   * @param rho for the probability of learning an item
   */
  public void setRho (float rho) {
    this.rho = rho;
  }

  /**
   * Accessor to retrieve value of similarity threshold, the number of items 
   * which must be shared between two images for a semantic link to be formed.
   * @return the threshold of similarity
   */
  public float getSimilarityThreshold () {
    return similarityThreshold;
  }

  /**
   * Modify value of similarity threshold.
   * @param threshold for the threshold of similarity
   */
  public void setSimilarityThreshold (int threshold) {
    similarityThreshold = threshold;
  }

  /**
   * Modify option to create semantic links.
   * @param value to set/reset option to create semantic links.
   */
  public void setCreateSemanticLinks (boolean value) {
    createSemanticLinks = value;
  }

  /**
   * Accessor to option of whether to create semantic links.
   * @return set/reset option to create semantic links.
   */
  public boolean getCreateSemanticLinks () {
    return createSemanticLinks;
  }

  /**
   * Modify option to create templates.
   * @param value to set/reset option to create templates.
   */
  public void setCreateTemplates (boolean value) {
    createTemplates = value;
  }

  /**
   * Accessor to option of whether to create templates.
   * @return set/reset option to create templates.
   */
  public boolean getCreateTemplates () {
    return createTemplates;
  }

  /**
   * Accessor to value of minimum template level.
   * @return minimum template level.
   */
  protected int getMinTemplateLevel () {
    return minTemplateLevel;
  }

  /**
   * Accessor to minimum require occurrences for forming template.
   * @return minimum number of occurrences needed for forming a template.
   */
  protected int getMinTemplateOccurrences () {
    return minTemplateOccurrences;
  }

  /**
   * Modify values for template construction.
   * @param minLevel is the minimum level for setting a template.
   * @param minOccurrences is the minimum number of occurrences needed 
   *                       before setting a template.
   */
  public void setTemplateConstructionParameters (int minLevel, int minOccurrences) {
    minTemplateLevel = minLevel;
    minTemplateOccurrences = minOccurrences;
  }

  /**
   * Accessor to retrieve the size of visual short-term memory.
   * @return the size of visual short-term memory.
   */
  public int getVisualStmSize () {
    return visualStm.getSize ();
  }

  /**
   * Modify size of visual short-term memory.
   * @param size for visual short-term memory.
   */
  public void setVisualStmSize (int size) {
    visualStm.setSize (size);
    if (!frozen) pcs.firePropertyChange ("visualStmSize", null, null);
  }

  /**
   * Accessor to retrieve the size of verbal short-term memory.
   * @return the size of verbal short-term memory.
   */
  public int getVerbalStmSize () {
    return verbalStm.getSize ();
  }

  /**
   * Modify size of verbal short-term memory.
   * @param size for verbal short-term memory.
   */
  public void setVerbalStmSize (int size) {
    verbalStm.setSize (size);
    if (!frozen) pcs.firePropertyChange ("verbalStmSize", null, null);
  }

  /**
   * Accessor to retrieve current time of model.
   * @return the current clock value.
   */
  public int getClock () {
    return clock;
  }

  /**
   * Advance the clock by given amount.
   * @param time to advance clock by.
   */
  public void advanceClock (int time) {
    clock += time;
  }

  /**
   * Retrieve the next available node number for long-term memory.
   * Package access only, as should only be used by Node.java.
   * @return the next available node number.
   */
  int getNextNodeNumber () {
    totalNodes += 1;
    return totalNodes;
  }

  /**
   * Accessor to retrieve the total number of nodes within LTM.
   * @return the total number of nodes in long-term memory.
   */
  public int getTotalLtmNodes () {
    return totalNodes;
  }

  /**
   * Accessor to retrieve visual short-term memory of model.
   * @return the visual short-term memory.
   */
  public Stm getVisualStm () {
    return visualStm;
  }

  /**
   * Accessor to retrieve verbal short-term memory of model.
   * @return the verbal short-term memory.
   */
  public Stm getVerbalStm () {
    return verbalStm;
  }

  /**
   * Accessor to retrieve visual long-term memory of model.
   * @return the root node of visual long-term memory.
   */
  public Node getVisualLtm () {
    return visualLtm;
  }

  /** 
   * Returns the number of nodes in visual long-term memory.
   * @return the number of nodes in visual long-term memory.
   */
  public int ltmVisualSize () {
    return visualLtm.size ();
  }

  /**
   * Returns the average depth of nodes in visual long-term memory.
   * @return the average depth of nodes in visual long-term memory.
   */
  public double getVisualLtmAverageDepth () {
    return visualLtm.averageDepth ();
  }

  /**
   * Returns the average image size of nodes in visual long-term memory.
   * @return the average image size of nodes in visual long-term memory.
   */
  public double getVisualLtmAverageImageSize () {
    return visualLtm.averageImageSize ();
  }

  /**
   * Returns the number of nodes in verbal long-term memory.
   * @return the number of nodes in verbal long-term memory.
   */
  public int ltmVerbalSize () {
    return verbalLtm.size ();
  }

  /**
   * Returns the average depth of nodes in verbal long-term memory.
   * @return the average depth of nodes in verbal long-term memory.
   */
  public double getVerbalLtmAverageDepth () {
    return verbalLtm.averageDepth ();
  }

  /**
   * Returns the average image size of nodes in verbal long-term memory.
   * @return the average image size of nodes in verbal long-term memory.
   */
  public double getVerbalLtmAverageImageSize () {
    return verbalLtm.averageImageSize ();
  }

  /**
   * Returns the number of nodes in action long-term memory.
   * @return the number of nodes in action long-term memory.
   */
  public int ltmActionSize () {
    return actionLtm.size ();
  }

  /**
   * Returns the average depth of nodes in action long-term memory.
   * @return the average depth of nodes in action long-term memory.
   */
  public double getActionLtmAverageDepth () {
    return actionLtm.averageDepth ();
  }

  /**
   * Returns the average image size of nodes in action long-term memory.
   * @return the average image size of nodes in action long-term memory.
   */
  public double getActionLtmAverageImageSize () {
    return actionLtm.averageImageSize ();
  }

  /**
   * Model is 'experienced' if it has at least 2000 nodes in LTM.
   * This parameter is taken from de Groot and Gobet (1996) to indicate 
   * point when master-level eye heuristics are used instead of novice 
   * ones.
   *
   * @return true if the model is regarded as experienced.
   */
  public boolean isExperienced () {
    if (!experienced) {
      if (ltmVisualSize()+ltmVerbalSize()+ltmActionSize() > 2000)
        experienced = true;
    }
    return experienced;
  }
  private boolean experienced = false; // for caching experience level

  /**
   * Instruct model to construct templates, if the 'constructTemplates' flag is true.  
   * This method should be called at the end of the learning process.
   * Note, the template construction process only currently works for visual patterns 
   * using the ItemSquarePattern primitive.
   */
  public void constructTemplates () {
    if (createTemplates) {
      visualLtm.constructTemplates ();
    }
  }

  /**
   * Return a count of the number of templates in the model's visual LTM.
   * @return a count of the number of templates in the model's visual LTM.
   */
  public int countTemplates () {
    return visualLtm.countTemplates ();
  }

  /**
   * Return the root node of the long-term memory which the given pattern
   * would be sorted through, based on its modality.
   * @param pattern to use for selecting long-term memory.
   * @return root node of long-term memory matching the pattern.
   */
  public Node getLtmByModality (ListPattern pattern) {
    if (pattern.isVisual ()) {
      return visualLtm;
    } else if (pattern.isVerbal ()) {
      return verbalLtm;
    } else { // if (pattern.isAction ()) 
      return actionLtm;
    }
  }

  /**
   * Return the short-term memory with the same modality as the given pattern.
   * @param pattern to use for selecting short-term memory.
   * @return short-term memory matching the pattern.
   */
  private Stm getStmByModality (ListPattern pattern) {
    if (pattern.isVisual ()) {
      return visualStm;
    } else if (pattern.isVerbal ()) {
      return verbalStm;
    } else { // if (pattern.isAction ()) 
      return actionStm;
    }
  }

  // use to freeze/unfreeze updates to the model to prevent GUI
  // seizing up during training
  private boolean frozen = false;
  
  /**
   * Instruct model not to update listeners.
   */
  public void freeze () {
    frozen = true;
  }

  /**
   * Instruct model to now update listeners for future changes.
   * Also triggers an immediate update of current listeners.
   */
  public void unfreeze () {
    frozen = false;
    pcs.firePropertyChange ("frozen", true, false);
  }

  /**
   * Return a map from content sizes to frequencies for the model's LTM.
   * @return map from content sizes to frequencies.
   */ 
  public Map<Integer, Integer> getContentCounts () {
    Map<Integer, Integer> size = new HashMap<> ();

    visualLtm.getContentCounts (size);
    verbalLtm.getContentCounts (size);
    actionLtm.getContentCounts (size);

    return size;
  }

  /**
   * Return a map from image sizes to frequencies for the model's LTM.
   * @return map from image sizes to frequencies
   */ 
  public Map<Integer, Integer> getImageCounts () {
    Map<Integer, Integer> size = new HashMap<> ();

    visualLtm.getImageCounts (size);
    verbalLtm.getImageCounts (size);
    actionLtm.getImageCounts (size);

    return size;
  }

  /**
   * Return a map from number of semantic links to frequencies for the model's LTM.
   * @return map from number of semantic links to frequencies
   */ 
  public Map<Integer, Integer> getSemanticLinkCounts () {
    Map<Integer, Integer> size = new HashMap<> ();

    visualLtm.getSemanticLinkCounts (size);
    verbalLtm.getSemanticLinkCounts (size);
    actionLtm.getSemanticLinkCounts (size);

    return size;
  }

  /**
   * Add given node to STM.  Check for formation of semantic links by
   * comparing incoming node with the hypothesis, or 'largest', node.
   *
   * @param node to add to short-term memory.
   */
  private void addToStm (Node node) {
    Stm stm = getStmByModality (node.getImage ());

    if (stm.getCount () > 0) {
      Node check = stm.getItem (0); // TODO: make this the hypothesis node
      if (check.getContents().isVisual () && // only add semantic links for visual
          check != node && 
          node.getImage().isSimilarTo (check.getImage (), similarityThreshold)) {
        node.addSemanticLink (check); 
        check.addSemanticLink (node); // two-way semantic link
      }
    }

    // TODO: Check if this is the best place
    // Idea is that node's filled slots are cleared when put into STM, 
    // are filled whilst in STM, and forgotten when it leaves.
    node.clearFilledSlots (); 
    stm.add (node);

    // inform listeners of a change in model's state
    if (!frozen) pcs.firePropertyChange ("addToStm", null, null);
  }

  /**
   * Accessor to retrieve the model's perceiver object.
   * @return perceiver instance for this model.
   */
  public Perceiver getPerceiver () {
    return perceiver;
  }

  /** 
   * Retrieve a node in long-term memory using the given ListPattern.
   * The sorting process works through the children of the currentNode.
   * If the link's test matches the remaining part of the pattern, then 
   * the current node is updated, and searching continues through the 
   * children of the new node.
   *
   * @param pattern for the model to examine.
   * @return node in long-term memory which best matches the given pattern.
   */
  public Node recognise (ListPattern pattern) {
    Node currentNode = getLtmByModality (pattern);
    List<Link> children = currentNode.getChildren ();
    ListPattern sortedPattern = pattern;
    int nextLink = 0;

    while (nextLink < children.size ()) {
      Link link = children.get (nextLink);
      if (link.passes (sortedPattern)) { // descend a test link in network
        // reset the current node, list of children and link index
        currentNode = link.getChildNode ();
        children = link.getChildNode().getChildren ();
        nextLink = 0;
        // remove the matched test from the sorted pattern
        sortedPattern = sortedPattern.remove (link.getTest ());
      } else { // move on to the next link on same level
        nextLink += 1;
      }
    }

    // final step, for short patterns
    if (sortedPattern.isEmpty()) {
      for (Link endTest : currentNode.getChildren ()) {
        if (endTest.getTest().isEmpty () && endTest.getTest().isFinished ()) {
          currentNode = endTest.getChildNode ();
        }
      }
    }

    // add retrieved node to STM
    addToStm (currentNode);

    // return retrieved node
    return currentNode;
  }

  /** 
   * The model will examine the given pattern at the given time, and then attempt 
   * to learn from it.
   * First, the pattern is sorted.  Then, if the retrieved node is the 
   * root node or its image mismatches the pattern, discrimination is 
   * used to extend the network.  Otherwise, new information will be added 
   * to the image using the pattern.
   *
   * @param pattern for the model to examine.
   * @param time at which the pattern should be examined.
   * @return node in long-term memory which best matches the given pattern.
   */
  public Node recogniseAndLearn (ListPattern pattern, int time) {
    Node currentNode = recognise (pattern);
    if (clock <= time) { // only try to learn if model clock is 'behind' the time of the call
      if (Math.random () < rho) { // depending on rho, may refuse to learn some random times
        clock = time; // bring clock up to date
        if (!currentNode.getImage().equals (pattern)) { // only try any learning if image differs from pattern
          if (currentNode == getLtmByModality (pattern) || // if is rootnode
              !currentNode.getImage().matches (pattern) || // or mismatch on image
              currentNode.getImage().isFinished ()) {      // or image finished
            currentNode = currentNode.discriminate (pattern); // then discriminate
          } else  { // else familiarise
            currentNode = currentNode.familiarise (pattern);
          }
          addToStm (currentNode); // add to stm, as node may have changed during learning
        }
      }
    }
    return currentNode;
  }

  /**
   * The model will examine the given pattern and then attempt to learn from it.
   * The current model time is used as the time of presentation of the pattern.
   *
   * @param pattern for the model to examine.
   * @return node in long-term memory which best matches the given pattern.
   */
  public Node recogniseAndLearn (ListPattern pattern) {
    return recogniseAndLearn (pattern, clock);
  }

  /**
   * Used to learn an association between two patterns.  The two patterns may be 
   * of the same or different modality.  
   *
   * @param pattern1 the first of the two patterns
   * @param pattern2 the second of the two patterns
   * @param time at which the patterns should be examined
   * @return the node learnt for the first pattern.
   */
  public Node associateAndLearn (ListPattern pattern1, ListPattern pattern2, int time) {
    if (ListPattern.isSameModality (pattern1, pattern2)) {
      return learnAndLinkPatterns(pattern1, pattern2, time);
    } else {
      // TODO: Handle differing modalities.
      return null;
    }
  }

  /**
   * Used to learn an association between two patterns.  The two patterns may be 
   * of the same or different modality.  The current model time is used as the 
   * time of presentation.
   *
   * @param pattern1 the first of the two patterns
   * @param pattern2 the second of the two patterns
   * @return the node learnt for the first pattern.
   */
  public Node associateAndLearn (ListPattern pattern1, ListPattern pattern2) {
    return associateAndLearn (pattern1, pattern2, clock);
  }

  /**
   * Asks Chrest to return the image of the node obtained by sorting given 
   * pattern through the network.
   *
   * @param pattern to examine
   * @return pattern from best matching node in long-term memory.
   */
  public ListPattern recallPattern (ListPattern pattern) {
    return recognise(pattern).getImage ();
  }

  /** 
   * Asks Chrest to return the image of the node which is associated 
   * with the node obtained by sorting given pattern through the network.
   *
   * @param pattern to examine
   * @return pattern from associated node in long-term memory, or null.
   */
  public ListPattern associatePattern (ListPattern pattern) {
    Node retrievedNode = recognise (pattern);
    if (retrievedNode.getAssociatedNode () != null) {
      return retrievedNode.getAssociatedNode().getImage ();
    } else {
      return null;
    }
  }

  /**
   * Asks Chrest to return the image of the node which names the node 
   * obtained by sorting given pattern through the network.
   *
   * @param pattern to examine
   * @return pattern from named node in long-term memory, or null.
   */
  public ListPattern namePattern (ListPattern pattern) {
    Node retrievedNode = recognise (pattern);
    if (retrievedNode.getNamedBy () != null) {
      return retrievedNode.getNamedBy().getImage ();
    } else {
      return null;
    }
  }

  /**
   * Presents Chrest with a pair of patterns, which it should learn and 
   * then attempt to learn a link.  Assumes the two patterns are of the same modality.
   *
   * @param pattern1 the first of the two patterns
   * @param pattern2 the second of the two patterns
   * @return the node learnt for the first pattern.
   */
  private Node learnAndLinkPatterns (ListPattern pattern1, ListPattern pattern2, int time) {
    Node pat1Retrieved = recognise (pattern1);
    // 1. is retrieved node image a match for pattern1?
    if (pat1Retrieved.getImage().matches (pattern1)) {
      // 2. does retrieved node have a lateral link?
      if (pat1Retrieved.getAssociatedNode() != null) {
        // if yes
        //   3. is linked node image match pattern2? if not, learn pattern2
        if (pat1Retrieved.getAssociatedNode().getImage().matches (pattern2)) {
          //   if yes
          //   4. if linked node image == pattern2, learn pattern1, else learn pattern2
          if (pat1Retrieved.getAssociatedNode().getImage().equals (pattern2)) {
            recogniseAndLearn (pattern1, time); // TODO: this is overlearning?
          } else {
            recogniseAndLearn (pattern2, time);
          }
        } else {
          recogniseAndLearn (pattern2, time);
          // force it to correct a mistake
          recogniseAndLearn (pattern1, time);
          if (clock <= time) {
            Node pat2Retrieved = recognise (pattern2);
            // 6. if pattern2 retrieved node image match for pattern2, learn link, else learn pattern2
            if (pat2Retrieved.getImage().matches (pattern2)) {
              associateTwoNodes (pat1Retrieved, pat2Retrieved);
            }
          }
        } 
      } else {
        // if not
        // 5. sort pattern2
        Node pat2Retrieved = recognise (pattern2);
        // 6. if pattern2 retrieved node image match for pattern2, learn link, else learn pattern2
        if (pat2Retrieved.getImage().matches (pattern2)) {
          associateTwoNodes (pat1Retrieved, pat2Retrieved);
        } else { // image not a match, so we need to learn pattern 2
          recogniseAndLearn (pattern2, time);
          // 5. sort pattern2
          pat2Retrieved = recognise (pattern2);
          // 6. if pattern2 retrieved node image is a match for pattern2, learn link
          if (pat2Retrieved.getImage().matches (pattern2)) {
            associateTwoNodes (pat1Retrieved, pat2Retrieved);
          }
        }
      }
    } else { // image not a match, so we need to learn pattern 1
      recogniseAndLearn (pattern1, time);
    }
    return pat1Retrieved;
  }

  /** Make node1 associated with node2, and update clock
   * @param node1 the first of the two nodes
   * @param node2 the second of the two nodes
   */
  private void associateTwoNodes (Node node1, Node node2) {
    node1.setAssociatedNode (node2);
    advanceClock (getAddLinkTime ());
    if (!frozen) pcs.firePropertyChange ("learnAndlink", null, null);
  }

  /**
   * Learns the two patterns assuming the time of presentation is the current 
   * Chrest clock time.
   */
  private void learnAndLinkPatterns (ListPattern pattern1, ListPattern pattern2) {
    learnAndLinkPatterns (pattern1, pattern2, clock);
  }

  /**
   * Learn and link a visual and verbal pattern with a naming link.
   *
   * @param pattern1 is the first of the two patterns, and should be a visual pattern
   * @param pattern2 is the second of the two patterns, and should be a verbal pattern
   * @param time is the time at which the learning should occur
   */
  public void learnAndNamePatterns (ListPattern pattern1, ListPattern pattern2, int time) {
    recogniseAndLearn (pattern1, time);
    recogniseAndLearn (pattern2, time);
    if (clock <= time) {
      if (pattern1.isVisual () && pattern2.isVerbal () && visualStm.getCount () > 0 && verbalStm.getCount () > 0) {
        visualStm.getItem(0).setNamedBy (verbalStm.getItem (0));
        advanceClock (getAddLinkTime ());
      }
      if (!frozen) pcs.firePropertyChange ("learnAndName", null, null);
    }
  }

  /**
   * Learn and link a visual and verbal pattern with a naming link, using the 
   * current model time as the time of learning.
   *
   * @param pattern1 is the first of the two patterns, and should be a visual pattern
   * @param pattern2 is the second of the two patterns, and should be a verbal pattern
   */
  public void learnAndNamePatterns (ListPattern pattern1, ListPattern pattern2) {
    learnAndNamePatterns (pattern1, pattern2, clock);
  }

  /**
   * Given a scene, model uses its perceiver the scan the scene for the 
   * given number of fixations, learning from the patterns observed in the 
   * scene.
   *
   * @param scene to scan and learn from
   * @param numFixations the number of eye fixations to make on the scene
   */
  public void learnScene (Scene scene, int numFixations) {
    perceiver.setScene (scene);
    perceiver.start (numFixations);
    for (int i = 0; i < numFixations; i++) {
      perceiver.moveEyeAndLearn ();
    }
  }

  /**
   * Learn a scene with an attached next move. The scene is scanned using 
   * the model's perceiver module for the given number of fixations. 
   * The move is linked to any chunks in visual STM.
   *
   * TODO: think about if there should be limitations on this.
   *
   * @param scene to scan and learn from
   * @param move to associate with the learned fixations
   * @param numFixations the number of eye fixations to make on the scene
   */
  public void learnSceneAndMove (Scene scene, Move move, int numFixations) {
    learnScene (scene, numFixations);
    recogniseAndLearn (move.asListPattern ());
    // attempt to link action with each perceived chunk
    if (visualStm.getCount () > 0 && actionStm.getCount () > 0) {
      for (Node node : visualStm) {
        node.addActionLink (actionStm.getItem (0));
      }
    }
    if (!frozen) pcs.firePropertyChange ("visualSTM", null, null);
  }

  private boolean sameColour (ListPattern move, String colour) {
    if (colour == null) return true;
    if ((move.size () == 1) && (move.getItem(0) instanceof ItemSquarePattern)) {
      ItemSquarePattern m = (ItemSquarePattern)move.getItem (0);
      return m.getItem() == colour;
    } else {
      return false;
    }
  }

  /**
   * Scan the given scene for the given number of fixations. 
   * Retrieve any familiar patterns from long-term memory, and extract 
   * any linked moves which relate to the given colour. 
   * Return a map of the linked moves identified with a count of their frequency.
   *
   * @param scene to scan
   * @param numFixations the number of eye fixations to make on the scene
   * @param colour of pieces to look for
   * @return a map of proposed moves with their frequences
   */
  public Map<ListPattern, Integer> getMovePredictions (Scene scene, int numFixations, String colour) {
    scanScene (scene, numFixations);
    // create a map of moves to their frequency of occurrence in nodes of STM
    Map<ListPattern, Integer> moveFrequencies = new HashMap<ListPattern, Integer> ();
    for (Node node : visualStm) {
      for (Node action : node.getActionLinks ()) {
        if (sameColour(action.getImage(), colour)) {
          if (moveFrequencies.containsKey(action.getImage ())) {
            moveFrequencies.put (
                action.getImage (), 
                moveFrequencies.get(action.getImage ()) + 1
                );
          } else {
            moveFrequencies.put (action.getImage (), 1);
          }
        }
      }
    }
    return moveFrequencies;
  }

  /**
   * Predict a move using a CHUMP-like mechanism.
   * TODO: Improve the heuristics here.
   *
   * @param scene to scan
   * @param numFixations the number of eye fixations to make on the scene
   * @return a move
   */
  public Move predictMove (Scene scene, int numFixations) {
    Map<ListPattern, Integer> moveFrequencies = getMovePredictions (scene, numFixations, null);
    // find the most frequent pattern
    ListPattern best = null;
    int bestFrequency = 0;
    for (ListPattern key : moveFrequencies.keySet ()) {
      if (moveFrequencies.get (key) > bestFrequency) {
        best = key;
        bestFrequency = moveFrequencies.get (key);
      }
    }
    // create a move to return
    if (best == null) {
      return new Move ("UNKNOWN", 0, 0);
    } else {
      // list pattern should be one item long, with the first item being an ItemSquarePattern
      if ((best.size () == 1) && (best.getItem(0) instanceof ItemSquarePattern)) {
        ItemSquarePattern move = (ItemSquarePattern)best.getItem (0);
        return new Move (move.getItem (), move.getRow (), move.getColumn ());
      } else {
        return new Move ("UNKNOWN", 0, 0);
      }
    }
  }

  /**
   * Predict a move using a CHUMP-like mechanism.
   * TODO: Improve the heuristics here.
   *
   * @param scene to scan
   * @param numFixations the number of eye fixations to make on the scene
   * @param colour of player to move
   * @return a move
   */
  public Move predictMove (Scene scene, int numFixations, String colour) {
    Map<ListPattern, Integer> moveFrequencies = getMovePredictions (scene, numFixations, colour);
    // find the most frequent pattern
    ListPattern best = null;
    int bestFrequency = 0;
    for (ListPattern key : moveFrequencies.keySet ()) {
      if (moveFrequencies.get (key) > bestFrequency) {
        best = key;
        bestFrequency = moveFrequencies.get (key);
      }
    }
    // create a move to return
    if (best == null) {
      return new Move ("UNKNOWN", 0, 0);
    } else {
      // list pattern should be one item long, with the first item being an ItemSquarePattern
      if ((best.size () == 1) && (best.getItem(0) instanceof ItemSquarePattern)) {
        ItemSquarePattern move = (ItemSquarePattern)best.getItem (0);
        return new Move (move.getItem (), move.getRow (), move.getColumn ());
      } else {
        return new Move ("UNKNOWN", 0, 0);
      }
    }
  }

  /** 
   * Scan given scene, then return a scene which would be recalled.
   * The recalled scene is reconstructed from the recognised patterns.
   * Default behaviour is to clear STM before scanning a scene.
   *
   * @param scene to scan
   * @param numFixations the number of eye fixations to make on the scene
   * @return scene which would be recalled
   */
  public Scene scanScene (Scene scene, int numFixations) {  
    return scanScene (scene, numFixations, true);
  }

  /** 
   * Scan given scene, then return a scene which would be recalled.
   * The recalled scene is reconstructed from the recognised patterns.
   *
   * @param scene to scan
   * @param numFixations the number of eye fixations to make on the scene
   * @param clearStm set if STM should be cleared first
   * @return scene which would be recalled
   */
  public Scene scanScene (Scene scene, int numFixations, boolean clearStm) {
    if (clearStm) { // only clear STM if flag is set
      visualStm.clear ();
    }
    perceiver.setScene (scene);
    perceiver.start (numFixations);
    for (int i = 0; i < numFixations; i++) {
      perceiver.moveEye ();
    }
    // build up and return recalled scene
    Scene recalledScene = new Scene ("Recalled scene of " + scene.getName (), 
        scene.getHeight (), scene.getWidth ());
    // -- get items from image in STM, and optionally template slots
    // TODO: use frequency count in recall
    for (Node node : visualStm) {
      ListPattern recalledInformation = node.getImage();
      if (createTemplates) { // check if templates needed
        recalledInformation = recalledInformation.append(node.getFilledSlots ());
      }
      for (PrimitivePattern item : recalledInformation) {
        if (item instanceof ItemSquarePattern) {
          ItemSquarePattern ios = (ItemSquarePattern)item;
          recalledScene.setItem (ios.getRow ()-1, ios.getColumn ()-1, ios.getItem ());
        }
      }
    }

    return recalledScene;
  }

  /** 
   * Clear the STM and LTM of the model, and reset its clock.
   */
  public void clear () {
    clock = 0;
    visualLtm.clear ();
    verbalLtm.clear ();
    actionLtm.clear ();
    visualLtm = new Node (this, 0, Pattern.makeVisualList (new String[]{"Root"}));
    verbalLtm = new Node (this, 0, Pattern.makeVerbalList (new String[]{"Root"}));
    actionLtm = new Node (this, 0, Pattern.makeActionList (new String[]{"Root"}));
    totalNodes = 0;
    visualStm.clear ();
    verbalStm.clear ();
    if (!frozen) pcs.firePropertyChange ("clear", null, null);
  }

  /** 
   * Write ltm to given Writer object in dot format.
   *
   * @param writer to write information to
   * @throws IOException in case of write error
   */
  public void writeLtmAsDot (Writer writer) throws IOException {
    writer.write ("digraph G {\n");
    writer.write ("  n0 [shape=circle,style=filled,color=black]\n");
    writer.write ("  n0 -> vis\n");
    visualLtm.writeAsDot (writer);
    writer.write ("  n0 -> ver\n");
    verbalLtm.writeAsDot (writer);
    writer.write ("  n0 -> act\n");
    actionLtm.writeAsDot (writer);
    writer.write ("}\n");
  }

  /** 
   * Write model to given Writer object in VNA format.
   *
   * @param writer to write information to
   * @throws IOException in case of write error
   */
  public void writeModelAsVna (Writer writer) throws IOException {
    writer.write ("*Node data\n\"ID\", \"contents\"\n");
    visualLtm.writeNodeAsVna (writer);
    writer.write ("*Tie data\nFROM TO\n");
    visualLtm.writeLinksAsVna (writer);
  }

  /** 
   * Write model semantic links to given Writer object in VNA format.
   *
   * @param writer to write information to
   * @throws IOException in case of write error
   */
  public void writeModelSemanticLinksAsVna (Writer writer) throws IOException {
    writer.write ("*Node data\n\"ID\", \"contents\"\n");
    visualLtm.writeNodeAsVna (writer);
    writer.write ("*Tie data\nFROM TO\n");
    visualLtm.writeSemanticLinksAsVna (writer);
  }

  /**
   * Sets the default value of alpha, for the emotion associator.
   *
   * @param alpha to use in emotion associator
   */
  public void setDefaultAlpha (double alpha) {
    emotionAssociator.setDefaultAlpha (alpha);
  }

  /**
   * Accessor for Emotion Associator.
   *
   * @return the Emotion Associator
   * @see EmotionAssociator
   */
  public EmotionAssociator getEmotionAssociator () {
    return emotionAssociator;
  }

  /**
   * Propagate emotion across all the given STMs.
   *
   * @param stmsobject, an array of short-term memories
   */
  public void emoteAndPropagateAcrossModalities (Object stmsobject) {
    Stm[] stms = (Stm[]) stmsobject;
    emotionAssociator.emoteAndPropagateAcrossModalities (stms, clock);
  }

  /**
   * Attach given emotion to top item in STM, if present.
   *
   * @param stm, a short-term memory
   * @param emotion, the emotion to attach
   */
  public void assignEmotionToCurrentItem (Stm stm, Emotion emotion) {
    if (stm.getCount () == 0) {
      return;  // STM empty, so nothing to be done
    }
    emotionAssociator.setRWEmotion (stm.getItem(0), emotion);
  }

  /** 
   * Accessor for the emotion associated with the topmost item in STM.
   *
   * @param stm, a short-term memory
   * @return emotion associated with the topmost item in given short-term memory
   */
  public Emotion getCurrentEmotion (Stm stm) {
    if (stm.getCount () == 0) {
      return null;
    } else {
      return emotionAssociator.getRWEmotion (stm.getItem (0));
    }
  }

  /** 
   * Returns the emotion of the node associated with the top-most item 
   * in given short-term memory.
   *
   * @param stm, a short-term memory
   * @return emotion from node associated with the top-most node in given short-term memory.
   */
  public Emotion getCurrentFollowedByEmotion (Stm stm) {
    if (stm.getCount () == 0) {
      return null;
    } else {
      Node followed_by = stm.getItem(0).getAssociatedNode ();
      if (followed_by == null) {
        return null;
      } else {
        return emotionAssociator.getRWEmotion (followed_by);
      }
    }
  }

  // support for property change listeners
  private final PropertyChangeSupport pcs = new PropertyChangeSupport (this);

  /**
   * Add an object to respond to any changes made to this Chrest instance. 
   * Typically used to update GUI views.
   *
   * @param listener object
   */
  public void addPropertyChangeListener (PropertyChangeListener listener) {
    pcs.addPropertyChangeListener (listener);
  }

  /**
   * Remove an object previously set to respond to any changes made to this 
   * Chrest instance. Typically used to update GUI views.
   *
   * @param listener object
   */
  public void removePropertyChangeListener (PropertyChangeListener listener) {
    pcs.removePropertyChangeListener (listener);
  }
}
