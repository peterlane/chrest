package chresttests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.Test;

import jchrest.architecture.*;
import jchrest.lib.*;

public class TestModel {

  // Process Test
  @Test
  public void timings () {
    Chrest model = new Chrest ();
    model.setRho (1.0f);
    model.setFamiliarisationTime (2000);
    model.setDiscriminationTime (10000);

    ListPattern patternA = Pattern.makeVisualList(new String[]{"B", "I", "F"});
    ListPattern patternB = Pattern.makeVisualList(new String[]{"X", "A", "Q"});

    assertEquals (0, model.getClock ());
    // check change on one learning operation
    model.recogniseAndLearn (patternA); // -- discriminate node for "B"
    assertEquals (10000, model.getClock ());
    // check change on second learning operation
    model.recogniseAndLearn (patternA); // -- familiarise node for "B"
    assertEquals (12000, model.getClock ());
    // check a busy model is not changed
    model.recogniseAndLearn (patternB, 10000); // -- busy, no change
    assertEquals (12000, model.getClock ());
    model.recogniseAndLearn (patternA); // -- discriminate node for "I"
    assertEquals (22000, model.getClock ());
    model.recogniseAndLearn (patternA); // -- familiarise node for "BI"
    assertEquals (24000, model.getClock ());
    model.recogniseAndLearn (patternA); // -- discriminate node for "F"
    assertEquals (34000, model.getClock ());
    model.recogniseAndLearn (patternA); // -- familiarise node for "BIF"
    assertEquals (36000, model.getClock ());
    model.recogniseAndLearn (patternA); // -- no change, pattern fully learnt
    assertEquals (36000, model.getClock ());
  }

  // Process Test
  @Test
  public void baseCase () {
    Chrest model = new Chrest ();
    ListPattern empty = Pattern.makeVisualList (new int[]{});
    assertTrue (Pattern.makeVisualList(new String[]{"Root"}).equals (model.recognise(empty).getImage ()));
  }

  // Process Test
  @Test
  public void learningCase1 () {
    Chrest model = new Chrest ();
    ListPattern empty = Pattern.makeVisualList (new int[]{});
    ListPattern list = Pattern.makeVisualList (new int[]{1,2,3,4});
    list.setFinished ();
    ListPattern primTest = Pattern.makeVisualList (new int[]{1});

    model.recogniseAndLearn (list);
    assertEquals (1, model.getLtmByModality(list).getChildren().size ());

    Link firstChild = model.getLtmByModality(list).getChildren().get (0);
    assertFalse (empty.equals (firstChild.getChildNode().getContents ()));
    assertTrue (firstChild.getTest().equals (primTest));
    assertTrue (firstChild.getChildNode().getContents().equals (primTest));
    assertTrue (firstChild.getChildNode().getImage().equals (empty));
  }
  
  // Process Test
  // Same as learningCase1, but using item-on-square instead of simple numbers
  @Test
  public void learningCase2 () {
    Chrest model = new Chrest ();
    ListPattern empty = new ListPattern ();
    ListPattern list = new ListPattern ();
    list.add (new ItemSquarePattern ("P", 1, 2));
    list.add (new ItemSquarePattern ("P", 2, 2));
    list.add (new ItemSquarePattern ("P", 3, 2));
    list.add (new ItemSquarePattern ("P", 4, 2));
    list.setFinished ();
    ListPattern primTest = new ListPattern ();
    primTest.add (new ItemSquarePattern ("P", 1, 2));

    model.recogniseAndLearn (list);
    assertEquals (1, model.getLtmByModality(list).getChildren().size ());

    Link firstChild = model.getLtmByModality(list).getChildren().get (0);
    assertFalse (empty.equals (firstChild.getChildNode().getContents ()));
    assertTrue (firstChild.getTest().equals (primTest));
    assertTrue (firstChild.getChildNode().getContents().equals (primTest));
    assertTrue (firstChild.getChildNode().getImage().equals (empty));
  }

  // Process Test
  // Check that after learning a primitive, the model will retrieve
  // that node on trying to recognise the list
  @Test
  public void simpleRetrieval () {
    Chrest model = new Chrest ();
    ListPattern empty = Pattern.makeVisualList (new int[]{});
    ListPattern list = Pattern.makeVisualList (new int[]{1,2,3,4});
    list.setFinished ();
    ListPattern primTest = Pattern.makeVisualList (new int[]{1});

    model.recogniseAndLearn (list);
    Node node = model.recognise (list);

    assertFalse (empty.equals (node.getContents ()));
    assertTrue (primTest.equals (node.getContents ()));
    assertTrue (empty.equals (node.getImage ()));
  }

  // Process Test
  @Test
  public void simpleLearning () {
    Chrest model = new Chrest ();
    ListPattern empty = Pattern.makeVisualList (new int[]{});
    ListPattern list = Pattern.makeVisualList (new int[]{1,2,3,4});
    ListPattern list2 = Pattern.makeVisualList (new int[]{2,3,4});
    ListPattern list3 = Pattern.makeVisualList (new int[]{1,3,4});
    ListPattern list3Test = Pattern.makeVisualList (new int[]{1,3});
    ListPattern prim1 = Pattern.makeVisualList (new int[]{1});
    ListPattern prim2 = Pattern.makeVisualList (new int[]{2});

    model.recogniseAndLearn (list2);
    model.recogniseAndLearn (list);
    assertEquals (2, model.getLtmByModality(list).getChildren().size ());
    // check most recent becomes the first child node
    assertTrue (prim1.equals (model.getLtmByModality(list).getChildren().get(0).getChildNode().getContents ()));
    assertTrue (prim2.equals (model.getLtmByModality(list).getChildren().get(1).getChildNode().getContents ()));
    // force discriminate from node 0
    // by first overlearning
    model.recogniseAndLearn (list);
    model.recogniseAndLearn (list);
    assertTrue (model.recognise(list).getImage().equals (Pattern.makeVisualList(new int[]{1,2})));
    Node node = model.getLtmByModality(list).getChildren().get(0).getChildNode ();
    assertEquals (0, node.getChildren().size ());
    model.recogniseAndLearn (list3); // first learn the '3' to use as test
    model.recogniseAndLearn (list3); // now trigger discrimination
    assertEquals (1, node.getChildren().size ());
    assertTrue (node.getContents().equals (node.getChildren().get(0).getChildNode().getImage ()));
    assertTrue (list3Test.equals (node.getChildren().get(0).getChildNode().getContents ()));

    // and familiarise
    node = node.getChildren().get(0).getChildNode ();
    model.recogniseAndLearn (list3);
    model.recogniseAndLearn (list3);
    model.recogniseAndLearn (list3);
    assertTrue (list3.equals (node.getImage ()));
  }

  // Process Test
  @Test
  public void learnNil () {
    Chrest model = new Chrest ();
    ListPattern list1 = Pattern.makeVisualList (new String[]{"A", "B", "C"});
    ListPattern list2 = Pattern.makeVisualList (new String[]{"A", "B"});
    ListPattern testb = Pattern.makeVisualList (new String[]{"B"});
    ListPattern test$ = Pattern.makeVisualList (new String[]{});
    test$.setFinished ();

    for (int i = 0; i < 8; i += 1) {
      model.recogniseAndLearn (list1);
    }

    assertTrue (list1.equals (model.recallPattern (list1)));
    assertTrue (list1.equals (model.recallPattern (list2)));
    Node node = model.recognise (list2);
    assertTrue (list1.equals (node.getImage ()));
    assertEquals (0, node.getChildren().size ());
    model.recogniseAndLearn(list2);
    assertEquals(1, node.getChildren().size ());
    assertTrue(testb.equals(node.getChildren().get(0).getTest ()));

    for (int i = 0; i < 8; i += 1) {
      model.recogniseAndLearn (list1);
    }
    node = model.recognise (list2);
    assertTrue (list1.equals (node.getImage ()));

    // we now have ROOT - <a> <a b c> - <b> <a b c>
    // learning <a b> must now discriminate from the last
    // node, and use '$' as a test
    model.recogniseAndLearn (list2); // learns <$> at top
    model.recogniseAndLearn (list2); // adds test to node
    assertEquals (1, node.getChildren().size ());
    assertTrue(test$.equals(node.getChildren().get(0).getTest ()));
    // Check we can retrieve the two target patterns
    assertTrue(list1.equals(model.recallPattern (list1)));
    assertTrue(list2.equals(model.recallPattern (list2)));
  }

  // Process Test
  @Test
  public void learnNilInVerbalPattern () {
    Chrest model = new Chrest ();
    ListPattern list1 = Pattern.makeVerbalList (new String[]{"A", "B", "C"});
    ListPattern list2 = Pattern.makeVerbalList (new String[]{"A", "B"});
    ListPattern testb = Pattern.makeVerbalList (new String[]{"B"});
    ListPattern test$ = Pattern.makeVerbalList (new String[]{});
    test$.setFinished ();

    for (int i = 0; i < 8; i += 1) {
      model.recogniseAndLearn (list1);
    }

    assertTrue (list1.equals (model.recallPattern (list1)));
    assertTrue (list1.equals (model.recallPattern (list2)));
    Node node = model.recognise (list2);
    assertTrue (list1.equals (node.getImage ()));
    assertEquals (0, node.getChildren().size ());
    model.recogniseAndLearn(list2);
    assertEquals(1, node.getChildren().size ());
    assertTrue(testb.equals(node.getChildren().get(0).getTest ()));

    for (int i = 0; i < 8; i += 1) {
      model.recogniseAndLearn (list1);
    }
    node = model.recognise (list2);
    assertTrue (list1.equals (node.getImage ()));

    // we now have ROOT - <a> <a b c> - <b> <a b c>
    // learning <a b> must now discriminate from the last
    // node, and use '$' as a test
    model.recogniseAndLearn (list2); // learns <$> at top
    model.recogniseAndLearn (list2); // adds test to node
    assertEquals (1, node.getChildren().size ());
    assertTrue(test$.equals(node.getChildren().get(0).getTest ()));
    // Check we can retrieve the two target patterns
    assertTrue(list1.equals(model.recallPattern (list1)));
    assertTrue(list2.equals(model.recallPattern (list2)));
  }

  // Process Test
  @Test
  public void fullLearning () {
    Chrest model = new Chrest ();
    ListPattern list1 = Pattern.makeVisualList (new int[]{3,4});
    ListPattern list2 = Pattern.makeVisualList (new int[]{1,2});

    for (int i = 0; i < 20; i += 1) {
      model.recogniseAndLearn (list1);
      model.recogniseAndLearn (list2);
    }

    assertTrue (list1.equals (model.recallPattern (list1)));
    assertTrue (list2.equals (model.recallPattern (list2)));
  }
}
