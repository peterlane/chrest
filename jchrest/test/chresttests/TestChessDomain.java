package chresttests;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.Test;

import jchrest.architecture.*;
import jchrest.lib.*;

public class TestChessDomain {

  private boolean any (Set<Square> squares, int row, int col) {
    for (Square square : squares) {
      if (square.getRow () == row && square.getColumn () == col) {
        return true;
      }
    }
    return false;
  }

  // Unit Test
  @Test
  public void pieceTypes () {
    ChessDomain domain = new ChessDomain ();
    Scene board = ChessDomain.constructBoard ("......../......../....N.../......../.......R/R...k.../....P.../N...R.p.");

    Set<Square> bigPieces = domain.getBigPieces (board);
    assertEquals (6, bigPieces.size ());
    assertTrue (any (bigPieces, 7, 0));
    assertTrue (any (bigPieces, 7, 4));
    assertTrue (any (bigPieces, 5, 0));
    assertTrue (any (bigPieces, 5, 4));
    assertTrue (any (bigPieces, 4, 7));
    assertTrue (any (bigPieces, 2, 4));

    Set<Square> offensivePieces = domain.getOffensivePieces (board);
    assertEquals (3, offensivePieces.size ());
    assertTrue (any (offensivePieces, 7, 6));
    assertTrue (any (offensivePieces, 5, 4));
    assertTrue (any (offensivePieces, 2, 4));
  }

  // Unit Test
  @Test
  public void normalisationItemSquarePatternLists () {
    ListPattern lp = new ListPattern ();
    lp.add (new ItemSquarePattern ("k", 2, 3));
    lp.add (new ItemSquarePattern ("P", 4, 2));
    lp.add (new ItemSquarePattern ("q", 2, 3));

    assertEquals (3, lp.size ());
    assertEquals ("k", ((ItemSquarePattern)lp.getItem(0)).getItem ());
    assertEquals ("P", ((ItemSquarePattern)lp.getItem(1)).getItem ());
    assertEquals ("q", ((ItemSquarePattern)lp.getItem(2)).getItem ());

    ListPattern sorted = (new ChessDomain ()).normalise (lp);

    assertEquals (3, sorted.size ());
    assertEquals ("P", ((ItemSquarePattern)sorted.getItem(0)).getItem ());
    assertEquals ("k", ((ItemSquarePattern)sorted.getItem(1)).getItem ());
    assertEquals ("q", ((ItemSquarePattern)sorted.getItem(2)).getItem ());
  }

  // Unit Test
  @Test
  public void boardMoves () {
    Scene board1 = ChessDomain.constructBoard("......../......../......../....N.../......../......../......../........");
    Scene board2 = ChessDomain.constructBoard("......../......../......../....N.../......p./......../......../........");
    Scene board3 = ChessDomain.constructBoard("......../......../......../....N.../......P./......../......../........");
    Scene board4 = ChessDomain.constructBoard("N......./......../......../......../......../......../......../........");
    Scene board5 = ChessDomain.constructBoard("N...R.p./....P.../R...k.../.......R/......../......../......../........");

    assertFalse (board1.isEmpty (3, 4));

    ChessDomain d = new ChessDomain ();
    // check knight moves
    assertEquals (8, (d.proposeMovementFixations(board1, new Square(3, 4)).size ()));
    assertEquals (8, (d.proposeMovementFixations(board2, new Square(3, 4)).size ()));
    assertEquals (7, (d.proposeMovementFixations(board3, new Square(3, 4)).size ()));
    assertEquals (2, (d.proposeMovementFixations(board4, new Square(0, 0)).size ()));
    // check rook moves
    assertEquals (5, (d.proposeMovementFixations(board5, new Square(0, 4)).size ()));
    assertEquals (10, (d.proposeMovementFixations(board5, new Square(2, 0)).size ()));
    assertEquals (14, (d.proposeMovementFixations(board5, new Square(3, 7)).size ()));
  }
}
