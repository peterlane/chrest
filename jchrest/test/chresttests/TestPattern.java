package chresttests;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.Test;

import jchrest.lib.*;

public class TestPattern {

  // Process Test
  @Test
  public void numberMatches () {
    Pattern number1 = Pattern.makeNumber (1);
    Pattern number2 = Pattern.makeNumber (2);

    assertTrue (number1.equals(NumberPattern.create (1)));
    assertFalse (number1.equals (number2));
    assertTrue (number1.matches(NumberPattern.create (1)));
    assertFalse (number1.matches (number2));
  }

  // Process Test
  @Test
  public void stringMatches () {
    Pattern string1 = Pattern.makeString ("abc");
    Pattern string2 = Pattern.makeString ("def");

    assertTrue (string1.equals(StringPattern.create ("abc")));
    assertFalse (string1.equals(string2));
    assertTrue (string1.matches(StringPattern.create ("abc")));
    assertFalse (string1.matches(string2));
  }

  // Process Test
  @Test
  public void iosMatches () {
    Pattern ios1 = new ItemSquarePattern ("P", 2, 3);
    Pattern ios2 = new ItemSquarePattern ("P", 2, 3);
    Pattern ios3 = new ItemSquarePattern ("Q", 2, 3);

    assertTrue (ios1.equals(ios2));
    assertFalse (ios1.equals(ios3));
    assertTrue (ios1.matches(ios2));
    assertFalse (ios1.matches(ios3));
  }

  // Process Test
  @Test
  public void mixedMatches () {
    Pattern number = Pattern.makeNumber (1);
    Pattern string = Pattern.makeString ("abc");

    assertFalse (number.equals (string));
    assertFalse (number.matches (string));
    assertFalse (string.equals (number));
    assertFalse (string.matches (number));
  }

  // Process Test
  @Test
  public void listFinished () {
    ListPattern lp = Pattern.makeVisualList (new int[]{1,2,3,4});

    assertFalse (lp.isFinished ());
    lp.setFinished ();
    assertTrue (lp.isFinished ());
  }

  // Process Test
  @Test
  public void listEquality () {
    ListPattern lp1 = Pattern.makeVisualList (new int[]{1,2,3,4});
    ListPattern lp2 = Pattern.makeVisualList (new String[]{"a", "b", "c", "d", "e"});

    assertFalse (Pattern.makeVisualList (new int[]{}).equals (Pattern.makeVerbalList (new int[]{})));
    assertTrue (lp1.equals (Pattern.makeVisualList (new int[]{1,2,3,4})));

    assertFalse (lp1.equals (Pattern.makeVisualList (new int[]{1,2,3})));
    assertFalse (lp1.equals (Pattern.makeVisualList (new int[]{1,2,3,4,5})));
    assertFalse (lp1.equals (Pattern.makeVisualList (new int[]{1,2,3,5})));
    assertFalse (lp1.equals (lp2));

    ListPattern lp1_copy = Pattern.makeVisualList (new int[]{1,2,3,4});
    lp1_copy.setFinished ();

    assertFalse (lp1.equals (lp1_copy));
    lp1.setFinished ();
    assertTrue (lp1.equals (lp1_copy));
  }

  // Process Test
  @Test
  public void iosListEquality () {
    ListPattern lp1 = new ListPattern ();
    lp1.add (new ItemSquarePattern ("P", 2, 3));
    ListPattern lp2 = new ListPattern ();
    lp2.add (new ItemSquarePattern ("Q", 2, 3));
    ListPattern lp3 = new ListPattern ();
    lp3.add (new ItemSquarePattern ("P", 2, 3));

    assertTrue (lp1.equals (lp3));
    assertFalse (lp1.equals (lp2));
  }

  // Process Test
  @Test
  public void listMatches1 () {
    ListPattern lp1 = Pattern.makeVisualList (new int[]{1,2,3,4});
    ListPattern lp2 = Pattern.makeVisualList (new String[]{"a", "b", "c", "d", "e"});
    ListPattern lp3 = Pattern.makeVisualList (new int[]{1,2,3});

    assertFalse (Pattern.makeVisualList(new int[]{}).matches(Pattern.makeVerbalList(new int[]{})));
    assertTrue ((new ListPattern ()).matches (lp1));
    assertTrue (lp1.matches (lp1));
    assertFalse (lp1.matches (lp2));
    assertTrue (lp3.matches (lp1));
    assertFalse (lp1.matches (lp3));
  }

  // Process Test
  @Test
  public void listMatches2 () {
    ListPattern lp1 = Pattern.makeVisualList (new int[]{1,2,3,4});
    ListPattern prim1 = Pattern.makeVisualList (new int[]{1});

    assertTrue (prim1.matches (lp1));

    ListPattern prim1_clone = prim1.clone ();
    prim1_clone.setFinished ();

    assertTrue (prim1.matches (prim1_clone));
    assertFalse (prim1_clone.matches (prim1));
  }

  // Process Test
  @Test
  public void listMatches3 () {
    ListPattern empty = new ListPattern ();
    ListPattern lp1 = Pattern.makeVisualList (new int[]{1,2,3,4});

    assertTrue (empty.matches (lp1));
    empty.setFinished ();

    assertFalse (empty.matches (lp1));
    assertTrue (empty.matches (empty));
  }

  // Process Test
  @Test
  public void listPatternAppend () {
    ListPattern lp1 = Pattern.makeVisualList (new int[]{1,2,3,4});
    ListPattern lp2 = Pattern.makeVisualList (new int[]{1,2,3});
      
    assertTrue (lp1.equals(lp2.append (Pattern.makeVisualList (new int[]{4}))));
    assertTrue (lp1.equals(lp2.append (Pattern.makeNumber (4))));
  }

  // Process Test
  @Test
  public void listPatternRemove () {
    ListPattern lp1 = Pattern.makeVisualList (new int[]{1,2,3,4});
    ListPattern lp2 = Pattern.makeVisualList (new int[]{1,2,3});

    assertTrue (Pattern.makeVisualList(new int[]{4}).equals(lp1.remove(lp2)));
    assertTrue (Pattern.makeVisualList(new int[]{4}).equals(lp1.remove(Pattern.makeVisualList(new int[]{1,2,3,5,6}))));

    ListPattern pattern = lp2.clone ();
    pattern.setFinished ();

    assertTrue (pattern.remove(lp2).isEmpty ());
    assertTrue (pattern.remove(lp2).isFinished ());
  }
}
