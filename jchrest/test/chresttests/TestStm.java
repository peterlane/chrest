package chresttests;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.Test;

import jchrest.architecture.*;
import jchrest.lib.*;

public class TestStm {

  // Process Test
  // Ensure empty LTM results in root node being placed in STM 
  // on trying to recognise a pattern
  @Test
  public void stm1 () {
    Chrest model = new Chrest ();
    ListPattern patternA = Pattern.makeVisualList (new String[]{"A", "E", "F"});

    assertEquals (0, model.getVisualStm().getCount ());
    model.recognise (patternA);
    assertEquals (1, model.getVisualStm().getCount ());
    assertTrue (model.getLtmByModality(patternA) == model.getVisualStm().getItem (0));
  }

  // Process Test
  // Ensure learning an item results in STM containing two nodes:
  // the first is the new node, and the second is the root node
  @Test
  public void stm2 () {
    Chrest model = new Chrest ();
    ListPattern patternA = Pattern.makeVisualList (new String[]{"A", "E", "F"});

    assertEquals (0, model.getVisualStm().getCount ());
    model.recogniseAndLearn (patternA);
    assertEquals (2, model.getVisualStm().getCount ());
    assertTrue (model.getLtmByModality(patternA) == model.getVisualStm().getItem (1));
  }

  // Process Test
  // Ensure capacity of STM is respected
  @Test
  public void capacity () {
    Chrest model = new Chrest ();
    ListPattern patternA = Pattern.makeVisualList (new String[]{"A", "E", "F"});
    ListPattern patternB = Pattern.makeVisualList (new String[]{"B", "E", "F"});
    ListPattern patternC = Pattern.makeVisualList (new String[]{"C", "E", "F"});
    ListPattern patternD = Pattern.makeVisualList (new String[]{"D", "E", "F"});

    model.recogniseAndLearn (patternA);
    assertEquals (2, model.getVisualStm().getCount ());
    model.recogniseAndLearn (patternB);
    assertEquals (3, model.getVisualStm().getCount ());
    model.recogniseAndLearn (patternC);
    assertEquals (4, model.getVisualStm().getCount ());
    model.recogniseAndLearn (patternD);
    assertEquals (4, model.getVisualStm().getCount ());
    model.recogniseAndLearn (patternA);
    assertEquals (4, model.getVisualStm().getCount ());
  }

  // Process Test
  // Ensure a new item, if repeated, is correctly placed into STM
  @Test
  public void repeatedItems () {
    Chrest model = new Chrest ();
    ListPattern patternA = Pattern.makeVisualList (new String[]{"A", "E", "F"});
    ListPattern patternB = Pattern.makeVisualList (new String[]{"B", "E", "F"});
    ListPattern patternC = Pattern.makeVisualList (new String[]{"C", "E", "F"});
    ListPattern patternD = Pattern.makeVisualList (new String[]{"D", "E", "F"});

    model.recogniseAndLearn (patternA);
    Node node = model.getVisualStm().getItem (0);
    model.recogniseAndLearn (patternB);
    model.recogniseAndLearn (patternC);
    // check node occurs only once in the STM, and at the fourth place down
    assertTrue (3 <= model.getVisualStm().getCount ());
    assertEquals (node, model.getVisualStm().getItem (3));
    int count = 0;
    for (int i=0, n=model.getVisualStm().getSize(); i<n; i+=1) {
      if (node == model.getVisualStm().getItem (i)) {
        count += 1;
      }
    }
    assertEquals (1, count);

    // check newly recognised node occurs only once in the STM, and at first place
    model.recognise (patternA);
    assertEquals (node, model.getVisualStm().getItem (0));
    count = 0;
    for (int i=0, n=model.getVisualStm().getSize(); i<n; i+=1) {
      if (node == model.getVisualStm().getItem (i)) {
        count += 1;
      }
    }
    assertEquals (1, count);
  }
}
