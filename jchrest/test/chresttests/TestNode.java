package chresttests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.Test;

import jchrest.architecture.*;
import jchrest.lib.*;

public class TestNode {

  // Process Test
  @Test
  public void nodeInformation1 () {
    Node node = new Node (
        new Chrest (), 
        0, 
        Pattern.makeVisualList (new int[]{})
        );
    assertEquals (0, node.information ());
  }

  // Process Test
  @Test
  public void nodeInformation2 () {
    Node node = new Node(
        new Chrest (),
        0,      // reference makes this a root node
        Pattern.makeVisualList (new int[]{}),
        Pattern.makeVisualList (new String[]{"A", "B"})
        );

    assertEquals (0, node.information ()); // root nodes always 0
  }

  // Process Test
  @Test
  public void nodeInformation3 () {
    Node node = new Node(
        new Chrest (),
        Pattern.makeVisualList (new int[]{}),
        Pattern.makeVisualList (new String[]{"A", "B"})
        );

    assertEquals (2, node.information ());
  }
}
