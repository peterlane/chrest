This folder contains some scripts using lisp to develop and run CHREST 
models.  These scripts have all been tested using the ABCL lisp 
implementation.

Installing:
===========

You need a commandline environment with the java runtime.

1. Download chrest-lisp.zip from: http://chrest.info/jchrest.html 
2. Unzip the folder
3. Use run-abcl-script.bat or run-abcl-script.sh depending on your platform
   passing in the name of the file you want to run.

e.g. run demo-1 on Windows:

> run-abcl-script.bat demo-1.lisp

or the same on Linux/Mac:

$ sh run-abcl-script.sh demo-1.lisp

[Java is a little slow to start up, so wait for a few seconds for output.]

