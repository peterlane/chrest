# Demonstration 1 : jython script
#
# jython is available from https://jython.org/downloads.html
# 
# Either 
#   install jython using the installer, and run
#   > jython demo-1.py
#
# Or
#   download the standalone jython jar file, and run
#   > java -jar jython-standalone-2.7.0.jar demo-1.py 
#
# you will also need the chrest.jar file from 
# http://chrest.info/jchrest.html
#
# In this example, we create some instances of patterns,
# train a Chrest model with them, and then print out 
# and display what Chrest has learnt.
#

import sys
sys.path.append("chrest.jar") # assumes chrest.jar is in the same directory as the script

from jchrest.architecture import Chrest
from jchrest.gui import ChrestView
from jchrest.lib import Pattern
import java.lang.System
import java.io.PrintStream

# Create an instance of the Chrest model
model = Chrest()

# Create three pattern instances
pattern1 = Pattern.makeVisualList([1, 2, 3])
pattern2 = Pattern.makeVisualList([1, 3, 2])
pattern3 = Pattern.makeVisualList([2, 1, 3])

# store them in an array 
patterns = [pattern1, pattern2, pattern3]

# Train the model a few times on the patterns
for _ in range(4):
    for pat in patterns:
        model.recogniseAndLearn(pat)

# Display the results
print("Current model time: " + str(model.getClock()))
for pat in patterns:
    print("For pattern: " + pat.toString() + 
            " model retrieves " + model.recallPattern(pat).toString())

# And display the Model in a graphical view
ChrestView(model)
