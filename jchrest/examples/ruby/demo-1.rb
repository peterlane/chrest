# Demonstration 1 : jRuby script
#
# In this example, we create some instances of patterns,
# train a Chrest model with them, and then print out 
# and display what Chrest has learnt.
#

require "jchrest"

# Create an instance of the Chrest model
model = Chrest.new()

# Create three pattern instances
pattern1 = JChrest.make_number_pattern([1, 2, 3])
pattern2 = JChrest.make_number_pattern([1, 3, 2])
pattern3 = JChrest.make_number_pattern([2, 1, 3])

# store them in an array 
patterns = [pattern1, pattern2, pattern3]

# Train the model a few times on the patterns
4.times do 
  for pat in patterns
    model.recognise_and_learn(pat)
  end
end

# Display the results
puts "Current model time: #{model.clock}"
for pat in patterns
  print "For pattern: #{pat} model retrieves "
  puts "#{model.recall_pattern(pat)}"
end

# And display the Model in a graphical view
ChrestView.new(model)

