# Construct a large number of patterns and train CHREST.
#
# jruby demo-3.rb 100000 50
# -- produces a 90793 node network in 41 seconds on an i7 processor.
# jruby demo-3.rb 500000 50
# -- produces a 447186 node network in 3 minutes 39 seconds on an i7 processor.

require "jchrest"

# Create an instance of the Chrest model and a view
@@model = Chrest.new

Patterns = []

# create a large pool of patterns by constructing lists of random 
# numbers.

@@num_patterns = ARGV[0].to_i  # total number of patterns to make
@@max_length = ARGV[1].to_i    # maximum length of largest pattern

@@num_patterns.times do 
  pattern = []
  rand(@@max_length).times do 
    pattern << rand(10)
  end
  Patterns << JChrest.make_number_pattern(pattern)
end

for pattern in Patterns
  pattern.size.times do
    @@model.recogniseAndLearn(pattern)
  end
end

puts "Network contains #{@@model.ltmVisualSize} nodes"
