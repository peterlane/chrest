# Simple test that the model can handle a classification problem

require "jchrest"

model = Chrest.new

Weather = [ ["sunny", "hot", "high", "false", "no"],
            ["sunny", "hot", "high", "true", "no"],
            ["overcast", "hot", "high", "false", "yes"],
            ["rainy", "mild", "high", "false", "yes"],
            ["rainy", "cool", "normal", "false", "yes"],
            ["rainy", "cool", "normal", "true", "no"],
            ["overcast", "cool", "normal", "true", "yes"],
            ["sunny", "mild", "high", "false", "no"],
            ["sunny", "cool", "normal", "false", "yes"],
            ["rainy", "mild", "normal", "false", "yes"],
            ["sunny", "mild", "normal", "true", "yes"],
            ["overcast", "mild", "high", "true", "yes"],
            ["overcast", "hot", "normal", "false", "yes"],
            ["rainy", "mild", "high", "true", "no"] ]

def construct_patterns data
  data.collect do |item| 
    [JChrest.make_string_pattern(["outlook-#{item[0]}", 
                                  "temperature-#{item[1]}", 
                                  "humidity-#{item[2]}", 
                                  "windy-#{item[3]}"]) , 
    JChrest.make_name_pattern(item[4])]
  end
end

patterns = construct_patterns Weather

12.times do |i|
  for pair in patterns
    model.learn_and_name_patterns(pair[0], pair[1])
  end
  print "Performance on cycle #{i} is: "
  sum = 0
  for pair in patterns
    unless model.name_pattern(pair[0]).nil?
      sum += 1 if model.name_pattern(pair[0]) == pair[1]
    end
  end
  puts "#{sum} / #{patterns.length}"
end

view = ChrestView.new(nil, model)
