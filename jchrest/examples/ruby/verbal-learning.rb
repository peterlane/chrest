require "jchrest"

# This file demonstrates some results from the verbal-learning literature. 
# These results were key to the early success of EPAM as a model of human 
# learning.  They still provide convincing evidence that human memory is 
# indexed using a discrimination-net type of index.
#
# For comparison, results for humans, EPAM III and EPAM VI are shown, 
# taken from H. Richman, H.A. Simon, E.A. Feigenbaum, 'Simulations of 
# paired associate learning using EPAM VI', Working paper 553, 2002.
#
# 1. Effect of stimulus and response familiarisation

puts "Verbal Learning Experiments"
puts

# Experiment 1 : Effect of stimulus and response familiarisation
#
# Aim:
# Simulate result of Chenzoff (1962) that pre-familiarisation of responses 
# is more important than pre-familiarisation of stimuli.

# Stimulus-Response pairs
# (taken from EPAM-VI file, Underwood's low-low condition)
Pairs = JChrest.makeSRPairs [
  ["xin", "vod"],
  ["toq", "hax"], 
  ["wep", "cem"], 
  ["duf", "jyl"], 
  ["myd", "siq"], 
  ["ruk", "fec"], 
  ["nas", "baj"], 
  ["pov", "loz"], 
  ["kir", "zub"],
  ["gac", "yug"]
]

# train model on given patterns until it recalls them all
# -- timeout is the number of attempts made until it gives up
def train_model(model, patterns, timeout = 100)
  cycle = 0
  begin
    some_unknown = false
    cycle += 1
    patterns.each do |pattern|
      if model.recall_pattern(pattern).nil? or 
          (model.recall_pattern(pattern) != pattern)
        some_unknown = true
        model.recognise_and_learn pattern
      end
    end
  end while some_unknown and cycle <= timeout
  return cycle # return the number of training cycles required
end

def show_responses(model, pairs)
  pairs.each do |stimulus, response|
    print "For #{stimulus.toString}-#{response.toString} model returns "
    print "(#{model.recognise(stimulus).contents})#{model.recognise(stimulus).image.toString}-"
    if model.assocate_pattern(stimulus).nil?
      puts "nothing"
    else
      puts "#{model.associate_pattern(stimulus).toString}"
    end
  end
end

# train model on given SR pairs until it gets them right
# -- timeout is the number of attempts made until it gives up
# Returns the number of cycles required
def train_model_pairs(model, pairs, timeout = 100)
  errors = 0
  cycle = 0
  begin
    some_unknown = false
    cycle += 1
    pairs.each do |stimulus, response|
      if model.associate_pattern(stimulus).nil? or 
          (model.associate_pattern(stimulus) != response)
        some_unknown = true
        errors += 1
        model.associate_and_learn(stimulus, response)
      end
    end
  end while some_unknown and cycle <= timeout
  return cycle
end

def create_model
  model = Chrest.new
  return model
end

def train_u_u_condition
  model = create_model
  return train_model_pairs(model, Pairs)
end

def train_f_u_condition
  model = create_model
  train_model(model, Pairs.collect{|p| p[0]})
  return train_model_pairs(model, Pairs)
end

def train_u_f_condition
  model = create_model
  train_model(model, Pairs.collect{|p| p[1]})
  return train_model_pairs(model, Pairs)
end

def train_f_f_condition
  model = create_model
  train_model(model, Pairs.flatten)
  return train_model_pairs(model, Pairs)
end

uu = train_u_u_condition
fu = train_f_u_condition
uf = train_u_f_condition
ff = train_f_f_condition

puts "Experiment 1:"
puts "Effects of Stimulus and Response Familiarisation"
puts
puts "Table of Trials to learn list"
puts
puts "Condition  People  EPAM III  EPAM VI  CHREST"
puts "---------  ------  --------  -------  ------"
puts "   F-F      1.0     1.0       1.0      #{ff.to_f/ff}"
puts "   U-F      1.2     1.3       1.9      #{uf.to_f/ff}"
puts "   F-U      1.6     1.8       2.8      #{fu.to_f/ff}"
puts "   U-U      1.8     2.5       3.7      #{uu.to_f/ff}"
puts

