import jchrest.architecture.Chrest;
import jchrest.gui.ChrestView;
import jchrest.lib.ListPattern;
import jchrest.lib.Pattern;

/*
 * Demonstration 1 : Java program
 *
 * In this example, we create some instances of patterns,
 * train a CHREST model with them, and then print out and 
 * display what CHREST has learnt.
 */
public class Demo1 {
  public static void main (String[] args) {
    // create an instance of the CHREST model
    var model = new Chrest ();

    // create three pattern instances
    var pattern1 = Pattern.makeVisualList(new int[]{1, 2, 3});
    var pattern2 = Pattern.makeVisualList(new int[]{1, 3, 2});
    var pattern3 = Pattern.makeVisualList(new int[]{2, 1, 3});

    // and store them in an array
    ListPattern[] patterns = {pattern1, pattern2, pattern3};

    // Train the model a few times on the patterns
    for (int i = 0; i < 4; i += 1) {
      for (ListPattern pattern : patterns) {
        model.recogniseAndLearn (pattern);
      }
    }

    // Display the results
    System.out.println ("Current model time: " + model.getClock ());
    for (ListPattern pattern : patterns) {
      System.out.print ("For pattern: " + pattern + " model retrieves ");
      System.out.println ("" + model.recallPattern (pattern));
    }

    // And display the model in a graphical view
    new ChrestView (model);
  }
}

