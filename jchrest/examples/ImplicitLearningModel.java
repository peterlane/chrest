import jchrest.architecture.Chrest;
import jchrest.lib.Pattern;
import jchrest.lib.ListPattern;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/*
 * Implicit learning task, using Reber grammar
 *
 * Java implementation written by Peter Lane, December 2019.
 *
 * This script simulates the experiment described in Kennedy and Patterson (2012)
 * and demonstrates how CHREST captures patterns of behaviour
 * typical of implicit learning.
 *
 * Model and results published in:
 *
 * P.C.R. Lane and F. Gobet,
 * 'CHREST models of implicit learning and board game interpretation',
 * in J.Bach, B.Goertzel and M.Ikle (Eds.),
 * Proceedings of the Fifth Conference on Artificial General Intelligence,
 * LNAI 7716, pp. 148-157, 2012. (Berlin, Heidelberg: Springer-Verlag)
 *
 */
public class ImplicitLearningModel {
  public static void main (String[] args) {
    var model = new ImplicitLearningModel ();
    model.runCompleteExperiment ();
  }

  /*
   * Runs the experiment and displays results
   */
  void runCompleteExperiment () {
    int numberRuns = 100;

    int truePos = 0;
    int falseNeg = 0;
    int trueNeg = 0;
    int falsePos = 0;

    for (int i = 0; i < numberRuns; i += 1) {
      int[] result = runExperiment ();
      truePos += result[0];
      falseNeg += result[1];
      trueNeg += result[2];
      falsePos += result[3];
    }

    System.out.println(
        "\n" +
        "*** Results ***\n" +
        "\n" +
        "Valid Invalid (Actual)\n" +
        String.format("%5d %5d    | Valid    (True)\n", truePos, falseNeg) +
        String.format("%5d %5d    | Invalid\n", falsePos, trueNeg) +
        "\n" +
        "                    Human  ACT-R  CHREST\n" +
        "-----------------------------------------------------\n" +
        "              Hits: 33/44  34/44  " + formatResult(truePos, falseNeg, numberRuns) + "\n" +
        "Correct rejections: 36/44  39/44  " + formatResult(trueNeg, falsePos, numberRuns) + "\n" +
        "            Misses: 11/44  10/44  " + formatResult(falseNeg, truePos, numberRuns) + "\n" +
        "      False alarms:  8/44   5/44  " + formatResult(falsePos, trueNeg, numberRuns) + "\n" +
        "\n" +
        "(Human/ACT-R from Kennedy & Patterson (2012).  \n" +
        " CHREST results averaged over 100 runs.)\n"
        );
  }

  /*
   * Format results for display
   */
  String formatResult (int a, int b, int n) {
    return String.format ("%5.2f / %5.2f", (float)a / n, (float)(a+b) / n);
  }

  /* 
   * Runs a single experiment, returning results in an array
   */
  int[] runExperiment () {
    var dataset = createData ();
    var trainingSet = dataset.subList (0, 18);
    var testSet = dataset.subList(21, dataset.size ());
    var model = trainModel (trainingSet);

    var valid = testModel (model, testSet, true);
    var rnd = testModel (model, rndTestset (), false);

    return new int[]{valid[0], valid[1], rnd[0], rnd[1]};
  }

  /* 
   * Create and train a CHREST model
   */
  Chrest trainModel (List<ListPattern> patterns) {
    var model = new Chrest ();

    for (ListPattern pattern : patterns) {
      model.recogniseAndLearn (pattern);
      var node = model.recognise (pattern);

      var newPattern = pattern.remove (node.getContents ());

      while (!newPattern.equals (pattern)) {
        pattern = newPattern;
        model.recogniseAndLearn (pattern);
        node = model.recognise (pattern);
        newPattern = pattern.remove (node.getContents ());
      }
    }

    return model;
  }

  /*
   * Tests a model, returning results in array
   */
  int[] testModel (Chrest model, List<ListPattern> patterns, boolean isValid) {
    int trues = 0;
    int falses = 0;

    for (int i = 0; i < 2; i += 1) { // the tests are run twice
      for (ListPattern pattern : patterns) {
        var chunks = retrieveChunks (model, pattern);
        if (rejectChunks (chunks)) {
          if (isValid) { falses += 1; } else { trues += 1; }
        } else {
          if (isValid) { trues += 1; } else { falses += 1; }
        }
      }
    }

    return new int[]{trues, falses};
  }

  /*
   * Tests if chunks should be rejected.
   * Reject chunks if either:
   * 1. number of small chunks is too large    (lack of knowledge)
   * 2. number of chunks in total is too large (articulatory loop)
   */
  boolean rejectChunks (List<ListPattern> chunks) {
    // 1. number of small chunks is too large
    int numSmall = 0;
    for (ListPattern chunk : chunks) {
      if (chunk.size () <= 1) numSmall += 1;
    }
    if (numSmall > 2) return true;

    // 2. number of chunks is too large
    if (chunks.size () > 5) return true;

    // passed tests, so do not reject
    return false;
  }

  /* 
   * Returns a list of chunks, retrieved while recognising given pattern.
   */
  List<ListPattern> retrieveChunks (Chrest model, ListPattern pattern) {
    var chunks = new ArrayList<ListPattern> ();

    var node = model.recognise (pattern);
    chunks.add (node.getContents ());
    var newPattern = pattern.remove (node.getContents ());

    while (!newPattern.equals (pattern)) {
      pattern = newPattern;
      node = model.recognise (pattern);
      chunks.add (node.getContents ());
      newPattern = pattern.remove (node.getContents ());
    }

    return chunks;
  }

  /*
   * Create a random set of data.
   */
  List<ListPattern> rndTestset () {
    var result = new ArrayList<ListPattern> ();

    for (int i = 0; i < 7; i += 1) {
      for (int size : new int[]{6,7,8}) {
        result.add (Pattern.makeVerbalList (randomString(size).split("")));
      }
    }
    result.add (Pattern.makeVerbalList (randomString(7).split("")));

    return result;
  }

  /*
   * Create a single random string of length n.
   */
  String randomString (int n) {
    var r = new Random ();
    String[] letters = {"T", "X", "P", "V"};
    String result;

    do { // creates a random string that is _not_ in validStrings
      var word = "";

      while (word.length () < n-1) {
        word += letters[r.nextInt(letters.length)];
      }
      result = word + "S"; // end with 'S'
    } while (Arrays.stream(validStrings).anyMatch(result::equals));

    return result;
  }

  /*
   * Converts the list of valid strings into CHREST's listpattern format, 
   * and returns the list shuffled
   */
  List<ListPattern> createData () {
    var patterns = new ArrayList<ListPattern> ();

    for (String validString : validStrings) {
      patterns.add (Pattern.makeVerbalList (validString.split("")));
    }

    Collections.shuffle (patterns);
    return patterns;
  }

  private final String[] validStrings = "TTS,VXS,TPTS,VXXS,VXPS,TPPTS,TTVXS,VXXXS,VXXPS,TPPPTS,TPTVXS,TTVXXS,TTVXPS,VXXXXS,VXXXPS,VXPVXS,TPPPPTS,TPPTVXS,TPTVXXS,TPTVXPS,TTVXXXS,TTVXXPS,VXXXXXS,VXXXXPS,VXXPVXS,VXPVXXS,VXPVXPS,TPPPPPTS,TPPPTVXS,TPPTVXXS,TPPTVXPS,TPTVXXXS,TPTVXXPS,TTVXXXXS,TTVXXXPS,TTVXPVXS,VXXXXXXS,VXXXXXPS,VXXXPVXS,VXXPVXXS,VXXPVXPS,VXPVXXXS,VXPVXXPS".split (",");

}

