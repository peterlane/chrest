/*
 * Simple example of using CHREST in a chess recall experiment.
 *
 * 1. Load some chess data, and separate into train/test sets
 * 2. Train different CHREST models, to various net sizes
 * 3. Analyse the recall performance. Expected results:
 *    a. improved average recall performance over size
 *    b. increase in recall performance when using templates
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import jchrest.architecture.Chrest;
import jchrest.lib.ChessDomain;
import jchrest.lib.Scene;
import jchrest.lib.Scenes;

public class ChessRecallExample {

  public static void main (String[] args) {
    var cre = new ChessRecallExample ();
    cre.run ();
  }

  public void run () {
    /* ******************* CHANGE TO SUIT REQUIREMENTS ********************* */
    int[] sizes = { 200, 10_000, 50_000, 100_000 }; // different net sizes to test
    var pathname = "examples/sample-data/chess-positions.dat"; // Pathname, relative to call of program

    try {
      // 1. Load some chess data, and separate into train/test sets
      var br = new BufferedReader (new FileReader (pathname));
      br.readLine (); // reads and ignores the 'visual search' line

      var dataset = new ArrayList<Scene> ();
      Scenes.read (br).forEach (dataset::add);
      // -- randomly separate into training / test sets in 9:1 ratio
      int division = 9 * dataset.size () / 10;
      Collections.shuffle (dataset); 
      var trainingSet = dataset.subList (0, division);
      var testSet = dataset.subList (division, dataset.size ());

      // 2. Train different CHREST models, each to the target size
      var models = new Chrest[sizes.length];
      for (int i = 0; i < sizes.length; i += 1) {
        models[i] = createModel (trainingSet, 10, 20, sizes[i]);
      }

      // 3. Analyse the recall performance 
      // -- initially without templates
      evaluateModels (false, models, sizes, testSet);
      // -- then set templates and repeat analysis
      for (Chrest model : models) {
        model.constructTemplates ();
      }
      evaluateModels (true, models, sizes, testSet);

    } catch (IOException ioe) {
      System.err.println ("Error in file operation");
      ioe.printStackTrace ();
      System.exit (-1);
    }
  }

  /* 
   * Train a new Chrest model on given dataset, until it reaches the given net size.
   */
  Chrest createModel (List<Scene> dataset, int maxCycles, int numFixations, int maxSize) {
    var model = new Chrest ();
    model.setDomain (new ChessDomain ()); // this script is for chess datasets

    System.out.printf (" *** Learning net of size %d ***\n", maxSize);
    System.out.println ("Cycle  Visual LTM size  Avg depth  Avg image size");

    for (int cycle = 0; cycle < maxCycles; cycle += 1) {
      if (model.ltmVisualSize () > maxSize) break;
      for (Scene scene : dataset) {
        if (model.ltmVisualSize () > maxSize) break;
        model.learnScene (scene, numFixations);
      }
      System.out.printf ("%5d  %15d  %9.2f  %14.2f\n", 
          cycle+1, 
          model.ltmVisualSize (), 
          model.getVisualLtmAverageDepth (), 
          model.getVisualLtmAverageImageSize ());
    }

    return model;
  }

  /* 
   * Evaluate the models on the test set.
   * Results are output in a tabular format, reflecting if templates have been added.
   */
  public void evaluateModels (boolean hasTemplates, Chrest[] models, int[] sizes, List<Scene> testSet) {
    var results = new double[models.length][4];
    for (int i = 0; i < models.length; i += 1) {
      var result = evaluateModel (models[i], testSet);
      // -- store average values in overall table
      for (int j = 0; j < 4; j += 1) {
        results[i][j] = result[j] / testSet.size ();
      }
    }

    System.out.println ("");
    System.out.printf ("Average recall performance - %s templates\n", (hasTemplates ? "with" : "no"));
    System.out.println ("");

    System.out.print ("                      ");
    for (int size : sizes) {
      System.out.printf ("%6d ", size);
    }
    System.out.println ("");

    System.out.print ("                      ");
    for (int size : sizes) {
      System.out.print ("------ ");
    }
    System.out.println ("");

    showResults (new String[]{"Recall", "Precision", "Omission", "Commission"}, results);

    if (hasTemplates) {
      System.out.print (" Number of templates: ");
      for (Chrest model : models) {
        System.out.printf ("%6d ", model.countTemplates ());
      }
      System.out.println ();
    }
  }

  /*
   * Use the test set to evaluate the given model. 
   * Returns an array of average recall/precision/omission/commission
   */
  double[] evaluateModel (Chrest model, List<Scene> testSet) {
    double[] result = {0.0, 0.0, 0.0, 0.0};

    for (Scene scene : testSet) {
      var recalled = model.scanScene (scene, 20);
      result[0] += scene.computeRecall (recalled);
      result[1] += scene.computePrecision (recalled);
      result[2] += scene.computeErrorsOfOmission (recalled);
      result[3] += scene.computeErrorsOfCommission (recalled);
    }

    return result;
  }

  /*
   * Display rows of the table, using titles as row labels
   */
  void showResults (String[] titles, double[][] results) {
    for (int row = 0; row < titles.length; row += 1) {
      System.out.printf ("%20s:", titles[row]);
      for (double[] result : results) {
        System.out.printf (" %6.2f", result[row]);
      }
      System.out.println ("");
    }
  }

}

