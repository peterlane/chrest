# Rakefile to support building, testing and releasing jCHREST.
#
# To build the user-guide, install:
# -- asciidoctor-pdf: `gem install asciidoctor-pdf` 
# -- rouge: `gem install rouge` 

VERSION='4.1.1' # Current version - increase this appropriately with each release

# Amend the following list to include or update library files.
LIB_URLS = [
  'https://repo1.maven.org/maven2/org/knowm/xchart/xchart/3.6.2/xchart-3.6.2.jar',
  'https://repo1.maven.org/maven2/org/junit/jupiter/junit-jupiter-api/5.6.1/junit-jupiter-api-5.6.1.jar',
  'https://repo1.maven.org/maven2/org/apiguardian/apiguardian-api/1.1.0/apiguardian-api-1.1.0.jar',
  'https://repo1.maven.org/maven2/org/junit/platform/junit-platform-console-standalone/1.6.1/junit-platform-console-standalone-1.6.1.jar'
]

desc 'download the required library jar files'
task :get_jars do
  Dir.chdir('jchrest/lib') do
    LIB_URLS.each { |url| `wget #{url}` }
  end
end

# ---------------------------------------------------------------------------
# Paths to libraries -- assumes jars available in lib/

CWD = File.expand_path(File.dirname(__FILE__)) 
LIB = File.join(CWD, 'jchrest/lib/')                 
CLASSES = File.join(CWD, 'jchrest/classes/')

def get_jars regexp # returns those jar names which match regexp
  Dir.entries(LIB).find_all {|file| file.end_with?(".jar") and file =~ regexp} - ['.', '..']
end

TEST_JARS = get_jars /jupiter|guardian/         # for junit test libraries
RUNNER = get_jars(/console/)[0]                 # for junit runner library
JARS = get_jars(/\w*/) - TEST_JARS - [RUNNER]   # all remaining jars

def make_classpath(with_tests=false)
  jars = JARS
  jars += TEST_JARS if with_tests
  jars.collect {|jar| "#{LIB}#{jar}"}.join(":")
end


# ---------------------------------------------------------------------------
# tasks

desc 'remove any created artifacts'
task :clean do
  Dir.chdir('jchrest') do
    if Dir.exist?("classes")
      rm_rf("classes")
    end
    if File.exist?("chrest.jar")
      rm("chrest.jar")
    end
    Dir.chdir('lib') do
      Dir.entries('.').each do |file|
        rm(file) if file.end_with?(".jar")
      end
    end
  end
  if Dir.exist?("doc/api");
    rm_rf("doc/api")
  end
  if File.exist?("doc/user-guide/user-guide.pdf")
    rm("doc/user-guide/user-guide.pdf")
  end
end

desc 'compile the main source code'
task :compile do
  Dir.chdir('jchrest') do
    if Dir.exist?("classes")
      rm_rf("classes")
    end
    Dir.chdir('src') do
      sh "javac --release 11 -cp .:#{make_classpath()} -d #{CLASSES} **/**/*.java"
    end
  end
end

desc 'compile and run tests - assumes compile task has been run'
task :test do
  Dir.chdir('jchrest') do
    Dir.chdir('test') do
      sh "javac -cp .:#{CLASSES}:#{make_classpath(true)} -d #{CLASSES} **/*.java"
    end
    Dir.chdir(CLASSES) do
      sh "java -jar #{LIB}#{RUNNER} -cp .:#{make_classpath()} --scan-classpath=."
    end
  end
end

desc 'build the user guide'
task :guide do
  Dir.chdir('doc/user-guide') do
    sh 'asciidoctor-pdf -a numbered -a source-highlighter=rouge user-guide.txt'
  end
end

desc 'make jar file'
task :jar => [:compile] do
  Dir.chdir('jchrest') do
    sh "cp -r src/jchrest/gui/icons classes/jchrest/gui"
    sh "cp lib/orange-chrest-logo.png classes"
    Dir.chdir('classes') do
      puts JARS.join(', ')
      JARS.each { |jar| `jar xf ../lib/#{jar}` }
      File.open("manifest.mf", "w") do |f|
        f.puts "Main-Class: jchrest.gui.Shell"
        f.puts "SplashScreen-Image: orange-chrest-logo.png"
      end
      sh "jar cfm chrest.jar manifest.mf ."
    end
    sh "mv classes/chrest.jar ."
  end
end

desc 'build the javadoc files'
task :javadoc do
  if Dir.exist?("doc/api")
    rm_rf("doc/api")
  end
  sh "mkdir -p doc/api"
  Dir.chdir('jchrest/src') do
    sh "javadoc -windowtitle 'Chrest #{VERSION} Documentation' -doctitle 'Chrest #{VERSION}' -overview jchrest/overview.html -cp .:#{make_classpath()} -d ../../doc/api `find -name \"*.java\"`"
  end
end

desc 'create a zip file for release'
task :release => [:jar, :javadoc, :guide] do
  if Dir.exist?('release')
    rm_rf('release')
  end
  # documentation
  sh 'mkdir -p release/chrest/documentation'
  cp 'doc/apache-license.txt', 'release/chrest/documentation'
  cp 'doc/mit-license.txt', 'release/chrest/documentation'
  cp 'doc/user-guide/user-guide.pdf', 'release/chrest/documentation'
  cp 'jchrest/chrest.jar', 'release/chrest'
  sh 'cp -r doc/api/ release/chrest/documentation/javadoc'
  File.open('release/chrest/README.txt', 'w') do |f|
    f.puts 'See documentation/user-guide.pdf for information on running and using CHREST.'
  end  
  # examples
  sh 'mkdir release/chrest/examples'
  sh 'cp jchrest/examples/*.java release/chrest/examples'
  sh 'cp -r jchrest/examples/lisp release/chrest/examples'
  sh 'cp -r jchrest/examples/python release/chrest/examples'
  sh 'cp -r jchrest/examples/ruby release/chrest/examples'
  sh 'cp -r jchrest/examples/sample-data release/chrest/examples'
  # start-up scripts
  File.open('release/chrest/start-chrest.sh', 'w') do |f|
    f.puts 'java -Xmx1000M -jar chrest.jar'
  end
  File.open('release/chrest/start-chrest.bat', 'w') do |f|
    f.puts 'start javaw -Xmx1000M -jar chrest.jar'
  end
  # create zip file
  Dir.chdir('release') do
    sh "zip -FS -r ../chrest-#{VERSION}.zip chrest"
  end
  # clean up
  rm_rf('release')
end


