# CHREST 

This software project contains materials relating to the CHREST cognitive 
architecture. More information on CHREST is available at 
[http://chrest.info/](http://chrest.info/).

## Contents

If you have downloaded the source code for this project, you will be looking 
at this file along with the following:

* `doc/` - project licenses and source text for the user-guide.
* `jchrest/` - the Java language implementation of CHREST.
* `jchrest-gem/` - a jRuby library which includes `jchrest`, used for model 
  development.
* `jchrest-gem-chess/` - a jRuby library which extends `jchrest-gem` with 
  features to support the classification and interpretation of chess positions.
* `results/` - source code behind some published results using jCHREST.
* `rakefile.rb` - a Ruby-based rakefile to help compile/test/release `jchrest`.

### Requirements

Software requirements:

* [Java](https://jdk.java.net/) - at least version 11, to compile and run CHREST.
* [Ruby](https://www.ruby-lang.org/en/) - to use the rakefile.
* [jRuby](https://www.jruby.org/) - to build and use the gems.
* [asciidoc](https://asciidoc.org/) and [pdftk](https://pdftk.en.softonic.com/) - to build the user-guide.

### Rakefile + jCHREST

The rakefile provides several tasks to help build and test jCHREST. 

```
$ rake -T
rake clean     # remove any created artifacts
rake compile   # compile the main source code
rake get_jars  # download the required library jar files
rake guide     # build the user guide
rake jar       # make jar file
rake javadoc   # build the javadoc files
rake release   # create a zip file for release
rake test      # compile and run tests - assumes compile task has been run
```

Note that the rake file has only been tested on Linux using bash.

### Gems

The gems can be built from their respective gemspec files, but before 
building `jchrest-gem`, build and copy `jchrest.jar` to `jchrest-gem/lib`.

## License 

The CHREST project files are released under the MIT License.

[XChart](https://knowm.org/open-source/XChart/) is used to provide the graphs
within CHREST, and the distribution includes XChart under the terms of the
[Apache 2.0 open source license](https://opensource.org/licenses/Apache-2.0).

(Full licenses are in the doc/ folder.)


## Contributors 

This repository is maintained by Peter Lane.

The original design and ideas behind CHREST were created by [Fernand Gobet](http://chrest.info/fg/home.htm).

The emotions module was created by [Marvin Schiller](http://www.marvin-schiller.de/).

