Gem::Specification.new do |s|
  s.name = "jchrest-chess"
  s.platform = 'java'
  s.author = "Peter Lane"
  s.version = "1.0.0"
  s.email = "peterlane@gmx.com"
  s.summary = "Extends the jCHREST library with support for classification and interpretation of chess positions."
  s.license = "MIT"
  s.description = <<-END
  END
  s.files = [
    "LICENSE.rdoc",
    "README.rdoc",
    "lib/jchrest-chess.rb",
    "lib/jchrest-chess/chrest-extensions.rb",
    "lib/jchrest-chess/feature-store.rb",
    "lib/jchrest-chess/positions.rb"
  ]
  s.require_path = "lib"
  s.extra_rdoc_files = ['README.rdoc', 'LICENSE.rdoc']
  s.rdoc_options << '-m' << 'README.rdoc'

  s.add_runtime_dependency 'chess_data', '~> 1.0'
  s.add_runtime_dependency 'jchrest', '~> 1.0'
end


