# Classes to handle Position and collections of Position objects
#
# Peter Lane, 3rd May 2012
#  updated 15/12/19
#  updated to use 'chess-data' and its Board class, 13/11/20

require "csv"

# Extends the
# ChessData::Board[https://www.rubydoc.info/gems/chess-data/ChessData/Board]
# representation of a chess board with
#
# * an attribute `features`, to store the semantic information related to this
#   position, and
# * a method to convert the board position into a Scene instance.
#
class ChessData::Board
  # Returns position converted into a jchrest Scene.
  def as_scene
    scene = Scene.new("#{@name}-scene", 8, 8)
    8.times do |i|
      8.times do |j|
        square = "#{"abcdefgh"[j]}#{i+1}"
        piece = self[square]
        piece = "." if piece.nil? # empty square represented as a full-stop.
        scene.setItem(7-i, j, piece)
      end
    end
    return scene
  end

  # Holds the features related to this board position.
  attr_accessor :features
end

# Store a set of chess positions and their related features
class Positions
  # Holds a board position and its associated features.
  ChessBoard = Struct.new(:position, :features)

  # Retrieve positions from given filename. 
  # File format is CSV, "FEN", "feature1 feature2 ..."
  def Positions.from_file filename
    name = File.basename(filename).delete File.extname(filename)
    data = [] # holds a list of ChessBoard instances

    csv_data = CSV.parse(File.read(filename), headers: false)
    csv_data.each do |line|
      data << ChessBoard.new(
        ChessData::Board.from_fen(line[0]),
        line[1].split(" ").collect(&:downcase)
      )
    end

    Positions.new(name, data)
  end

  # The name of this set of positions (usually the filename)
  attr_reader :name
  # A list of ChessBoard instances, which pair up board positions and features.
  attr_reader :data

  # Create an instance of Positions from a given name and list of ChessBoard
  # instances.
  def initialize(name, data)
    @name = name
    @data = data
  end

  # Returns all the separate features.
  def all_features
    @data.map{|b| b.features}.flatten.uniq.sort
  end

  # Returns a count of how often a feature appears in given set of positions.
  #
  def count_occurrences(feature)
    count = 0
    self.each_position_and_features do |posn, features|
      count += 1 if features.include?(feature)
    end
    return count
  end

  # Iterator for the collection of positions.
  def each
    @data.each do |posn|
      yield posn
    end
  end

  # Iterator on positions alone.
  def each_position
    @data.each do |posn|
      yield posn.position
    end
  end

  # Iterator on positions and features.
  def each_position_and_features
    @data.size.times do |i|
      yield position(i), features(i)
    end
  end

  # Returns features at index i.
  def features i
    @data[i].features
  end

  # Returns a new instance of Positions made by combining this and the given
  # set of Positions.
  def merge positions
    Positions.new("#{@name}+#{positions.name}", @data + positions.data)
  end

  # Returns position at index i.
  def position i
    @data[i].position
  end

  # Return number of instances.
  def size
    @data.size
  end

  # Returns two new Positions objects with data split randomly in given proportion.
  #
  # This is used to construct train/test datasets.
  #
  def split propn
    data1 = []
    data2 = []
    data.each do |datum|
      if rand <= propn
        data1 << datum
      else
        data2 << datum
      end
    end
    return [
      Positions.new("#{name}<=#{propn}", data1),
      Positions.new("#{name}>#{propn}", data2)
    ]
  end

end

