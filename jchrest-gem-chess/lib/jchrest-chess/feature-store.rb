# A feature store is used to store the features associated with a given node.
# A node ID is used to retrieve a map of features to a frequency count.
#
class FeatureStore
  # Creates an empty feature store.
  def initialize
    @map = {}
  end

  # Features are associated with a given id, and a count is 
  # maintained of how often the feature has been added.
  #
  def add_feature(id, feature)
    unless @map.has_key?(id)
      @map[id] = {}
    end
    if @map[id].has_key?(feature)
      @map[id][feature] += 1
    else
      @map[id][feature] = 1
    end
  end

  # Retrieves the features related to the given id.
  #
  def get_features id 
    @map[id]
  end
end


