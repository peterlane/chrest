# Some additional routines for managing the chess experiments, 
# and to develop a CHREST model for interpretation.
# -- once clarified, these mechanisms will be moved into the 
#    Java base.
#
# Written by Peter Lane, May-July 2012.

# Extend the Chrest class with methods to build models which can map 
# chunks/templates to features, and use these to provide interpretations or 
# classifications of chess positions.
#
class Chrest
  # Create a model, training it on the given dataset with given parameters.
  # 
  # This method displays some information about its progress per learning 
  # cycle.
  #
  # dataset:: an instance of Positions, containing the board positions and 
  #           features to learn from.
  # cycles:: the number of times the dataset to be presented to the model 
  #          before learning is stopped.
  # num_fixations:: the number of fixations for the model to make on each chess 
  #                 position.
  # cap:: maximum size of the model's LTM - learning stops if the LTM size 
  #       reaches the cap.
  #
  def Chrest.create_model_with_features(dataset, cycles, num_fixations = 20, cap = 1_000_000)
    model = Chrest.new
    model.store = FeatureStore.new
    model.set_domain(ChessDomain.new)
    puts "Cycle LTM-size Contents Images"
    puts "                 size    size "
    cycles.times do |cycle|
      dataset.each do |posn|
        break if model.ltm_visual_size > cap
        model.learn_scene_and_feature(posn.position.as_scene, posn.features, num_fixations)
      end
      puts "#{"%5d" % (cycle+1)} #{"%8d" % model.ltm_visual_size} #{"%8.2f" % model.get_visual_ltm_average_depth} #{"%6.2f" % model.get_visual_ltm_average_image_size}"
    end

    return model
  end

  # Classify scene based on the seen nodes.
  #
  # The classification process returns a feature based on
  #
  # * the number of seen nodes the feature occurs in, and 
  # * then the overall frequency count of that feature.
  # 
  # posn:: an instance of ChessData::Board.
  # num_fixations:: optional number of eye fixations to make when scanning the 
  #                 scene.
  #
  def classify_scene(posn, num_fixations = 20)
    key_counts, options = process_scene(posn, num_fixations)
    # which has the most counts?
    classn = "NONE"
    options.each do |key|
      if classn == "NONE" 
        classn = key
      else
        if key_counts[key] > key_counts[classn]
          classn = key
        end
      end
    end

    return classn 
  end

  # Interpret scene based on the seen nodes.
  #
  # The interpretation process returns a list of those features
  # which occur most often in the seen nodes.
  #
  # posn:: an instance of ChessData::Board.
  # num_fixations:: optional number of eye fixations to make when scanning the 
  #                 scene.
  #
  def interpret_scene(posn, num_fixations = 20)
    _, options = process_scene(posn, num_fixations)
    options
  end

  # Extends the CHREST scene learning process by associating features with 
  # the nodes retrieved into visual STM.
  #
  # scene:: an instance of Java Scene
  # features:: a list of features to associate with the scene
  # num_fixations:: optional number of eye fixations to make when scanning the 
  #                 scene.
  #
  def learn_scene_and_feature(scene, features, num_fixations=20)
    # learn the scene
    learn_scene(scene, num_fixations)
    # learn the features as verbal patterns
    # for each chunk in STM, link to the verbal pattern
    visual_stm.count.times do |i|
      next unless visual_stm.get_item(i).image.size > 0
      features.each do |feature|
        if chunk_matches_feature(visual_stm.get_item(i).contents, feature)
          @store.add_feature visual_stm.get_item(i).reference, feature
        end
      end
    end
  end

  # A FeatureStore, holding the mapping between node IDs and feature 
  # frequencies.
  attr_accessor :store

  private

  # Scan given scene and preserve a list of the nodes which are retrieved
  # by the model in an internal list of "seen nodes".
  #
  # scene:: an instance of Java Scene
  # num_fixations:: optional number of eye fixations to make when scanning the 
  #                 scene.
  #
  def scan_scene(scene, num_fixations=20)
    @seen_nodes = []
    perceiver.scene = scene
    perceiver.start num_fixations
    num_fixations.times do
      perceiver.move_eye
      visual_stm.count.times do |i|
        unless @seen_nodes.include? visual_stm.get_item(i)
          @seen_nodes << visual_stm.get_item(i)
        end
      end
    end
  end

  def process_scene(posn, num_fixations = 20)
    scan_scene(posn.as_scene, num_fixations)

    key_occurrences = {}
    key_counts = {}
    @seen_nodes.each do |node|
      map = @store.get_features(node.reference)
      next if map.nil?
      map.each_key do |key|
        if key_occurrences.has_key? key
          key_occurrences[key] += 1
        else
          key_occurrences[key] = 1
        end
        if key_counts.has_key? key
          key_counts[key] += map[key]
        else
          key_counts[key] = map[key]
        end
      end
    end
    # which has most occurrences?
    most_occurrences = ""
    key_occurrences.each_key do |key|
      if most_occurrences == ""
        most_occurrences = key
      else
        if key_occurrences[key] > key_occurrences[most_occurrences]
          most_occurrences = key
        end
      end
    end
    # how many have the most occurrences?
    options = []
    key_occurrences.each_key do |key|
      if key_occurrences[key] == key_occurrences[most_occurrences]
        options << key
      end
    end

    return key_counts, options 
  end

  # Check if the named item occurs in the pattern.
  def chunk_contains_item(pattern, item)
    pattern.size.times do |i|
      next unless pattern.getItem(i).class == Java::JchrestLib::ItemSquarePattern
      if pattern.getItem(i).item == item
        return true
      end
    end
    return false
  end

  # Check if the named column occurs in the pattern.
  def chunk_contains_column(pattern, column)
    pattern.size.times do |i|
      next unless pattern.getItem(i).class == Java::JchrestLib::ItemSquarePattern
      if pattern.getItem(i).column == column
        return true
      end
    end
    return false
  end

  # find max/min row/col, and check given column/row within bounds
  def chunk_covers_square(pattern, column, row)
    max_row = -1
    min_row = 10
    max_col = -1
    min_col = 10
    pattern.size.times do |i|
      next unless pattern.getItem(i).class == Java::JchrestLib::ItemSquarePattern
      c = pattern.getItem(i).column
      max_col = c if c > max_col
      min_col = c if c < min_col
      r = pattern.getItem(i).row
      max_row = r if r > max_row
      min_row = r if r < min_row
    end
    return false if column > max_col
    return false if column < min_col
    return false if row > max_row
    return false if row < min_row
    return true
  end

  # Check if feature is a match to given pattern by analysing the words 
  # it contains. For example, if the feature mentions a white rook, then 
  # the pattern must contain a white rook. This method checks for named 
  # pieces, files and squares.
  #
  # pattern:: a chunk of chess pieces, as stored by CHREST.
  # feature:: the name of a feature possibly associated with this chunk.
  #
  def chunk_matches_feature(pattern, feature)
    # -- check given piece mentioned in pattern
    piece_checks = { 
      ["white", "king"] => "K",
      ["white", "queen"] => "Q",
      ["white", "rook"] => "R",
      ["white", "bishop"] => "B",
      ["white", "knight"] => "N",
      ["white", "pawn"] => "P",
      ["black", "king"] => "k",
      ["black", "queen"] => "q",
      ["black", "rook"] => "r",
      ["black", "bishop"] => "b",
      ["black", "knight"] => "n",
      ["black", "pawn"] => "p"
    }
    piece_checks.each_pair do |key, item|
      if key.all?{|k| feature.include?(k)}
        return false unless chunk_contains_item(pattern, item)
      end
    end
    # -- check given file mentioned in pattern
    [["a", 1],["b", 2], ["c", 3], ["d", 4], ["e", 5], ["f", 6], ["g", 7], ["h", 8]].each do |file, index|
      if feature.include?("#{file}file")
        return false unless chunk_contains_column(pattern, index)
      end
    end
    # -- check given square mentioned in pattern
    [["a", 1],["b", 2], ["c", 3], ["d", 4], ["e", 5], ["f", 6], ["g", 7], ["h", 8]].each do |file, col_index|
      [[8, 1], [7, 2], [6, 3], [5, 4], [4, 5], [3, 6], [2, 7], [1, 8]].each do |row, row_index|
        if feature.include?("#{file}#{row}")
          return false unless chunk_covers_square(pattern, col_index, row_index)
        end
      end
    end
    return true
  end

end


