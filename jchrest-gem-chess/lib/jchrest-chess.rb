require "jchrest"
require "chess_data"

require "jchrest-chess/chrest-extensions.rb"
require "jchrest-chess/feature-store.rb"
require "jchrest-chess/positions.rb"
