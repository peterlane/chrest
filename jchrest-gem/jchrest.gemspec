Gem::Specification.new do |s|
  s.name = "jchrest"
  s.platform = 'java'
  s.author = "Peter Lane"
  s.version = "1.0.3"
  s.email = "peterlane@gmx.com"
  s.homepage = "http://chrest.info"
  s.summary = "CHREST packaged for use with JRuby"
  s.license = "MIT"
  s.files = [
    "lib/chrest.jar",
    "LICENSE.txt",
    "README.rdoc"
  ] + Dir["lib/*.rb"]
  s.require_path = "lib"
  s.extra_rdoc_files = ['README.rdoc', 'LICENSE.txt']
  s.rdoc_options << '-m' << 'README.rdoc'
end
