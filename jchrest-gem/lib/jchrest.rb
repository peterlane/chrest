
require "java"
require "chrest.jar"

java_import [
  "jchrest.architecture.Chrest", 
  "jchrest.gui.ChrestView",       # imported here for convenience of scripts
  "jchrest.lib.ChessDomain",
  "jchrest.lib.Pattern",
  "jchrest.lib.Scene",
  "jchrest.lib.Scenes"
]

#
# Using Java code from jruby is mostly straightforward. However, constructing an 
# array of a required Java type means that Java type must be written out.
#
# This module provides a set of methods to ease the construction of CHREST's 
# pattern types.
#
# For example, to make the pattern < "a" "b" "c" >, call
#   JChrest.make_pattern_from_string "abc"
#
module JChrest

  # Convenience function which splits a ruby string into separate characters
  # and builds a visual list pattern, using each separate character as a token/primitive.
  # 
  #   JChrest.make_pattern_from_string("abc").to_s          # => "< a b c >"
  #   JChrest.make_pattern_from_string("abc").modality.to_s # => "VISUAL" 
  #
  # str:: containing the characters to use as a list
  # 
  def self.make_pattern_from_string str
    Pattern.makeVisualList(str.split("").to_java(:String))
  end

  # Convenience function which splits a ruby string into separate characters
  # and builds a verbal list pattern, using each separate character as a token/primitive.
  #
  #   JChrest.make_verbal_pattern_from_string("abc").to_s          # => "< a b c >"
  #   JChrest.make_verbal_pattern_from_string("abc").modality.to_s # => "VERBAL" 
  #
  # str:: containing the characters to use as a list
  #
  def self.make_verbal_pattern_from_string str
    Pattern.makeVerbalList(str.split("").to_java(:String))
  end

  # Convenience function to build a pair of list patterns for stimulus-response experiments.
  #
  #   JChrest.makeSRPairs([["a","b"], ["b","c"]]) 
  #
  # pairs:: with each pair being a (stimulus, response) pair
  #
  def self.makeSRPairs pairs
    pairs.collect do |s, r| 
      [make_pattern_from_string(s), make_pattern_from_string(r)]
    end
  end

  # Convenience function to convert a ruby array of integers into 
  # a visual ListPattern. Each integer is used as a single token/primitive.
  #
  #   JChrest.make_number_pattern([1,2,3]).to_s #  => "< 1 2 3 >" 
  #
  # array:: an array of integers
  #
  def self.make_number_pattern array
    Pattern.makeVisualList(array.to_java(:int))
  end

  # Convenience function to convert a ruby array of Strings into 
  # a visual ListPattern. Each string is used as a single token/primitive.
  #
  #   JChrest.make_string_pattern(["ab", "bc"]).to_s          # => "< ab bc >" 
  #   JChrest.make_string_pattern(["ab", "bc"]).modality.to_s # => "VISUAL" 
  #
  # array:: array of strings
  #
  def self.make_string_pattern array
    Pattern.makeVisualList(array.to_java(:String))
  end

  # Convenience function to convert a ruby String into 
  # a verbal ListPattern. The string is used as a single token/primitive.
  #
  #   JChrest.make_name_pattern("abc").to_s          # => "< abc >" 
  #   JChrest.make_name_pattern("abc").modality.to_s # => "VERBAL" 
  #
  # str:: is the string to use as the named pattern
  #
  def self.make_name_pattern str
    Pattern.makeVerbalList([str].to_java(:String))
  end

end

# Extension method to the Java Scenes class.
#
class Scenes

  # Reads in a set of Scene descriptions, and returns an instance of Scenes.
  # filename:: containing text description of scenes
  def Scenes.read_from_file filename
    scenes = []
    begin 
      fbr = java.io.BufferedReader.new(java.io.FileReader.new(filename))
      fbr.read_line # reads and ignores the 'visual search' line
      scenes = Scenes.read(fbr)
    rescue java.io.IOException 
      puts "Error !"
    end
    return scenes
  end
end

# Extension method to the Java Chrest class.
#
class Chrest

  # Returns a new CHREST model. The model is trained for the given 
  # number of training _cycles_. In each cycle, the model scans 
  # each _scene_ in turn, allowing the eye to move the given number 
  # of fixations. 
  #
  # scenes:: with which to train the model
  # cycles:: number of cycles to train the model
  # fixations:: number of fixations for the eye to make per scene
  # cap:: an upper bound on the discrimination net
  def Chrest.create_model(scenes, cycles, fixations = 20, cap = 1_000_000)
    model = Chrest.new

    model.set_domain(ChessDomain.new)
    puts "Learning: "
    puts "Cycle  Visual LTM size  Avg depth  Avg image size"
    cycles.times do |cycle|
      break if model.ltm_visual_size > cap 
      scenes.size.times do |i|
        break if model.ltm_visual_size > cap 
        model.learn_scene(scenes.get(i), fixations)
      end
      puts "#{"%5d" % (cycle+1)}  #{"%15d" % model.ltm_visual_size}  #{"%9.2f" % model.get_visual_ltm_average_depth}  #{"%14.2f" % model.get_visual_ltm_average_image_size}"
    end

    return model
  end
end

# Extension method to the Java Scene class.
#
class Scene

  # Displays this scene onto standard output in a readable format
  def display
    height.times do |h|
      width.times do |w|
        print get_item(h, w)
      end
      puts
    end
  end

end
