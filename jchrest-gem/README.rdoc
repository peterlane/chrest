= jChrest

home:: http://chrest.info
source:: https:/notabug.org/peterlane/jchrest/releases/

== Description

The CHREST[http://chrest.info/jchrest.html] cognitive architecture bundled
as a jRuby library. Models and simulations can be developed in ruby, and
extensions made to the architecture.

This library consists of two parts:

1. Extensions to some Java-based classes, notably +Chrest+, +Scene+ and +Scenes+.
   These extensions are mainly methods to aid in creating models or scenes from files.
2. The module JChrest contains several methods to make it easier to create pattern 
   types in JRuby.

== Examples

Some examples are included with the main implementation of 
CHREST[http://chrest.info/jchrest.html], but may be downloaded separately:
(chrest-ruby.zip)[http://chrest.info/downloads/chrest-ruby.zip].

Larger examples:

* Experiments in chess interpretation and categorisation using CHREST:
  description[http://chrest.info/software.html#chessboard] - 
  source[https://notabug.org/peterlane/chrest-chess/releases].

== Example code

Example of creating some pattern instances, training a Chrest model and
displaying the results.

  require "jchrest"
  
  # Create an instance of the Chrest model
  model = Chrest.new()
  
  # Create three pattern instances
  pattern1 = JChrest.make_number_pattern([1, 2, 3])
  pattern2 = JChrest.make_number_pattern([1, 3, 2])
  pattern3 = JChrest.make_number_pattern([2, 1, 3])
  
  # store them in an array 
  patterns = [pattern1, pattern2, pattern3]
  
  # Train the model a few times on the patterns
  4.times do 
    for pat in patterns
      model.recognise_and_learn(pat)
    end
  end
  
  # Display the results
  puts "Current model time: #{model.clock}"
  for pat in patterns
    print "For pattern: #{pat} model retrieves "
    puts "#{model.recall_pattern(pat)}"
  end
  
  # And display the Model in a graphical view
  ChrestView.new(model)

